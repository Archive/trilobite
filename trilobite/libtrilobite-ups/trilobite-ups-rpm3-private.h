/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8; tab-width: 8 -*- */
/* 
 * Copyright (C) 2000, 2001 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Eskil Heyn Olsen <eskil@eazel.com>
 */

#ifndef TRILOBITE_UPS_RPM3_PRIVATE_H
#define TRILOBITE_UPS_RPM3_PRIVATE_H

#include <libtrilobite-ups/trilobite-ups-rpm3.h>
#include <rpm/rpmlib.h>

gboolean trilobite_package_system_rpm3_open_dbs (TrilobitePackageSystemRpm3 *system);
gboolean trilobite_package_system_rpm3_close_dbs (TrilobitePackageSystemRpm3 *system);
gboolean trilobite_package_system_rpm3_free_dbs (TrilobitePackageSystemRpm3 *system);
void trilobite_package_system_rpm3_create_dbs (TrilobitePackageSystemRpm3 *system, GList*);
void trilobite_package_system_rpm3_packagedata_fill_from_header (TrilobitePackageSystemRpm3 *system,
								 PackageData *pack, 
								 Header hd,
								 int detail_level);
rpmdb trilobite_package_system_rpm3_get_db (TrilobitePackageSystemRpm3 *system,
					    const char *dbpath);
void trilobite_package_system_rpm3_get_and_set_string_tag (Header hd,
							   int tag, 
							   char **str);

PackageData* trilobite_package_system_rpm3_load_package (TrilobitePackageSystemRpm3 *system,
							 PackageData *in_package,
							 const char *filename,
							 int detail_level);
void trilobite_package_system_rpm3_install (TrilobitePackageSystemRpm3 *system, 
					    const char *dbpath,
					    GList* packages,
					    unsigned long flags);
void trilobite_package_system_rpm3_uninstall (TrilobitePackageSystemRpm3 *system, 
					      const char *dbpath,
					      GList* packages,
					      unsigned long flags);
gboolean trilobite_package_system_rpm3_verify (TrilobitePackageSystemRpm3 *system, 
					       const char *dbpath,
					       GList* packages);

int trilobite_package_system_rpm3_compare_version (TrilobitePackageSystem *system,
						   const char *a,
						   const char *b);

GList* trilobite_package_system_rpm3_query (TrilobitePackageSystemRpm3 *system,
					    const char *dbpath,
					    const gpointer key,
					    TrilobitePackageSystemQueryEnum flag,
					    int detail_level);

void trilobite_package_system_rpm3_query_requires (TrilobitePackageSystemRpm3 *system,
						   const char *dbpath,
						   const gpointer *key,
						   int detail_level,
						   GList **result);
void trilobite_package_system_rpm3_query_requires_feature (TrilobitePackageSystemRpm3 *system,
							   const char *dbpath,
							   const gpointer *key,
							   int detail_level,
							   GList **result);
time_t trilobite_package_system_rpm3_database_mtime (TrilobitePackageSystemRpm3 *system);

struct RpmQueryPiggyBag {
	TrilobitePackageSystemRpm3 *system;
	gpointer key;
	TrilobitePackageSystemQueryEnum flag;
	int detail_level;
	GList **result;
};

struct _TrilobitePackageSystemRpm3Private 
{
	GList *dbpaths; /* GList*< pair < char *dbpath, char *root> > */
	GHashTable *db_to_root;
	GHashTable *dbs;

};

#endif /* TRILOBITE_UPS_RPM3_PRIVATE_H */
