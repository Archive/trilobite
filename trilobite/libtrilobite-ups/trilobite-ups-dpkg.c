/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8; tab-width: 8 -*- */
/* 
 * Copyright (C) 2000, 2001 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Eskil Heyn Olsen <eskil@eazel.com>
 *          Ian McKellar <ian@eazel.com>
 */

#include <config.h>
#include <gnome.h>
#include "trilobite-ups-dpkg.h"
#include "trilobite-ups-private.h"
#include <libtrilobite/trilobite-core-utils.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define MAX_LINE_LEN 4096
#define ROOT "su -c "
#define DPKG_STATUS_FILE "/var/lib/dpkg/status"

typedef struct {
	char *name;
	char *version;
	char *minor;
	char *archtype;

	char *summary;
	char *description;

	char *status;
} DebPackage;

typedef gboolean (*PackageParseCallback) (const DebPackage *package, 
		gpointer data);

TrilobitePackageSystem* trilobite_package_system_implementation (GList*);

/* This is the parent class pointer */
static TrilobitePackageSystemClass *trilobite_package_system_dpkg_parent_class;

static void
debpackage_free (DebPackage *package) 
{
	g_free (package->name);
	g_free (package->version);
	g_free (package->minor);
	g_free (package->archtype);
	g_free (package->summary);
	g_free (package->description);
	g_free (package->status);
	g_free (package);
}

static void
debpackage_fill_packagedata (const DebPackage *dpkg, 
			     PackageData *pdata) 
{
	g_return_if_fail (dpkg != NULL);
	g_return_if_fail (pdata != NULL);

	if (dpkg->name) {
		pdata->name = g_strdup (dpkg->name);
	}
	if (dpkg->version) {
		pdata->version = g_strdup (dpkg->version);
	}
	if (dpkg->minor) {
		pdata->minor = g_strdup (dpkg->minor);
	}
	if (dpkg->archtype) {
		pdata->archtype = g_strdup (dpkg->archtype);
	}
	if (dpkg->summary) {
		pdata->summary = g_strdup (dpkg->summary);
	}
	if (dpkg->description) {
		pdata->description = g_strdup (dpkg->description);
	}

	/* FIXME is there an equivalent of status? */
}

static void 
strip_trailing_whitespace (char *string) 
{
	int i = strlen (string)-1;

	while ( (i>=0) && isspace(string[i])) {
		string[i] = '\0';
		i--;
	}
}


static void 
parse_packages (FILE *packages_file, 
		PackageParseCallback cb,
		gpointer user_data)
{
	char line[MAX_LINE_LEN+1];
	char *value;
	char **previous = NULL;
	DebPackage *package;

	g_return_if_fail (packages_file != NULL);

	package = g_new0 (DebPackage, 1);

	while (!feof (packages_file) && !ferror (packages_file)) {
		fgets (line, MAX_LINE_LEN, packages_file);
		strip_trailing_whitespace (line);
		//g_print ("line: %s\n", line);

		if (isspace (line[0])) {
			/* continuation */
			if (previous) {
				char *tmp1, *tmp2 = *previous;

				//g_print("continuing line...\n");
				tmp1 = g_strconcat (tmp2, "\n", line+1, NULL);
				g_free (tmp2);
				*previous = tmp1;
			}
			continue;
		}

		value = strchr (line, ':');

		if (value == NULL) {
			/* found start of new package */
			(cb)(package, user_data); /*check return? FIXME*/
			debpackage_free (package);
			package = g_new0 (DebPackage, 1);
			previous = NULL;
			continue;
		}

		*value = '\0';
		value++;
		while (isspace (*value)) {
			*value = '\0';
			value++;
		}

		previous = NULL;

		if (!strcmp (line, "Package")) {
			package->name = g_strdup (value);
			previous = &package->name;
		} else if (!strcmp (line, "Version")) {
			/* FIXME: is this the defn of minor */
			char *minor = strrchr (line, '-');

			if (minor) {
				*minor = '\0';
				minor++;
				package->minor = g_strdup (value);
			}
			package->version = g_strdup (value);
			previous = &package->version;
		} else if (!strcmp (line, "Architecture")) {
			package->archtype = g_strdup (value);
			previous = &package->archtype;
		} else if (!strcmp (line, "Description")) {
			package->summary = g_strdup (value);
			package->description = g_strdup (value);
			previous = &package->description;
		} else if (!strcmp (line, "Status")) {
			package->status = g_strdup (value);
			previous = &package->status;
		} else {
			//g_print ("Ignoring [%s][%s]\n", line, value);
		}
	}
	(cb)(package, user_data);
	debpackage_free (package);
}

static gboolean
load_package_callback (const DebPackage *package, 
		       gpointer data) 
{
	PackageData *pdata = data;

	//g_print ("cb: %s\n", package->name);
	//g_print ("cb desc: %s\n", package->description);
	debpackage_fill_packagedata (package, pdata);
	return TRUE;
}

static PackageData*
trilobite_package_system_dpkg_load_package (TrilobitePackageSystemDpkg *system,
					    PackageData *in_package,
					    const char *filename,
					    int detail_level)
{
	PackageData *result = in_package;
	char *dpkg_cmd;
	FILE *dpkg_file;

	g_assert (system != NULL);
	g_assert (TRILOBITE_IS_PACKAGE_SYSTEM_DPKG (system));
	trilobite_debug ("trilobite_package_system_dpkg_load_package");

	dpkg_cmd = g_strdup_printf ("dpkg --field %s", filename);
	dpkg_file = popen (dpkg_cmd, "r");
	if (dpkg_file == NULL) {
		g_warning ("failed to run `%s'", dpkg_cmd);
	}
	g_free (dpkg_cmd);

	if (result == NULL) {
		result = packagedata_new ();
	}

	parse_packages (dpkg_file, load_package_callback, result);

	result->filename = g_strdup (filename);

	pclose (dpkg_file);

	return result;
}

struct QueryData {
	gpointer key;
	TrilobitePackageSystemQueryEnum flag;
	GList *packages;
};

static gboolean
query_callback (const DebPackage *package, 
		gpointer data) 
{
	struct QueryData *query_data = data;
	PackageData *pd;

	//g_print ("cb: %s\n", package->name);

	if ( (package->status == NULL) ||
			strcmp (package->status, "install ok installed")) {
		/* hmm - not actually installed */
		//g_print ("cb discarding\n");
		return TRUE;
	}
	//g_print ("cb keeping\n");

	/* FIXME: umm - actually check that it matches */
	
	pd = packagedata_new ();
	debpackage_fill_packagedata (package, pd);
	query_data->packages = g_list_append (query_data->packages, pd);
	return TRUE;
}

static GList*               
trilobite_package_system_dpkg_query (TrilobitePackageSystemDpkg *system,
				     const char *dbpath,
				     const gpointer key,
				     TrilobitePackageSystemQueryEnum flag,
				     int detail_level)
{
	struct QueryData query_data;
	FILE *status;

	g_assert (system != NULL);
	g_assert (TRILOBITE_IS_PACKAGE_SYSTEM_DPKG (system));
	trilobite_debug ("trilobite_package_system_dpkg_query");

	query_data.key = key;
	query_data.flag = flag;
	query_data.packages = NULL;

	/* FIMXE: lock db! */

	status = fopen (DPKG_STATUS_FILE, "rt");

	if (status == NULL) return NULL;

	parse_packages (status, query_callback, &query_data);

	fclose (status);

	return query_data.packages;
}

static void                 
trilobite_package_system_dpkg_install (TrilobitePackageSystemDpkg *epsystem, 
				       const char *dbpath,
				       GList* packages,
				       unsigned long flags)
{
	char *dpkg_cmd;
	int result;
	PackageData *pdata;

	g_assert (epsystem != NULL);
	g_assert (TRILOBITE_IS_PACKAGE_SYSTEM_DPKG (epsystem));
	trilobite_debug ("trilobite_package_system_dpkg_install");
	/* Code Here */

	while (packages) {
		pdata = (PackageData *)packages->data;

		dpkg_cmd = g_strdup_printf (ROOT "dpkg --unpack %s", 
				pdata->filename);
		result = system (dpkg_cmd);
		g_free (dpkg_cmd);

		/* FIXME: erm - perhaps pop up some kind of zvt???? */
		dpkg_cmd = g_strdup_printf (ROOT "dpkg --configure %s", 
				pdata->name);
		result = system (dpkg_cmd);
		g_free (dpkg_cmd);
		g_free (dpkg_cmd);

		packages = packages->next;
	}

}

static void                 
trilobite_package_system_dpkg_uninstall (TrilobitePackageSystemDpkg *system, 
					 const char *dbpath,
					 GList* packages,
					 unsigned long flags)
{
	g_assert (system != NULL);
	g_assert (TRILOBITE_IS_PACKAGE_SYSTEM_DPKG (system));
	trilobite_debug ("trilobite_package_system_dpkg_uninstall");
	/* Code Here */
}

static void                 
trilobite_package_system_dpkg_verify (TrilobitePackageSystemDpkg *system, 
				      GList* packages)
{
	g_assert (system != NULL);
	g_assert (TRILOBITE_IS_PACKAGE_SYSTEM_DPKG (system));
	trilobite_debug ("trilobite_package_system_dpkg_verify");
	/* Code Here */
}

static int
trilobite_package_system_dpkg_compare_version (TrilobitePackageSystemDpkg *system,
					       const char *a,
					       const char *b)
{
	g_assert (system != NULL);
	g_assert (TRILOBITE_IS_PACKAGE_SYSTEM_DPKG (system));
	trilobite_debug ("trilobite_package_system_dpkg_compare_version");
	/* Code Here */
	return 0;
}


/************************************
 Database mtime implementation
************************************/
static time_t
trilobite_package_system_dpkg_database_mtime (TrilobitePackageSystemDpkg *system)
{
	struct stat st;

	if (stat (DPKG_STATUS_FILE, &st) == 0) {
		return st.st_mtime;
	} else {
		return (time_t)0;
	}


}

/*****************************************
  GTK+ object stuff
*****************************************/

static void
trilobite_package_system_dpkg_finalize (GtkObject *object)
{
	TrilobitePackageSystemDpkg *system;

	g_return_if_fail (object != NULL);
	g_return_if_fail (TRILOBITE_PACKAGE_SYSTEM_DPKG (object));

	system = TRILOBITE_PACKAGE_SYSTEM_DPKG (object);

	if (GTK_OBJECT_CLASS (trilobite_package_system_dpkg_parent_class)->finalize) {
		GTK_OBJECT_CLASS (trilobite_package_system_dpkg_parent_class)->finalize (object);
	}
}

static void
trilobite_package_system_dpkg_class_initialize (TrilobitePackageSystemDpkgClass *klass) 
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass*)klass;
	object_class->finalize = trilobite_package_system_dpkg_finalize;
	
	trilobite_package_system_dpkg_parent_class = gtk_type_class (trilobite_package_system_get_type ());
}

static void
trilobite_package_system_dpkg_initialize (TrilobitePackageSystemDpkg *system) {
	g_assert (system != NULL);
	g_assert (TRILOBITE_IS_PACKAGE_SYSTEM_DPKG (system));
}

GtkType
trilobite_package_system_dpkg_get_type() {
	static GtkType system_type = 0;

	/* First time it's called ? */
	if (!system_type)
	{
		static const GtkTypeInfo system_info =
		{
			"TrilobitePackageSystemDpkg",
			sizeof (TrilobitePackageSystemDpkg),
			sizeof (TrilobitePackageSystemDpkgClass),
			(GtkClassInitFunc) trilobite_package_system_dpkg_class_initialize,
			(GtkObjectInitFunc) trilobite_package_system_dpkg_initialize,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		system_type = gtk_type_unique (trilobite_package_system_get_type (), &system_info);
	}

	return system_type;
}

TrilobitePackageSystemDpkg *
trilobite_package_system_dpkg_new (GList *dbpaths) 
{
	TrilobitePackageSystemDpkg *system;

	system = TRILOBITE_PACKAGE_SYSTEM_DPKG (gtk_object_new (TYPE_TRILOBITE_PACKAGE_SYSTEM_DPKG, NULL));

	gtk_object_ref (GTK_OBJECT (system));
	gtk_object_sink (GTK_OBJECT (system));

	return system;
}

TrilobitePackageSystem*
trilobite_package_system_implementation (GList *dbpaths)
{
	TrilobitePackageSystem *result;

	trilobite_debug ("trilobite_package_system_implementation (dpkg)");

	result = TRILOBITE_PACKAGE_SYSTEM (trilobite_package_system_dpkg_new (dbpaths));
	
	result->private->load_package
		= (TrilobitePackageSytemLoadPackageFunc)trilobite_package_system_dpkg_load_package;

	result->private->query
		= (TrilobitePackageSytemQueryFunc)trilobite_package_system_dpkg_query;

	result->private->install
		= (TrilobitePackageSytemInstallFunc)trilobite_package_system_dpkg_install;

	result->private->uninstall
		= (TrilobitePackageSytemUninstallFunc)trilobite_package_system_dpkg_uninstall;

	result->private->verify
		= (TrilobitePackageSytemVerifyFunc)trilobite_package_system_dpkg_verify;

	result->private->compare_version
		= (TrilobitePackageSystemCompareVersionFunc)trilobite_package_system_dpkg_compare_version;

	result->private->database_mtime
		= (TrilobitePackageSystemDatabaseMtimeFunc)trilobite_package_system_dpkg_database_mtime;

	return result;
}
