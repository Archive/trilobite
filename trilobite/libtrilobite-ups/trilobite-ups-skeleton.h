/* -*- Mode: C; indent-tabs-mode: false; c-basic-offset: 8; tab-width: 8 -*- */
/* 
 * Copyright (C) 2000, 2001 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Eskil Heyn Olsen <eskil@eazel.com>
 */

#ifndef TRILOBITE_UPS_SKELETON_H
#define TRILOBITE_UPS_SKELETON_H

#include <libtrilobite-ups/trilobite-ups.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define TYPE_TRILOBITE_PACKAGE_SYSTEM_SKELETON \
        (trilobite_package_system_skeleton_get_type ())
#define TRILOBITE_PACKAGE_SYSTEM_SKELETON(obj) \
	(GTK_CHECK_CAST ((obj), TYPE_TRILOBITE_PACKAGE_SYSTEM_SKELETON, TrilobitePackageSystemSkeleton))
#define TRILOBITE_PACKAGE_SYSTEM_SKELETON_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST ((klass), TYPE_TRILOBITE_PACKAGE_SYSTEM_SKELETON, TrilobitePackageSystemSkeletonClass))
#define TRILOBITE_IS_PACKAGE_SYSTEM_SKELETON(obj) \
	(GTK_CHECK_TYPE ((obj), TYPE_TRILOBITE_PACKAGE_SYSTEM_SKELETON))
#define TRILOBITE_IS_PACKAGE_SYSTEM_SKELETON_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE ((klass), TYPE_TRILOBITE_PACKAGE_SYSTEM_SKELETON))

typedef struct _TrilobitePackageSystemSkeleton TrilobitePackageSystemSkeleton;
typedef struct _TrilobitePackageSystemSkeletonClass TrilobitePackageSystemSkeletonClass;


struct _TrilobitePackageSystemSkeletonClass
{
	TrilobitePackageSystemClass parent_class;
};

struct _TrilobitePackageSystemSkeleton
{
	TrilobitePackageSystem parent;
};

TrilobitePackageSystemSkeleton *trilobite_package_system_skeleton_new (GList *dbpaths);
GtkType              		trilobite_package_system_skeleton_get_type (void);

#endif /* TRILOBITE_UPS_SKELETON_H */

