/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8; tab-width: 8 -*- */
/* 
 * Copyright (C) 2000, 2001 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Eskil Heyn Olsen <eskil@eazel.com>
 *          Ian McKellar <ian@eazel.com>
 */

#ifndef TRILOBITE_UPS_DPKG_H
#define TRILOBITE_UPS_DPKG_H

#include <libtrilobite-ups/trilobite-ups.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define TYPE_TRILOBITE_PACKAGE_SYSTEM_DPKG \
	(trilobite_package_system_dpkg_get_type ())
#define TRILOBITE_PACKAGE_SYSTEM_DPKG(obj) \
	(GTK_CHECK_CAST ((obj), TYPE_TRILOBITE_PACKAGE_SYSTEM_DPKG, TrilobitePackageSystemDpkg))
#define TRILOBITE_PACKAGE_SYSTEM_DPKG_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST ((klass), TYPE_TRILOBITE_PACKAGE_SYSTEM_DPKG, TrilobitePackageSystemDpkgClass))
#define TRILOBITE_IS_PACKAGE_SYSTEM_DPKG(obj) \
	(GTK_CHECK_TYPE ((obj), TYPE_TRILOBITE_PACKAGE_SYSTEM_DPKG))
#define TRILOBITE_IS_PACKAGE_SYSTEM_DPKG_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE ((klass), TYPE_TRILOBITE_PACKAGE_SYSTEM_DPKG))

typedef struct _TrilobitePackageSystemDpkg TrilobitePackageSystemDpkg;
typedef struct _TrilobitePackageSystemDpkgClass TrilobitePackageSystemDpkgClass;


struct _TrilobitePackageSystemDpkgClass
{
	TrilobitePackageSystemClass parent_class;
};

struct _TrilobitePackageSystemDpkg
{
	TrilobitePackageSystem parent;
};

TrilobitePackageSystemDpkg     *trilobite_package_system_dpkg_new (GList *dbpaths);
GtkType              	    	trilobite_package_system_dpkg_get_type (void);

#endif /* TRILOBITE_UPS_DPKG_H */

