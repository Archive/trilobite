/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* 
 * Copyright (C) 2000, 2001 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Eskil Heyn Olsen <eskil@eazel.com>
 */

#ifndef TRILOBITE_UPS_PRIVATE_H
#define TRILOBITE_UPS_PRIVATE_H

#include <libtrilobite-ups/trilobite-ups.h>

#define TRILOBITE_PACKAGE_SYSTEM_PROGRESS_LONGS 6

#define EPS_SANE(val) g_return_if_fail (val!=NULL); \
                      g_return_if_fail (TRILOBITE_IS_PACKAGE_SYSTEM (val)); \
                      g_return_if_fail (val->private); 

#define EPS_SANE_VAL(val, v) g_return_val_if_fail (val!=NULL, v); \
                             g_return_val_if_fail (TRILOBITE_IS_PACKAGE_SYSTEM (val), v); \
                             g_return_val_if_fail (val->private, v); 

#define EPS_API(val) g_assert (val!=NULL); g_assert (TRILOBITE_IS_PACKAGE_SYSTEM (val)); g_assert (val->private);

#define info(system, s...) if (trilobite_package_system_get_debug (TRILOBITE_PACKAGE_SYSTEM (system)) & TRILOBITE_PACKAGE_SYSTEM_DEBUG_INFO) { trilobite_debug (s); }
#define fail(system, s...) if (trilobite_package_system_get_debug (TRILOBITE_PACKAGE_SYSTEM (system)) & TRILOBITE_PACKAGE_SYSTEM_DEBUG_FAIL) { trilobite_debug (s); }
#define verbose(system, s...) if (trilobite_package_system_get_debug (TRILOBITE_PACKAGE_SYSTEM (system)) & TRILOBITE_PACKAGE_SYSTEM_DEBUG_VERBOSE) { trilobite_debug (s); }

typedef TrilobitePackageSystem*(*TrilobitePackageSystemConstructorFunc) (GList*);

typedef	PackageData* (*TrilobitePackageSytemLoadPackageFunc) (TrilobitePackageSystem*,
							      PackageData*, 
							      const char*, 
							      int);
typedef GList* (*TrilobitePackageSytemQueryFunc) (TrilobitePackageSystem*, 
						  const char*, 
						  gpointer key, 
						  TrilobitePackageSystemQueryEnum, 
						  int);
typedef void (*TrilobitePackageSytemInstallFunc) (TrilobitePackageSystem*, 
						  const char*,
						  GList*, 
						  unsigned long);
typedef void (*TrilobitePackageSytemUninstallFunc) (TrilobitePackageSystem*, 
						    const char *,
						    GList*, 
						    unsigned long);
typedef gboolean (*TrilobitePackageSytemVerifyFunc) (TrilobitePackageSystem*, 
						     const char*,
						     GList*);
typedef int (*TrilobitePackageSystemCompareVersionFunc) (TrilobitePackageSystem*, 
							 const char *,
							 const char *);
typedef time_t (*TrilobitePackageSystemDatabaseMtimeFunc) (TrilobitePackageSystem*);

struct _TrilobitePackageSystemPrivate {	
	TrilobitePackageSytemLoadPackageFunc load_package;
	TrilobitePackageSytemQueryFunc query;
	TrilobitePackageSytemInstallFunc install;
	TrilobitePackageSytemUninstallFunc uninstall;
	TrilobitePackageSytemVerifyFunc verify;
	TrilobitePackageSystemCompareVersionFunc compare_version;
	TrilobitePackageSystemDatabaseMtimeFunc database_mtime;

	TrilobitePackageSystemDebug debug;
};

TrilobitePackageSystem  *trilobite_package_system_new_real (void);

gboolean trilobite_package_system_emit_start (TrilobitePackageSystem*, 
					      TrilobitePackageSystemOperation, 
					      const PackageData*);
gboolean trilobite_package_system_emit_progress (TrilobitePackageSystem*, 
						 TrilobitePackageSystemOperation, 
						 const PackageData*, 
						 unsigned long[TRILOBITE_PACKAGE_SYSTEM_PROGRESS_LONGS]);
gboolean trilobite_package_system_emit_failed (TrilobitePackageSystem*, 
					       TrilobitePackageSystemOperation, 
					       const PackageData*);
gboolean trilobite_package_system_emit_end (TrilobitePackageSystem*, 
					    TrilobitePackageSystemOperation, 
					    const PackageData*);

void trilobite_package_system_marshal_BOOL__ENUM_POINTER (GtkObject *object,
							  GtkSignalFunc func,
							  gpointer func_data,
							  GtkArg *args);
void trilobite_package_system_marshal_BOOL__ENUM_POINTER_POINTER (GtkObject *object,
								  GtkSignalFunc func,
								  gpointer func_data,
								  GtkArg *args);

#endif /* TRILOBITE_UPS_PRIVATE_H */
