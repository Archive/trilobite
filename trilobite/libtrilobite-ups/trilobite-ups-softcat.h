/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8; tab-width: 8 -*- */
/* 
 * Copyright (C) 2000, 2001 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Robey Pointer <robey@eazel.com>
 *          Eskil Heyn Olsen <eskil@eazel.com>
 */

#ifndef TRILOBITE_UPS_SOFTCAT_PUBLIC_H
#define TRILOBITE_UPS_SOFTCAT_PUBLIC_H

#include <libtrilobite-ups/trilobite-ups-types.h>
#include <libtrilobite-ups/trilobite-ups-softcat-private.h>

#define TYPE_TRILOBITE_SOFTCAT \
	(trilobite_softcat_get_type ())
#define TRILOBITE_SOFTCAT(obj) \
        (GTK_CHECK_CAST ((obj), TYPE_TRILOBITE_SOFTCAT, TrilobiteSoftCat))
#define TRILOBITE_SOFTCAT_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST ((klass), TYPE_TRILOBITE_SOFTCAT, TrilobiteSoftCatClass))
#define TRILOBITE_IS_SOFTCAT(obj) \
	(GTK_CHECK_TYPE ((obj), TYPE_TRILOBITE_SOFTCAT))
#define TRILOBITE_IS_SOFTCAT_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE ((klass), TYPE_TRILOBITE_SOFTCAT))

typedef struct _TrilobiteSoftCat TrilobiteSoftCat;
typedef struct _TrilobiteSoftCatClass TrilobiteSoftCatClass;

typedef enum {
	TRILOBITE_SOFTCAT_SUCCESS = 0,
	TRILOBITE_SOFTCAT_ERROR_BAD_MOJO,
	TRILOBITE_SOFTCAT_ERROR_SERVER_UNREACHABLE,
	TRILOBITE_SOFTCAT_ERROR_MULTIPLE_RESPONSES,
	TRILOBITE_SOFTCAT_ERROR_SERVER_UPDATED,
	TRILOBITE_SOFTCAT_ERROR_NO_SUCH_PACKAGE,
} TrilobiteSoftCatError;

struct _TrilobiteSoftCatClass
{
	GtkObjectClass parent_class;
};

typedef struct _TrilobiteSoftCatPrivate TrilobiteSoftCatPrivate;

struct _TrilobiteSoftCat
{
	GtkObject parent;
	TrilobiteSoftCatPrivate *private;
};

TrilobiteSoftCat *	trilobite_softcat_new (void);
GtkType		        trilobite_softcat_get_type (void);
void			trilobite_softcat_unref (GtkObject *object);

/* set and get fields */
void trilobite_softcat_set_server (TrilobiteSoftCat *softcat, const char *server);
const char *trilobite_softcat_get_server (TrilobiteSoftCat *softcat);

void trilobite_softcat_set_server_host (TrilobiteSoftCat *softcat, const char *server);
const char *trilobite_softcat_get_server_host (TrilobiteSoftCat *softcat);

void trilobite_softcat_set_server_port (TrilobiteSoftCat *softcat, int port);
int trilobite_softcat_get_server_port (TrilobiteSoftCat *softcat);

void trilobite_softcat_set_cgi_path (TrilobiteSoftCat *softcat, const char *cgi_path);
const char *trilobite_softcat_get_cgi_path (const TrilobiteSoftCat *softcat);

void trilobite_softcat_set_authn (TrilobiteSoftCat *softcat, gboolean use_authn, const char *username);
gboolean trilobite_softcat_get_authn (const TrilobiteSoftCat *softcat, const char **username);

void trilobite_softcat_set_packages_per_query (TrilobiteSoftCat *softcat, int number);
void trilobite_softcat_set_authn_flag (TrilobiteSoftCat *softcat, gboolean use_authn);
void trilobite_softcat_set_username (TrilobiteSoftCat *softcat, const char *username);
void trilobite_softcat_set_retry (TrilobiteSoftCat *softcat, unsigned int retries, unsigned int delay_us);
void trilobite_softcat_reset_server_update_flag (TrilobiteSoftCat *softcat);

TrilobiteSoftCatSense trilobite_softcat_convert_sense_flags (int flags);
char *trilobite_softcat_sense_flags_to_string (TrilobiteSoftCatSense flags);
TrilobiteSoftCatSense trilobite_softcat_string_to_sense_flags (const char *str);

const char *trilobite_softcat_error_string (TrilobiteSoftCatError err);

/* Query softcat about a package, and return a list of matching packages
 * (because there may be more than one if the package refers to a suite).
 */
TrilobiteSoftCatError trilobite_softcat_query (TrilobiteSoftCat *softcat,
                                               GList *packages,
                                               int sense_flags,
                                               int fill_flags,
                                               GList **result);

/* Given a partially filled packagedata object, 
   check softcat, and fill it with the desired info */
TrilobiteSoftCatError  trilobite_softcat_get_info (TrilobiteSoftCat *softcat,
                                                   PackageData *partial,
                                                   int sense_flags,
                                                   int fill_flags);

/* Given a partially filled packagedata object, 
   check softcat, and fill it with the desired info */
TrilobiteSoftCatError  trilobite_softcat_get_info_plural (TrilobiteSoftCat *softcat,
                                                          GList *partials,
                                                          GList **out, 
                                                          GList **error,
                                                          int sense_flags,
                                                          int fill_flags);

/* Check if there's a newer version in SoftCat.
 * Returns TRUE and fills in 'newpack' if there is, returns FALSE otherwise.
 */
gboolean trilobite_softcat_available_update (TrilobiteSoftCat *softcat,
                                             PackageData *oldpack,
                                             PackageData **newpack,
                                             int fill_flags);

#endif /* TRILOBITE_UPS_SOFTCAT_PUBLIC_H */
