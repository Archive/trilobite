/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* 
 * Copyright (C) 2000 Eazel, Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Eskil Heyn Olsen <eskil@eazel.com>
 *
 */

#ifndef TRILOBITE_UPS_RPM3_H
#define TRILOBITE_UPS_RPM3_H

#include <libtrilobite-ups/trilobite-ups.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define TYPE_TRILOBITE_PACKAGE_SYSTEM_RPM3 \
	(trilobite_package_system_rpm3_get_type ())
#define TRILOBITE_PACKAGE_SYSTEM_RPM3(obj) \
	(GTK_CHECK_CAST ((obj), TYPE_TRILOBITE_PACKAGE_SYSTEM_RPM3, TrilobitePackageSystemRpm3))
#define TRILOBITE_PACKAGE_SYSTEM_RPM3_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST ((klass), TYPE_TRILOBITE_PACKAGE_SYSTEM_RPM3, TrilobitePackageSystemRpm3Class))
#define TRILOBITE_IS_PACKAGE_SYSTEM_RPM3(obj) \
	(GTK_CHECK_TYPE ((obj), TYPE_TRILOBITE_PACKAGE_SYSTEM_RPM3))
#define TRILOBITE_IS_PACKAGE_SYSTEM_RPM3_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE ((klass), TYPE_TRILOBITE_PACKAGE_SYSTEM_RPM3))

typedef struct _TrilobitePackageSystemRpm3 TrilobitePackageSystemRpm3;
typedef struct _TrilobitePackageSystemRpm3Class TrilobitePackageSystemRpm3Class;

typedef void (*TrilobitePackageSystemRpmQueryForeachFunc) (const char *dbpath, 
							   gpointer db, 
							   gpointer pig);
typedef void (*TrilobitePackageSystemRpmQueryImplFunc) (TrilobitePackageSystem *system,
							const char *dbpath,
							const char* key,
							TrilobitePackageSystemQueryEnum flag,
							int detail_level,
							GList **result);

struct _TrilobitePackageSystemRpm3Class
{
	TrilobitePackageSystemClass parent_class;

	TrilobitePackageSystemRpmQueryForeachFunc query_foreach;
	TrilobitePackageSystemRpmQueryImplFunc query_impl;	
};

typedef struct _TrilobitePackageSystemRpm3Private TrilobitePackageSystemRpm3Private;

struct _TrilobitePackageSystemRpm3
{
	TrilobitePackageSystem parent;
	TrilobitePackageSystemRpm3Private *private;
};
TrilobitePackageSystemRpm3 *trilobite_package_system_rpm3_new (GList *dbpaths);
GtkType                     trilobite_package_system_rpm3_get_type (void);

#endif /* TRILOBITE_UPS_RPM3_H */

