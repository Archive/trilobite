/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8; tab-width: 8 -*- */
/* 
 * Copyright (C) 2000, 2001 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Eskil Heyn Olsen <eskil@eazel.com>
 *
 */

#ifndef TRILOBITE_UPS_RPM4_H
#define TRILOBITE_UPS_RPM4_H

#include <libtrilobite-ups/trilobite-ups-rpm3.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define TYPE_TRILOBITE_PACKAGE_SYSTEM_RPM4 \
	(trilobite_package_system_rpm4_get_type ())
#define TRILOBITE_PACKAGE_SYSTEM_RPM4(obj) \
	(GTK_CHECK_CAST ((obj), TYPE_TRILOBITE_PACKAGE_SYSTEM_RPM4, TrilobitePackageSystemRpm4))
#define TRILOBITE_PACKAGE_SYSTEM_RPM4_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST ((klass), TYPE_TRILOBITE_PACKAGE_SYSTEM_RPM4, TrilobitePackageSystemRpm4Class))
#define TRILOBITE_IS_PACKAGE_SYSTEM_RPM4(obj) \
	(GTK_CHECK_TYPE ((obj), TYPE_TRILOBITE_PACKAGE_SYSTEM_RPM4))
#define TRILOBITE_IS_PACKAGE_SYSTEM_RPM4_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE ((klass), TYPE_TRILOBITE_PACKAGE_SYSTEM_RPM4))

typedef struct _TrilobitePackageSystemRpm4 TrilobitePackageSystemRpm4;
typedef struct _TrilobitePackageSystemRpm4Class TrilobitePackageSystemRpm4Class;

struct _TrilobitePackageSystemRpm4Class
{
	TrilobitePackageSystemRpm3Class parent_class;
	
};

struct _TrilobitePackageSystemRpm4
{
	TrilobitePackageSystemRpm3 parent;
};
TrilobitePackageSystemRpm4     *trilobite_package_system_rpm4_new (GList *dbpaths);
GtkType              		trilobite_package_system_rpm4_get_type (void);

#endif /* TRILOBITE_UPS_RPM4_H */

