/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8; tab-width: 8 -*- */
/* 
 * Copyright (C) 2000, 2001 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Eskil Heyn Olsen <eskil@eazel.com>
 */

#ifndef TRILOBITE_UPS_H
#define TRILOBITE_UPS_H

#include <libtrilobite-ups/trilobite-ups-types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define TYPE_TRILOBITE_PACKAGE_SYSTEM \
	(trilobite_package_system_get_type ())
#define TRILOBITE_PACKAGE_SYSTEM(obj) \
        (GTK_CHECK_CAST ((obj), TYPE_TRILOBITE_PACKAGE_SYSTEM, TrilobitePackageSystem))
#define TRILOBITE_PACKAGE_SYSTEM_CLASS(klass) \
        (GTK_CHECK_CLASS_CAST ((klass), TYPE_TRILOBITE_PACKAGE_SYSTEM, TrilobitePackageSystemClass))
#define TRILOBITE_IS_PACKAGE_SYSTEM(obj) \
        (GTK_CHECK_TYPE ((obj), TYPE_TRILOBITE_PACKAGE_SYSTEM))
#define TRILOBITE_IS_PACKAGE_SYSTEM_CLASS(klass) \
 	(GTK_CHECK_CLASS_TYPE ((klass), TYPE_TRILOBITE_PACKAGE_SYSTEM))

typedef struct _TrilobitePackageSystem TrilobitePackageSystem;
typedef struct _TrilobitePackageSystemClass TrilobitePackageSystemClass;

/* This enum identifies the package system
   used for the object instance */
typedef enum {
	TRILOBITE_PACKAGE_SYSTEM_UNSUPPORTED,
	TRILOBITE_PACKAGE_SYSTEM_RPM_3,
	TRILOBITE_PACKAGE_SYSTEM_RPM_4,
	TRILOBITE_PACKAGE_SYSTEM_DEB,
} TrilobitePackageSystemId;

typedef enum {
	TRILOBITE_PACKAGE_SYSTEM_QUERY_OWNS,
	TRILOBITE_PACKAGE_SYSTEM_QUERY_PROVIDES,
	TRILOBITE_PACKAGE_SYSTEM_QUERY_REQUIRES,
	TRILOBITE_PACKAGE_SYSTEM_QUERY_REQUIRES_FEATURE,
	TRILOBITE_PACKAGE_SYSTEM_QUERY_MATCHES,
	TRILOBITE_PACKAGE_SYSTEM_QUERY_SUBSTR
} TrilobitePackageSystemQueryEnum;

enum {
	TRILOBITE_PACKAGE_SYSTEM_OPERATION_TEST = 0x1,
	TRILOBITE_PACKAGE_SYSTEM_OPERATION_FORCE = 0x2,
	TRILOBITE_PACKAGE_SYSTEM_OPERATION_UPGRADE = 0x10, 
	TRILOBITE_PACKAGE_SYSTEM_OPERATION_DOWNGRADE = 0x20
};

typedef enum {
	TRILOBITE_PACKAGE_SYSTEM_OPERATION_INSTALL,
	TRILOBITE_PACKAGE_SYSTEM_OPERATION_UNINSTALL,
	TRILOBITE_PACKAGE_SYSTEM_OPERATION_VERIFY,
} TrilobitePackageSystemOperation;

struct _TrilobitePackageSystemClass
{
	GtkObjectClass parent_class;
	gboolean (*start)(TrilobitePackageSystem*, 
			  TrilobitePackageSystemOperation, 
			  const PackageData*,
			  unsigned long*);
	gboolean (*progress)(TrilobitePackageSystem*, 
			     TrilobitePackageSystemOperation, 
			     const PackageData*, 
			     unsigned long*);
	gboolean (*failed)(TrilobitePackageSystem*, 
			   TrilobitePackageSystemOperation, 
			   const PackageData*);
	gboolean (*end)(TrilobitePackageSystem*, 
			TrilobitePackageSystemOperation, 
			const PackageData*);
};

typedef enum {
	TRILOBITE_PACKAGE_SYSTEM_DEBUG_SILENT = 0x0,
	TRILOBITE_PACKAGE_SYSTEM_DEBUG_INFO = 0x1,
	TRILOBITE_PACKAGE_SYSTEM_DEBUG_FAIL = 0x2,
	TRILOBITE_PACKAGE_SYSTEM_DEBUG_VERBOSE = 0xffff
} TrilobitePackageSystemDebug;

/* I hate myself for this... please, give me exceptions! */
typedef enum  {
	TrilobitePackageSystemError_DB_ACCESS
} TrilobitePackageSystemErrorEnum;

typedef struct _TrilobitePackageSystemError TrilobitePackageSystemError;
struct  _TrilobitePackageSystemError {	
	TrilobitePackageSystemErrorEnum e;
	union {
		struct {
			const char *path;
			pid_t pid;
		} db_access;
	} u;
};

typedef struct _TrilobitePackageSystemPrivate TrilobitePackageSystemPrivate;
struct _TrilobitePackageSystem
{
	GtkObject parent;
	TrilobitePackageSystemPrivate *private;
	TrilobitePackageSystemError *err;
};

TrilobitePackageSystemId	trilobite_package_system_suggest_id (void);
TrilobitePackageSystem         *trilobite_package_system_new (GList *dbpaths);
TrilobitePackageSystem         *trilobite_package_system_new_with_id (TrilobitePackageSystemId, GList *dbpaths);
GtkType                         trilobite_package_system_get_type (void);

TrilobitePackageSystemDebug     trilobite_package_system_get_debug (TrilobitePackageSystem *system);
void                            trilobite_package_system_set_debug (TrilobitePackageSystem *system,
								    TrilobitePackageSystemDebug d);

gboolean       			trilobite_package_system_is_installed (TrilobitePackageSystem *package_system,
								       const char *dbpath,
								       const char *name,
								       const char *version,
								       const char *minor,
								       TrilobiteSoftCatSense version_sense);

PackageData                    *trilobite_package_system_load_package (TrilobitePackageSystem *package_system,
								       PackageData *in_package,
								       const char *filename,
								       int detail_level);
GList *                       	trilobite_package_system_query (TrilobitePackageSystem *package_system,
								const char *dbpath,
								const gpointer key,
								TrilobitePackageSystemQueryEnum flag,
								int detail_level);
void				trilobite_package_system_install (TrilobitePackageSystem *package_system, 
								  const char *dbpath,
								  GList* packages,
								  unsigned long flags);
void                            trilobite_package_system_uninstall (TrilobitePackageSystem *package_system, 
								    const char *dbpath,
								    GList* packages,
								    unsigned long flags);
gboolean                        trilobite_package_system_verify (TrilobitePackageSystem *package_system, 
								 const char *dbpath,
								 GList* packages);
int                             trilobite_package_system_compare_version (TrilobitePackageSystem *package_system, 
									  const char *a,
									  const char *b);
time_t                          trilobite_package_system_database_mtime (TrilobitePackageSystem *package_system);

#endif /* TRILOBITE_UPS_H */

