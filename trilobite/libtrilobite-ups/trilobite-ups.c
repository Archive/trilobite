/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* 
 * Copyright (C) 2000, 2001 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Eskil Heyn Olsen <eskil@eazel.com>
 */

#include <config.h>
#include "trilobite-ups-private.h"

#include <libtrilobite/trilobite-core-distribution.h>
#include <libtrilobite/trilobite-core-utils.h>
#include <libtrilobite/trilobite-md5-tools.h>
#include <gtk/gtksignal.h>
#include <gmodule.h>

#undef EPS_DEBUG

enum {
	START,
	END,
	PROGRESS,
	FAILED,
	LAST_SIGNAL
};

/* The signal array, used for building the signal bindings */
static guint signals[LAST_SIGNAL] = { 0 };
/* This is the parent class pointer */
static GtkObjectClass *trilobite_package_system_parent_class;

/*****************************************/

TrilobitePackageSystemId 
trilobite_package_system_suggest_id ()
{
	TrilobitePackageSystemId  result = TRILOBITE_PACKAGE_SYSTEM_UNSUPPORTED;
	TrilobiteDistributionInfo dist = trilobite_get_distribution ();

	switch (dist.name) {
	case DISTRO_REDHAT: 
		if (dist.version_major == 6) {
			result = TRILOBITE_PACKAGE_SYSTEM_RPM_3;
		} else {
			result = TRILOBITE_PACKAGE_SYSTEM_RPM_4;
		}
		break;
	case DISTRO_MANDRAKE: 
               if (dist.version_major >= 8) {
                       result = TRILOBITE_PACKAGE_SYSTEM_RPM_4;
               }
               else {
                       result = TRILOBITE_PACKAGE_SYSTEM_RPM_3;
               }
		break;
	case DISTRO_YELLOWDOG:
		result = TRILOBITE_PACKAGE_SYSTEM_UNSUPPORTED;
		break;
	case DISTRO_TURBOLINUX: 
		result = TRILOBITE_PACKAGE_SYSTEM_RPM_3;
		break;
	case DISTRO_COREL: 
		result = TRILOBITE_PACKAGE_SYSTEM_DEB;
		break;
	case DISTRO_DEBIAN: 
		result = TRILOBITE_PACKAGE_SYSTEM_DEB;
		break;
	case DISTRO_CALDERA: 
		result = TRILOBITE_PACKAGE_SYSTEM_RPM_3;
		break;
	case DISTRO_SUSE: 
		result = TRILOBITE_PACKAGE_SYSTEM_RPM_3;
		break;
	case DISTRO_LINUXPPC: 
		result = TRILOBITE_PACKAGE_SYSTEM_RPM_3;
		break;
	case DISTRO_UNKNOWN:
		result = TRILOBITE_PACKAGE_SYSTEM_UNSUPPORTED;
		break;
	}
	return result;		
}

#ifdef TRILOBITE_INSTALL_SLIM
TrilobitePackageSystem *trilobite_package_system_implementation (GList *roots);
#endif

static TrilobitePackageSystem*
trilobite_package_system_load_implementation (TrilobitePackageSystemId id, GList *roots)
{
	TrilobitePackageSystem *result = NULL;
	TrilobitePackageSystemConstructorFunc const_func = NULL;
	GModule *module = NULL;

#ifdef TRILOBITE_INSTALL_SLIM
	return trilobite_package_system_implementation (roots);
#endif

	switch (id) {
	case TRILOBITE_PACKAGE_SYSTEM_DEB:
		module = g_module_open ("libtrilobite-ups-dpkg.so", G_MODULE_BIND_LAZY);
		break;
	case TRILOBITE_PACKAGE_SYSTEM_RPM_3:
		module = g_module_open ("libtrilobite-ups-rpm3.so", G_MODULE_BIND_LAZY);
		break;
	case TRILOBITE_PACKAGE_SYSTEM_RPM_4:
		module = g_module_open ("libtrilobite-ups-rpm4.so", G_MODULE_BIND_LAZY);
		break;
#if 0	/* someday */
	case TRILOBITE_PACKAGE_SYSTEM_BUILTIN:
		module = g_module_open (NULL /* myself */, G_MODULE_BIND_LAZY);
		break;
#endif
	default:
		trilobite_debug ("EPS: Unsupported System");
	};

	if (module==NULL) {
		g_warning ("gmodule: %s", g_module_error ());
	} else {
		g_module_make_resident (module);
		g_module_symbol (module, "trilobite_package_system_implementation", (gpointer)&const_func);
		result = (*const_func)(roots);
	}

	return result;
}

static gboolean
trilobite_package_system_matches_versioning (TrilobitePackageSystem *package_system,
					     PackageData *pack,
					     const char *version,
					     const char *minor,
					     TrilobiteSoftCatSense sense)
{
	gboolean version_result = FALSE;
	gboolean minor_result = FALSE;
	int result;

	g_assert (!((version == NULL) && (minor != NULL)));

	if (version != NULL) {
		result = trilobite_package_system_compare_version (package_system, pack->version, version);
		if ((sense & TRILOBITE_SOFTCAT_SENSE_EQ) && (result == 0)) {
			version_result = TRUE;
		}
		if ((sense & TRILOBITE_SOFTCAT_SENSE_GT) && (result > 0)) {
			version_result = TRUE;
		}
		if ((sense & TRILOBITE_SOFTCAT_SENSE_LT) && (result < 0)) {
			version_result = TRUE;
		}
	} else {
		version_result = TRUE;
	}

	if (minor != NULL) {
		result = trilobite_package_system_compare_version (package_system, pack->minor, minor);
		if ((sense & TRILOBITE_SOFTCAT_SENSE_EQ) && (result == 0)) {
			minor_result = TRUE;
		}
		if ((sense & TRILOBITE_SOFTCAT_SENSE_GT) && (result > 0)) {
			minor_result = TRUE;
		}			
		if ((sense & TRILOBITE_SOFTCAT_SENSE_LT) && (result < 0)) {
			minor_result = TRUE;
		}			
	} else {
		minor_result = TRUE;
	}

	return (version_result && minor_result);
}

gboolean             
trilobite_package_system_is_installed (TrilobitePackageSystem *package_system,
				       const char *dbpath,
				       const char *name,
				       const char *version,
				       const char *minor,
				       TrilobiteSoftCatSense version_sense)
{
	GList *matches;
	gboolean result = FALSE;
	
#if EPS_DEBUG
	trilobite_debug ("trilobite_package_system_is_installed (..., %s, %s, %s, %d)",
			 name, version, minor, version_sense);
#endif			 

	matches = trilobite_package_system_query (package_system,
					      dbpath,
					      (const gpointer)name,
					      TRILOBITE_PACKAGE_SYSTEM_QUERY_MATCHES,
					      PACKAGE_FILL_MINIMAL);

	if (matches) {
		if (version || minor) {
			GList *iterator;

			for (iterator = matches; iterator && !result; iterator = g_list_next (iterator)) {
				PackageData *pack = (PackageData*)iterator->data;				
										  
				if (trilobite_package_system_matches_versioning (package_system,
									     pack, 
									     version, 
									     minor, 
									     version_sense)) {
					result = TRUE;
					
				}
#if EPS_DEBUG
				g_message("is_installed (%s, %s, %s, %d) == (%s-%s %d %s-%s) %s", 
					  name, version, minor, version_sense, 
					  pack->version, pack->minor,
					  version_sense, 
					  version, minor, 
					  result ? "TRUE" : "FALSE");
#endif
			}
		} else {
			result = TRUE;
		}
		g_list_foreach (matches, (GFunc)gtk_object_unref, NULL);
	}
	g_list_free (matches);
	
	return result;
}

PackageData*
trilobite_package_system_load_package (TrilobitePackageSystem *system,
				       PackageData *in_package,
				       const char *filename,
				       int detail_level)
{
	PackageData *result = NULL;
	char md5[16];

	EPS_SANE_VAL (system, NULL);

	if (system->private->load_package) {
		result = (*system->private->load_package) (system, in_package, filename, detail_level);
		if (result) {
			trilobite_md5_get_digest_from_file (filename, md5);
			result->md5 = g_strdup (trilobite_md5_get_string_from_md5_digest (md5));
		}
	}

	return result;
}

GList*               
trilobite_package_system_query (TrilobitePackageSystem *system,
				const char *root,
				const gpointer key,
				TrilobitePackageSystemQueryEnum flag,
				int detail_level)
{
	GList *result = NULL;

	EPS_SANE_VAL (system, NULL);

	if (system->private->query) {
		g_assert (key);
		result = (*system->private->query) (system, root, key, flag, detail_level);
	}

	return result;
}

static void
trilobite_package_system_fail_all_packages (TrilobitePackageSystem *system, 
					    TrilobitePackageSystemOperation op, 
					    GList *packages)
{
	GList *iterator;
	
	for (iterator = packages; iterator; iterator = g_list_next (iterator)) {
		PackageData *p = PACKAGEDATA (iterator->data);
		trilobite_package_system_emit_start (system, op, p);
		trilobite_package_system_emit_failed (system, op, p);			
		trilobite_package_system_emit_end (system, op, p);		
	}
}

void                 
trilobite_package_system_install (TrilobitePackageSystem *system, 
				  const char *root,
				  GList* packages,
				  unsigned long flags)
{
	EPS_SANE (system);

	if (system->private->install) {		
		/* If we're in test mode, disable FORCE just to trigger
		   any potiental errors */
		if (flags & TRILOBITE_PACKAGE_SYSTEM_OPERATION_TEST) {
			(*system->private->install) (system, root, packages, 
						     flags & ~TRILOBITE_PACKAGE_SYSTEM_OPERATION_FORCE);
		} else {
			(*system->private->install) (system, root, packages, flags);
		}
	} else {
		trilobite_package_system_fail_all_packages (system, 
							TRILOBITE_PACKAGE_SYSTEM_OPERATION_INSTALL, 
							packages);
	}
}

void                 
trilobite_package_system_uninstall (TrilobitePackageSystem *system, 
				    const char *root,
				    GList* packages,
				    unsigned long flags)
{
	EPS_SANE (system);

	if (system->private->uninstall) {
		(*system->private->uninstall) (system, root, packages, flags);
	} else {
		trilobite_package_system_fail_all_packages (system, 
							TRILOBITE_PACKAGE_SYSTEM_OPERATION_UNINSTALL, 
							packages);
	} 
}

gboolean
trilobite_package_system_verify (TrilobitePackageSystem *system, 
				 const char *dbpath,
				 GList* packages)
{
	EPS_SANE_VAL (system, FALSE);
	if (system->private->verify) {
		return (*system->private->verify) (system, dbpath, packages);	
	} else {
		trilobite_package_system_fail_all_packages (system, 
							TRILOBITE_PACKAGE_SYSTEM_OPERATION_VERIFY, 
							packages);
		return FALSE;
	}
}

int
trilobite_package_system_compare_version (TrilobitePackageSystem *system,
					  const char *a,
					  const char *b)
{
	int result;
	EPS_SANE_VAL (system, 0);
	g_assert (system->private->compare_version);
	result = (*system->private->compare_version) (system, a, b);
	return result;
}

time_t
trilobite_package_system_database_mtime (TrilobitePackageSystem *system)
{
	time_t result;
	EPS_SANE_VAL (system, 0);
	if (system->private->database_mtime == NULL) {
		return 0;
	}
	result = (*system->private->database_mtime) (system);
	return result;
}

/******************************************
 The private emitter functions
*******************************************/

gboolean 
trilobite_package_system_emit_start (TrilobitePackageSystem *system, 
				     TrilobitePackageSystemOperation op, 
				     const PackageData *package)
{
	gboolean result = TRUE;
	EPS_API (system);
	gtk_signal_emit (GTK_OBJECT (system),
			 signals [START],
			 op, 
			 package,
			 &result);
	return result;
}

gboolean 
trilobite_package_system_emit_progress (TrilobitePackageSystem *system, 
					TrilobitePackageSystemOperation op, 
					const PackageData *package,
					unsigned long info[TRILOBITE_PACKAGE_SYSTEM_PROGRESS_LONGS])
{
	gboolean result = TRUE;
	int infos;
	unsigned long *infoblock;

	EPS_API (system);
	infoblock = g_new0 (unsigned long, TRILOBITE_PACKAGE_SYSTEM_PROGRESS_LONGS+1);
	for (infos = 0; infos < TRILOBITE_PACKAGE_SYSTEM_PROGRESS_LONGS; infos++) {
		infoblock[infos] = info[infos];
	}

	gtk_signal_emit (GTK_OBJECT (system),
			 signals [PROGRESS],
			 op, 
			 package,
			 infoblock,
			 &result);

	g_free (infoblock);
	return result;
}

gboolean 
trilobite_package_system_emit_failed (TrilobitePackageSystem *system, 
				      TrilobitePackageSystemOperation op, 
				      const PackageData *package)
{
	gboolean result = TRUE;
	EPS_API (system);

	gtk_signal_emit (GTK_OBJECT (system),
			 signals [FAILED],
			 op, 
			 package,
			 &result);

	return result;
}

gboolean 
trilobite_package_system_emit_end (TrilobitePackageSystem *system, 
				   TrilobitePackageSystemOperation op, 
				   const PackageData *package)
{
	gboolean result = TRUE;
	EPS_API (system);

	gtk_signal_emit (GTK_OBJECT (system),
			 signals [END],
			 op, 
			 package,
			 &result);

	return result;
}

TrilobitePackageSystemDebug 
trilobite_package_system_get_debug (TrilobitePackageSystem *system)
{
	if (system->private) {
		return system->private->debug;
	} else {
		return 0;
	}
}

void                    
trilobite_package_system_set_debug (TrilobitePackageSystem *system, 
				    TrilobitePackageSystemDebug d)
{
	EPS_API (system);
	system->private->debug = d;
}

/*****************************************
  GTK+ object stuff
*****************************************/

static void
trilobite_package_system_finalize (GtkObject *object)
{
	TrilobitePackageSystem *system;

	g_return_if_fail (object != NULL);
	g_return_if_fail (TRILOBITE_PACKAGE_SYSTEM (object));

	system = TRILOBITE_PACKAGE_SYSTEM (object);

	if (GTK_OBJECT_CLASS (trilobite_package_system_parent_class)->finalize) {
		GTK_OBJECT_CLASS (trilobite_package_system_parent_class)->finalize (object);
	}
}

static void
trilobite_package_system_class_initialize (TrilobitePackageSystemClass *klass) 
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass*)klass;
	object_class->finalize = trilobite_package_system_finalize;
	
	trilobite_package_system_parent_class = gtk_type_class (gtk_object_get_type ());

	signals[START] = 
		gtk_signal_new ("start",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (TrilobitePackageSystemClass, start),
				trilobite_package_system_marshal_BOOL__ENUM_POINTER,
				GTK_TYPE_BOOL, 2, 
				GTK_TYPE_ENUM, GTK_TYPE_POINTER);	
	signals[END] = 
		gtk_signal_new ("end",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (TrilobitePackageSystemClass, end),
				trilobite_package_system_marshal_BOOL__ENUM_POINTER,
				GTK_TYPE_BOOL, 2, 
				GTK_TYPE_ENUM, GTK_TYPE_POINTER);	
	signals[PROGRESS] = 
		gtk_signal_new ("progress",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (TrilobitePackageSystemClass, progress),
				trilobite_package_system_marshal_BOOL__ENUM_POINTER_POINTER,
				GTK_TYPE_BOOL, 3, 
				GTK_TYPE_ENUM, GTK_TYPE_POINTER, GTK_TYPE_POINTER);
	signals[FAILED] = 
		gtk_signal_new ("failed",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (TrilobitePackageSystemClass, failed),
				trilobite_package_system_marshal_BOOL__ENUM_POINTER,
				GTK_TYPE_BOOL, 2, 
				GTK_TYPE_ENUM, GTK_TYPE_POINTER);	
	gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);

	klass->start = NULL;
	klass->progress = NULL;
	klass->failed = NULL;
	klass->end = NULL;
}

static void
trilobite_package_system_initialize (TrilobitePackageSystem *system) {
	g_assert (system!=NULL); 
	g_assert (TRILOBITE_IS_PACKAGE_SYSTEM (system));
	
	system->private = g_new0 (TrilobitePackageSystemPrivate, 1);
	system->err = NULL;
}

GtkType
trilobite_package_system_get_type() {
	static GtkType system_type = 0;

	/* First time it's called ? */
	if (!system_type)
	{
		static const GtkTypeInfo system_info =
		{
			"TrilobitePackageSystem",
			sizeof (TrilobitePackageSystem),
			sizeof (TrilobitePackageSystemClass),
			(GtkClassInitFunc) trilobite_package_system_class_initialize,
			(GtkObjectInitFunc) trilobite_package_system_initialize,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		system_type = gtk_type_unique (gtk_object_get_type (), &system_info);
	}

	return system_type;
}

/* 
   This is the real constructor 
*/
TrilobitePackageSystem *
trilobite_package_system_new_real ()
{
	TrilobitePackageSystem *system;

	system = TRILOBITE_PACKAGE_SYSTEM (gtk_object_new (TYPE_TRILOBITE_PACKAGE_SYSTEM, NULL));

	gtk_object_ref (GTK_OBJECT (system));
	gtk_object_sink (GTK_OBJECT (system));

	return system;
}

/* 
   This lets the user create a packagesystem with a specific 
   id 
*/
TrilobitePackageSystem *
trilobite_package_system_new_with_id (TrilobitePackageSystemId id, GList *roots)
{
	TrilobitePackageSystem *result;
	
	result = trilobite_package_system_load_implementation (id, roots);

	/* If we failed (eg. unsupported system id), return
	   an empty object */
	if (result == NULL) {
		result = trilobite_package_system_new_real ();
	}

	return result;
}

/*
  Autodetect distribution and creates
  an instance of a TrilobitePackageSystem with the appropriate
  type
 */
TrilobitePackageSystem *
trilobite_package_system_new (GList *roots) 
{
	return trilobite_package_system_new_with_id (trilobite_package_system_suggest_id (), roots);
}

/* Marshal functions */

typedef gboolean (*GtkSignal_BOOL__ENUM_POINTER_POINTER) (GtkObject *object,
							  gint arg1,
							  gpointer arg2,
							  gpointer arg3,
							  gpointer user_data);

void trilobite_package_system_marshal_BOOL__ENUM_POINTER_POINTER (GtkObject *object,
								  GtkSignalFunc func,
								  gpointer func_data, 
								  GtkArg *args)
{
	GtkSignal_BOOL__ENUM_POINTER_POINTER rfunc;
	gboolean *result;

	result = GTK_RETLOC_BOOL (args[3]);
	rfunc = (GtkSignal_BOOL__ENUM_POINTER_POINTER)func;
	(*result) = (*rfunc) (object,
			      GTK_VALUE_ENUM (args[0]),
			      GTK_VALUE_POINTER (args[1]),
			      GTK_VALUE_POINTER (args[2]),
			      func_data);
}

typedef gboolean (*GtkSignal_BOOL__ENUM_POINTER) (GtkObject *object,
						  gint arg1,
						  gpointer arg2,
						  gpointer user_data);

void trilobite_package_system_marshal_BOOL__ENUM_POINTER (GtkObject *object,
							  GtkSignalFunc func,
							  gpointer func_data, 
							  GtkArg *args)
{
	GtkSignal_BOOL__ENUM_POINTER rfunc;
	gboolean *result;
	
	rfunc = (GtkSignal_BOOL__ENUM_POINTER)func;
	result = GTK_RETLOC_BOOL (args[2]);
	(*result) = (*rfunc) (object,
			      GTK_VALUE_ENUM (args[0]),
			      GTK_VALUE_POINTER (args[1]),
			      func_data);
}
