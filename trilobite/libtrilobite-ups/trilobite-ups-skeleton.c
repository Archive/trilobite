/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8; tab-width: 8 -*- */
/* 
 * Copyright (C) 2000, 2001 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Eskil Heyn Olsen <eskil@eazel.com>
 */

#include <config.h>
#include <gnome.h>
#include "trilobite-ups-skeleton.h"
#include "trilobite-ups-private.h"
#include <libtrilobite/trilobite-core-utils.h>

TrilobitePackageSystem* trilobite_package_system_implementation (GList*);

/* This is the parent class pointer */
static TrilobitePackageSystemClass *trilobite_package_system_skeleton_parent_class;

static PackageData*
trilobite_package_system_skeleton_load_package (TrilobitePackageSystemSkeleton *system,
						PackageData *in_package,
						const char *filename,
						int detail_level)
{
	PackageData *result = NULL;
	g_assert (system != NULL);
	g_assert (TRILOBITE_IS_PACKAGE_SYSTEM_SKELETON (system));
	trilobite_debug ("trilobite_package_system_skeleton_load_package");
	/* Code Here */
	return result;
}

static GList*               
trilobite_package_system_skeleton_query (TrilobitePackageSystemSkeleton *system,
					 const char *dbpath,
					 const gpointer key,
					 TrilobitePackageSystemQueryEnum flag,
					 int detail_level)
{
	GList *result = NULL;
	g_assert (system != NULL);
	g_assert (TRILOBITE_IS_PACKAGE_SYSTEM_SKELETON (system));
	trilobite_debug ("trilobite_package_system_skeleton_query");
	/* Code Here */
	return result;
}

static void                 
trilobite_package_system_skeleton_install (TrilobitePackageSystemSkeleton *system, 
					   const char *dbpath,
					   GList* packages,
					   unsigned long flags)
{
	g_assert (system != NULL);
	g_assert (TRILOBITE_IS_PACKAGE_SYSTEM_SKELETON (system));
	trilobite_debug ("trilobite_package_system_skeleton_install");
	/* Code Here */
}

static void                 
trilobite_package_system_skeleton_uninstall (TrilobitePackageSystemSkeleton *system, 
					     const char *dbpath,
					     GList* packages,
					     unsigned long flags)
{
	g_assert (system != NULL);
	g_assert (TRILOBITE_IS_PACKAGE_SYSTEM_SKELETON (system));
	trilobite_debug ("trilobite_package_system_skeleton_uninstall");
	/* Code Here */
}

static gboolean
trilobite_package_system_skeleton_verify (TrilobitePackageSystemSkeleton *system, 
					  GList* packages)
{
	g_assert (system != NULL);
	g_assert (TRILOBITE_IS_PACKAGE_SYSTEM_SKELETON (system));
	trilobite_debug ("trilobite_package_system_skeleton_verify");
	/* Code Here */
	return FALSE;
}

static int
trilobite_package_system_skeleton_compare_version (TrilobitePackageSystemSkeleton *system,
						   const char *a,
						   const char *b)
{
	g_assert (system != NULL);
	g_assert (TRILOBITE_IS_PACKAGE_SYSTEM_SKELETON (system));
	trilobite_debug ("trilobite_package_system_skeleton_compare_version");
	/* Code Here */
	return 0;
}


/*****************************************
  GTK+ object stuff
*****************************************/

static void
trilobite_package_system_skeleton_finalize (GtkObject *object)
{
	TrilobitePackageSystemSkeleton *system;

	g_return_if_fail (object != NULL);
	g_return_if_fail (TRILOBITE_PACKAGE_SYSTEM_SKELETON (object));

	system = TRILOBITE_PACKAGE_SYSTEM_SKELETON (object);

	if (GTK_OBJECT_CLASS (trilobite_package_system_skeleton_parent_class)->finalize) {
		GTK_OBJECT_CLASS (trilobite_package_system_skeleton_parent_class)->finalize (object);
	}
}

static void
trilobite_package_system_skeleton_class_initialize (TrilobitePackageSystemSkeletonClass *klass) 
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass*)klass;
	object_class->finalize = trilobite_package_system_skeleton_finalize;
	
	trilobite_package_system_skeleton_parent_class = gtk_type_class (trilobite_package_system_get_type ());
}

static void
trilobite_package_system_skeleton_initialize (TrilobitePackageSystemSkeleton *system) {
	g_assert (system != NULL);
	g_assert (TRILOBITE_IS_PACKAGE_SYSTEM_SKELETON (system));
}

GtkType
trilobite_package_system_skeleton_get_type() {
	static GtkType system_type = 0;

	/* First time it's called ? */
	if (!system_type)
	{
		static const GtkTypeInfo system_info =
		{
			"TrilobitePackageSystemSkeleton",
			sizeof (TrilobitePackageSystemSkeleton),
			sizeof (TrilobitePackageSystemSkeletonClass),
			(GtkClassInitFunc) trilobite_package_system_skeleton_class_initialize,
			(GtkObjectInitFunc) trilobite_package_system_skeleton_initialize,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		system_type = gtk_type_unique (trilobite_package_system_get_type (), &system_info);
	}

	return system_type;
}

TrilobitePackageSystemSkeleton *
trilobite_package_system_skeleton_new (GList *dbpaths) 
{
	TrilobitePackageSystemSkeleton *system;

	system = TRILOBITE_PACKAGE_SYSTEM_SKELETON (gtk_object_new (TYPE_TRILOBITE_PACKAGE_SYSTEM_SKELETON, NULL));

	gtk_object_ref (GTK_OBJECT (system));
	gtk_object_sink (GTK_OBJECT (system));

	return system;
}

TrilobitePackageSystem*
trilobite_package_system_implementation (GList *dbpaths)
{
	TrilobitePackageSystem *result;

	trilobite_debug ("trilobite_package_system_implementation (skeleton)");

	result = TRILOBITE_PACKAGE_SYSTEM (trilobite_package_system_skeleton_new (dbpaths));
	
	result->private->load_package
		= (TrilobitePackageSytemLoadPackageFunc)trilobite_package_system_skeleton_load_package;

	result->private->query
		= (TrilobitePackageSytemQueryFunc)trilobite_package_system_skeleton_query;

	result->private->install
		= (TrilobitePackageSytemInstallFunc)trilobite_package_system_skeleton_install;

	result->private->uninstall
		= (TrilobitePackageSytemUninstallFunc)trilobite_package_system_skeleton_uninstall;

	result->private->verify
		= (TrilobitePackageSytemVerifyFunc)trilobite_package_system_skeleton_verify;

	result->private->compare_version
		= (TrilobitePackageSystemCompareVersionFunc)trilobite_package_system_skeleton_compare_version;

	return result;
}
