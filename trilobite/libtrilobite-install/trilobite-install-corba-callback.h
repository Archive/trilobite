/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8; tab-width: 8 -*- */
/* 
 * Copyright (C) 2000, 2001 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Eskil Heyn Olsen <eskil@eazel.com>
 */

#ifndef TRILOBITE_INSTALL_CORBA_CALLBACK_H
#define TRILOBITE_INSTALL_CORBA_CALLBACK_H 

#include <libgnome/gnome-defs.h>
#include "bonobo.h"
#include "trilobite-install.h"

#include <libtrilobite-ups/trilobite-ups-types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define TYPE_TRILOBITE_INSTALL_CALLBACK \
	(trilobite_install_callback_get_type ())
#define TRILOBITE_INSTALL_CALLBACK(obj) \
	(GTK_CHECK_CAST ((obj), TYPE_TRILOBITE_INSTALL_CALLBACK, TrilobiteInstallCallback))
#define TRILOBITE_INSTALL_CALLBACK_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST ((klass), TYPE_TRILOBITE_INSTALL_CALLBACK, TrilobiteInstallCallbackClass))
#define TRILOBITE_IS_INSTALL_CALLBACK(obj) \
	(GTK_CHECK_TYPE ((obj), TYPE_TRILOBITE_INSTALL_CALLBACK))
#define TRILOBITE_IS_INSTALL_CALLBACK_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE ((klass), TYPE_TRILOBITE_INSTALL_CALLBACK))

typedef struct _TrilobiteInstallCallback TrilobiteInstallCallback;
typedef struct _TrilobiteInstallCallbackClass TrilobiteInstallCallbackClass;

typedef enum TrilobiteInstallCallbackOperation {
	TrilobiteInstallCallbackOperation_INSTALL,
	TrilobiteInstallCallbackOperation_UNINSTALL,
	TrilobiteInstallCallbackOperation_REVERT
} TrilobiteInstallCallbackOperation;

struct _TrilobiteInstallCallbackClass 
{
	BonoboObjectClass parent_class;

	/* Called during the download of a file */
	void (*download_progress) (TrilobiteInstallCallback *service, const char *name, int amount, int total);

	/* Called after download and before (un)install_progress */
	gboolean (*preflight_check) (TrilobiteInstallCallback *service, 
				     TrilobiteInstallCallbackOperation op,
				     const GList *packages,
				     int total_size, 
				     int num_packages);

	/* Called after (un)install progress and done, only if
	   result is good */
	gboolean (*save_transaction) (TrilobiteInstallCallback *service, 
				      TrilobiteInstallCallbackOperation op,
				      const GList *packages);

	/* Called during install of a package */
	void (*install_progress)  (TrilobiteInstallCallback *service, 
				   const PackageData *pack, 
				   int package_num, int num_packages, 
				   int package_size_completed, int package_size_total,
				   int total_size_completed, int total_size);
	/* Called during uninstall of a package */
	void (*uninstall_progress)  (TrilobiteInstallCallback *service, 
				     const PackageData *pack, 
				     int package_num, int num_packages, 
				     int package_size_completed, int package_size_total,
				     int total_size_completed, int total_size);

	/* Called when a package is undergoing the different checks */
	void (*file_conflict_check)(TrilobiteInstallCallback *service, const PackageData *package);
	void (*file_uniqueness_check)(TrilobiteInstallCallback *service, const PackageData *package);
	void (*feature_consistency_check)(TrilobiteInstallCallback *service, const PackageData *package);

	/* Called when a dependency check is being resolved */
	void (*dependency_check) (TrilobiteInstallCallback *service,
				  const PackageData *package,
				  const PackageData *needed);

	/* Called when a file could not be downloaded */
	void (*download_failed) (TrilobiteInstallCallback *service, char *name);
	/* Called when a package install request fails, eg. for dependency reasons.
	   pd->soft_depends and pd->breaks can be traversed to see why the package
	   failed */
	void (*install_failed) (TrilobiteInstallCallback *service, PackageData *pd);
	/* Same as install_failed... */
	void (*uninstall_failed) (TrilobiteInstallCallback *service, PackageData *pd);

	/* Emitted when the md5 checksum fails for a package file.
	   pd contains the md5 that the package file should have,
	   and actual_md5 contains the actual md5 of the package file */
	void (*md5_check_failed) (TrilobiteInstallCallback *service, 
				  const PackageData *pd, 
				  const char *actual_md5);

	/* Called after installation to determine if the RPM files should be deleted */
	gboolean (*delete_files) (TrilobiteInstallCallback *service);

	/* Called when the operation is complete */
	void (*done) (gboolean result);

	gpointer servant_vepv;
};

struct _TrilobiteInstallCallback
{
	BonoboObject parent;
	GNOME_Trilobite_InstallCallback cb;

	BonoboObjectClient *installservice_bonobo;
	GNOME_Trilobite_Install installservice_corba;
};

/* Create a new trilobite-install-callback object */
TrilobiteInstallCallback      *trilobite_install_callback_new (void);
/* Destroy the trilobite-install-callback object */
void                           trilobite_install_callback_unref    (GtkObject *object);

/* Request the installation of a set of packages in categories.
   If categories = NULL, the service tries to fetch the packagelist
   from the server. See the trilobite-install.idl file for
   the function to specify packagelist name */
void trilobite_install_callback_install_packages (TrilobiteInstallCallback *service, 
						  GList *categories,
						  const char *root,
						  CORBA_Environment *ev);

/* Request the uninstallation of a set of packages */
void trilobite_install_callback_uninstall_packages (TrilobiteInstallCallback *service, 
						    GList *categories,
						    const char *root,
						    CORBA_Environment *ev);

GList* trilobite_install_callback_simple_query (TrilobiteInstallCallback *service, 
						const char* query,
						const char *root,
						CORBA_Environment *ev);

void trilobite_install_callback_revert_transaction (TrilobiteInstallCallback *service, 
						    const char *xmlfile,
						    const char *root,
						    CORBA_Environment *ev);

void trilobite_install_callback_delete_files (TrilobiteInstallCallback *service,
					      CORBA_Environment *ev);

/* Stuff */
GtkType                                   trilobite_install_callback_get_type   (void);
POA_GNOME_Trilobite_InstallCallback__epv *trilobite_install_callback_get_epv (void);
GNOME_Trilobite_InstallCallback           trilobite_install_callback_create_corba_object (BonoboObject *service);
GNOME_Trilobite_Install                   trilobite_install_callback_corba_objref (TrilobiteInstallCallback *service);
BonoboObjectClient                       *trilobite_install_callback_bonobo (TrilobiteInstallCallback *service);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* TRILOBITE_INSTALL_CORBA_CALLBACK_H */
