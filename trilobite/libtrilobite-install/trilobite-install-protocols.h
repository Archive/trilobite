/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* 
 * Copyright (C) 2000 Eazel, Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: J Shane Culpepper <pepper@eazel.com>
 */

/* eazel-install - services command line install/update/uninstall
 * component.  This program will parse the eazel-services-config.xml
 * file and install a services generated package-list.xml.
 */

#ifndef TRILOBITE_INSTALL_PROTOCOLS_H
#define TRILOBITE_INSTALL_PROTOCOLS_H

#include <libtrilobite-ups/trilobite-ups-types.h>
#include <libtrilobite-install/trilobite-install-public.h>

gboolean trilobite_install_fetch_file (TrilobiteInstall *service,
				       char *url, 
				       const char *file_to_report,
				       const char *target_file,
				       const PackageData *package);

gboolean trilobite_install_fetch_package (TrilobiteInstall *service,
					  PackageData *package);

void trilobite_install_fetch_definitive_category_info (TrilobiteInstall *service,
						       CategoryData *category);

void dump_packages (GList *packages);

#endif /* TRILOBITE_INSTALL_PROTOCOLS_H */
