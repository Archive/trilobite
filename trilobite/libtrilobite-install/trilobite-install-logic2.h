/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* 
 * Copyright (C) 2000, 2001 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Eskil Heyn Olsen <eskil@eazel.com>
 */

#ifndef TRILOBITE_INSTALL_LOGIC2_H
#define TRILOBITE_INSTALL_LOGIC2_H

#include <libtrilobite-ups/trilobite-ups-types.h>
#include <libtrilobite-install/trilobite-install-protocols.h>
#include <libtrilobite-install/trilobite-install-public.h>
#include <libtrilobite-ups/trilobite-ups.h>

/* FIXME: these should not be exported once eazel-install-logic is being
   phased out */
gboolean check_md5_on_files (TrilobiteInstall *service, GList *packages);
TrilobiteInstallStatus trilobite_install_check_existing_packages (TrilobiteInstall *service, 
								  PackageData *pack);
unsigned long trilobite_install_get_total_size_of_packages (TrilobiteInstall *service,
							    const GList *packages);

TrilobiteInstallOperationStatus install_packages (TrilobiteInstall *service, GList *categories);
TrilobiteInstallOperationStatus uninstall_packages (TrilobiteInstall *service, GList *categories);
TrilobiteInstallOperationStatus revert_transaction (TrilobiteInstall *service, GList *packages);

#endif /* TRILOBITE_INSTALL_LOGIC_H */
