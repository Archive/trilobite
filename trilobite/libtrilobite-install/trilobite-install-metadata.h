/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8; tab-width: 8 -*- */
/* 
 * Copyright (C) 2000, 2001 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: J Shane Culpepper <pepper@eazel.com>
 */

#ifndef TRILOBITE_INSTALL_METADATA_H
#define TRILOBITE_INSTALL_METADATA_H

#include <libtrilobite-ups/trilobite-ups-types.h>
#include <libtrilobite-ups/trilobite-ups-softcat.h>

InstallOptions *init_default_install_configuration (void);
TransferOptions *init_default_transfer_configuration (void);
void transferoptions_destroy (TransferOptions *topts);
void installoptions_destroy (InstallOptions *iopts);
void trilobite_install_configure_softcat (TrilobiteSoftCat *softcat);
gboolean trilobite_install_configure_check_jump_after_install (char **url);
gboolean trilobite_install_configure_use_local_db (void);

#endif /* TRILOBITE_INSTALL_METADATA_H */
