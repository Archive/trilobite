/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8; tab-width: 8 -*- */
/* 
 * Copyright (C) 2000, 2001 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Eskil Heyn Olsen <eskil@eazel.com>
 */

#ifndef TRILOBITE_INSTALL_QUERY_H
#define TRILOBITE_INSTALL_QUERY_H

#include <libtrilobite-ups/trilobite-ups-types.h>
#include <libtrilobite-install/trilobite-install-public.h>

typedef enum {
	TI_SIMPLE_QUERY_OWNS,
	TI_SIMPLE_QUERY_PROVIDES,
	TI_SIMPLE_QUERY_REQUIRES,
	TI_SIMPLE_QUERY_MATCHES
} TrilobiteInstallSimpleQueryEnum;

/* Performs a simple query for "input", using
   the enum flag.
   
   neglists is the number of GList* provided to the function. These
   lists should contain pacakage that should _not_ be returned in the
   result. So normally you will just use it like :
   
   GList *matches = trilobite_install_simple_query (service, "somepackage", 
                                                    TI_SIMPLE_QUERY_MATCHES, 0, NULL);
*/
   
GList* trilobite_install_simple_query (TrilobiteInstall *service, 
				       const char *input, 
				       TrilobiteInstallSimpleQueryEnum flag, 
				       int neglists, ...);

#endif /* TRILOBITE_INSTALL_QUERY_H */
