/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8; tab-width: 8 -*- */
/* 
 * Copyright (C) 2000, 2001 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Eskil Heyn Olsen <eskil@eazel.com>
 */


/*
  How to use :
  Create an instance e of TrilobiteInstallProblem pr. complete operation you're going to try.
  (Eg. installing nautilus). Whenever the install_failed signal is called,
  call trilobite_install_problem_tree_to_case (e, package). This will return
  a list of problems (you can call several times before pr. operation attempt.
  Note, that every case is only reported pr tree. So if you're installing package
  A and B, and they both cause case C, you can get case C twice.
  Now do with these cases as you want, optionally call
  trilobite_install_problem_handle_cases untill the problem list is empty. If
  you do not use trilobite_install_problem_handle_cases, but manually prioritize the
  problems, be sure to call trilobite_install_problem_step between operation executions.
 */

#ifndef TRILOBITE_INSTALL_PROBLEM_H
#define TRILOBITE_INSTALL_PROBLEM_H

#include <libtrilobite-ups/trilobite-ups-types.h>
#ifdef TRILOBITE_INSTALL_NO_CORBA
#include "trilobite-install-public.h"
#else /* TRILOBITE_INSTALL_NO_CORBA */
#include "trilobite-install-corba-callback.h"
#endif /* TRILOBITE_INSTALL_NO_CORBA */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define TYPE_TRILOBITE_INSTALL_PROBLEM \
	(trilobite_install_problem_get_type ())
#define TRILOBITE_INSTALL_PROBLEM(obj) \
	(GTK_CHECK_CAST ((obj), TYPE_TRILOBITE_INSTALL_PROBLEM, TrilobiteInstallProblem))
#define TRILOBITE_INSTALL_PROBLEM_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST ((klass), TYPE_TRILOBITE_INSTALL_PROBLEM, TrilobiteInstallProblemClass))
#define TRILOBITE_IS_INSTALL_PROBLEM(obj) \
	(GTK_CHECK_TYPE ((obj), TYPE_TRILOBITE_INSTALL_PROBLEM))
#define TRILOBITE_IS_INSTALL_PROBLEM_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE ((klass), TYPE_TRILOBITE_INSTALL_PROBLEM))
	
typedef enum _TrilobiteInstallProblemContinueFlag TrilobiteInstallProblemContinueFlag;
typedef enum _TrilobiteInstallProblemEnum TrilobiteInstallProblemEnum;
typedef struct _TrilobiteInstallProblemCase TrilobiteInstallProblemCase;
typedef struct _TrilobiteInstallProblemClass TrilobiteInstallProblemClass;
typedef struct _TrilobiteInstallProblem TrilobiteInstallProblem;

/* Please, in switches on this enum, do not handle
   default, but always handle all the enums.
   That ways you cannot add a enum without
   getting compiler borkage to let you know you're
   about to fuck up. */

enum _TrilobiteInstallProblemEnum {
	TI_PROBLEM_BASE = 0,
	TI_PROBLEM_UPDATE,                 /* package is in the way, update or remove */
	TI_PROBLEM_CONTINUE_WITH_FLAG,     /* Ok, just barge ahead with the operation */
	TI_PROBLEM_REMOVE,                 /* Remove a package might help */
	TI_PROBLEM_FORCE_INSTALL_BOTH,     /* two packages are fighting it out, install both ? */	
	TI_PROBLEM_CASCADE_REMOVE,         /* Delete a lot of stuff */
        TI_PROBLEM_FORCE_REMOVE,           /* Okaeh, force remove the package */
	TI_PROBLEM_CANNOT_SOLVE,           /* The water is too deep */
        TI_PROBLEM_INCONSISTENCY           /* There's an inconsistency in the db */ 
};

enum _TrilobiteInstallProblemContinueFlag {
	TrilobiteInstallProblemContinueFlag_FORCE = 0x1,
	TrilobiteInstallProblemContinueFlag_UPGRADE = 0x2,
	TrilobiteInstallProblemContinueFlag_DOWNGRADE = 0x4
};

struct _TrilobiteInstallProblemCase {
	TrilobiteInstallProblemEnum problem;
	gboolean file_conflict;
	union {
		struct {
			TrilobiteInstallProblemContinueFlag flag;
		} continue_with_flag;
		struct {
			PackageData *pack;
		} update;
		struct {
			PackageData *pack_1;
			PackageData *pack_2;
		} force_install_both;
		struct {
			PackageData *pack;
		} remove;
		struct {
			PackageData *pack;
		} force_remove;
		struct {
			TrilobiteInstallProblemCase *problem;
		} cannot_solve;
		struct {
			TrilobiteInstallProblemCase *problem;
		} force;
		struct {
			GList *packages;
		} cascade;
	} u;
};

TrilobiteInstallProblemCase *trilobite_install_problem_case_new (TrilobiteInstallProblemEnum problem_type);
void trilobite_install_problem_case_destroy (TrilobiteInstallProblemCase *pcase);
void trilobite_install_problem_case_list_destroy (GList *list);

struct _TrilobiteInstallProblemClass {
	GtkObjectClass parent_class;
};

struct _TrilobiteInstallProblem {
	/* Parent stuff */
	GtkObject parent;

	/* Private vars */
	/* The fist set of attributes are lists of previously encountered problems.
	   If I'm about to add a problem with the same stuff, don't,
	   but "upgrade it".
	   Eg for install:
	   TI_PROBLEM_UPDATE -> TI_PROBLEM_REMOVE
	   TI_PROBLEM_REMOVE -> TI_PROBLEM_CASCADE_REMOVE
	   TI_PROBLEM_CASCADE_REMOVE -> TI_PROBLEM_FORCE_REMOVE
	   TI_PROBLEM_FORCE_REMOVE -> TI_PROBLEM_CONTINUE_WITH_FORCE
	   TI_PROBLEM_CONTINUE_WITH_FORCE -> TI_PROBLEM_CANNOT_SOLVE

	   Uninstall will start at TI_PROBLEM_CASCADE_REMOVE
	   for uninstall, TI_PROBLEM_CONTINUE_WITH_FORCE == TI_PROBLEM_FORCE_REMOVE

	   This logic is implemented in the add_*_case functions and
	   in trilobite_install_problem_step_problem.
	*/

	GHashTable *attempts;
	/* This is the list of problems currently being build,
	   called trilobite_install_problem_step moves these into
	   atttempts */
	GHashTable *pre_step_attempts;
	gboolean in_step_problem_mode;
};

TrilobiteInstallProblem *trilobite_install_problem_new (void);
GtkType trilobite_install_problem_get_type (void);       

void trilobite_install_problem_step (TrilobiteInstallProblem *problem);

/* This returns a GList<TrilobiteInstallProblemCase> list containing the 
   encountered problems in the given PackageData tree */
void trilobite_install_problem_tree_to_case (TrilobiteInstallProblem *problem,
					     PackageData *pack,
					     gboolean uninstall,
					     GList **output);
	
/* This returns a GList<char*> list containing the 
   encountered problems in the given PackageData tree */
GList * trilobite_install_problem_tree_to_string (TrilobiteInstallProblem *problem,
						  PackageData *pack,
						  gboolean uninstall);

/* This returns a GList<char*> list containing the 
   encountered problems in the given PackageData tree */
GList * trilobite_install_problem_cases_to_string (TrilobiteInstallProblem *problem,
						   GList *cases);

/* Like above, but only returns the package names, and only for packages that are about to be removed */
GList * trilobite_install_problem_cases_to_package_names (TrilobiteInstallProblem *problem,
							  GList *cases);

/* This lets you know which type of problems the next call
   to trilobite_install_handle_cases will handle. So if eg. 
   the next type is TI_PROBLEM_FORCE_REMOVE, you can alert the user */
TrilobiteInstallProblemEnum 
trilobite_install_problem_find_dominant_problem_type (TrilobiteInstallProblem *problem,
						      GList *problems);

/*
  If you're not satisfied with the given dominant problem,
  use this to get a peek at what the next step will be.
  If you like the result, you can g_list_free the "problems", 
  if not, g_list_free the result. Do _not_ destroy the 
  problem's themselves.
  If you like the result, pass them to trilobite_install_problem_use_set 
*/
GList *
trilobite_install_problem_step_problem (TrilobiteInstallProblem *problem,
					TrilobiteInstallProblemEnum problem_type,
					GList *problems);

void
trilobite_install_problem_use_set  (TrilobiteInstallProblem *problem,
				    GList *problems);

/* Given a series of problems, it will remove
   the most important ones, and execute them given 
   a TrilobiteInstall service object */
void trilobite_install_problem_handle_cases (TrilobiteInstallProblem *problem,					 
#ifdef TRILOBITE_INSTALL_NO_CORBA
					     TrilobiteInstall *service,
#else /* TRILOBITE_INSTALL_NO_CORBA */
					     TrilobiteInstallCallback *service,
#endif /* TRILOBITE_INSTALL_NO_CORBA */

					     GList **problems,
					     GList **install_categories,
					     GList **uninstall_categories,
					     char *root);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* TRILOBITE_INSTALL_PROBLEM_H */
