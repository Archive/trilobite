/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Copyright (C) 2000, 2001 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Eskil Heyn Olsen  <eskil@eazel.com>
 *          Robey Pointer <robey@eazel.com>
 */

#include "trilobite-install-logic2.h"
#include "trilobite-install-public.h"
#include "trilobite-install-private.h"

#include <libtrilobite/trilobite-core-utils.h>
#include <libtrilobite/trilobite-md5-tools.h>

#include <eel/eel-debug.h>

#ifndef TRILOBITE_INSTALL_NO_CORBA
#include <libtrilobite/libtrilobite.h>
#else
#include <libtrilobite/trilobite-root-helper.h>
#include <libtrilobite/trilobite-i18n.h>
#endif

#include <glib.h>
#include <string.h>

/*
  0x1 enables post check_dependencies treedumps
  0x2 enables post dedupe tree dumps
  0x4 enables random spewing
  0x8 enables dumping the final tree
 */
#define EI2_DEBUG 0xFF

#define PATCH_FOR_SOFTCAT_BUG 1
#define MUST_HAVE PACKAGE_FILL_NO_DIRS_IN_PROVIDES
#define UPDATE_MUST_HAVE PACKAGE_FILL_NO_DEPENDENCIES|PACKAGE_FILL_NO_TEXT|PACKAGE_FILL_NO_DIRS_IN_PROVIDES
#define OWNS_MUST_HAVE PACKAGE_FILL_NO_DEPENDENCIES|PACKAGE_FILL_NO_TEXT|PACKAGE_FILL_NO_DIRS_IN_PROVIDES
#define MODIFY_MUST_HAVE PACKAGE_FILL_NO_DEPENDENCIES|PACKAGE_FILL_NO_TEXT|PACKAGE_FILL_NO_DIRS_IN_PROVIDES

enum {
	DEPENDENCY_OK = 1,
	DEPENDENCY_NOT_OK = 2
};

/***********************************************************************************/
/* 
   this is a debug thing that'll dump the memory addresses of all packages
   in trees, and thereby let you manually check whether the dedupe worked or not.
*/
#if EI2_DEBUG & 0x10
static void
dump_tree (GList *packages)
{
	char *out;
	char **lines;
	int i;
	GList *packs = NULL;
	GList *iterator;
	
	for (iterator = packages; iterator; iterator = g_list_next (iterator)) {
		PackageData *pack = NULL;
		if (IS_PACKAGEDATA (iterator->data)) {
			pack = PACKAGEDATA (iterator->data);
		} else if (IS_PACKAGEDEPENDENCY (iterator->data)) {
			pack = PACKAGEDEPENDENCY (iterator->data)->package;
		} else {
			g_assert_not_reached ();
		}
		packs = g_list_prepend (packs, pack);
	}

	out = packagedata_dump_tree (packs, 2);
	lines = g_strsplit (out, "\n", 0);
	for (i = 0; lines[i] != NULL; i++) {
		trilobite_debug ("%s", lines[i]);
	}
	g_strfreev (lines);
	g_free (out);
	g_list_free (packs);
}
#endif


/***********************************************************************************/

/* This is code to check the md5 of downloaded packages */


gboolean
check_md5_on_files (TrilobiteInstall *service, 
		    GList *packages)
{
	gboolean result = TRUE;
	GList *iterator;
	GList *flat_packages;

	result = trilobite_install_lock_tmp_dir (service);

	if (!result) {
		g_warning (_("Failed to lock the downloaded file"));
		return FALSE;
	}

	flat_packages = flatten_packagedata_dependency_tree (packages);

	for (iterator = flat_packages; iterator; iterator = g_list_next (iterator)) {
		PackageData *pack = (PackageData*)iterator->data;
		
		if (pack->md5) {
			char pmd5[16];
			char md5[16];

			trilobite_md5_get_digest_from_file (pack->filename, md5);
			trilobite_md5_get_digest_from_md5_string (pack->md5, pmd5);

			if (memcmp (pmd5, md5, 16) != 0) {
				g_warning (_("MD5 mismatch, package %s may be compromised"), pack->name);
#if EI2_DEBUG & 0x4				
				/* get_readable_name is leaked */
				trilobite_debug ("read md5 from file %s", pack->filename);
				trilobite_debug ("for package %s", packagedata_get_readable_name (pack));
#endif
				trilobite_install_emit_md5_check_failed (service, 
									 pack, 
									 trilobite_md5_get_string_from_md5_digest (md5));
				result = FALSE;
			} else {
#if EI2_DEBUG & 0x4				
				trilobite_debug ("md5 match on %s", pack->name);
#endif
			}
		} else {
			g_message (_("No MD5 available for %s"), pack->name);
		}
	}	

	g_list_free (flat_packages);
	trilobite_install_unlock_tmp_dir (service);

	return result;
}

/***********************************************************************************/

/* This is code to removed failed trees from the tree */

void prune_failed_packages (TrilobiteInstall *service, GList **packages);
void prune_failed_packages_helper (TrilobiteInstall *service, PackageData *root, 
				   PackageData *pack, GList *packages, GList **path, GList **result);

typedef enum {
	PRUNE_ACTION_NORMAL,
	PRUNE_ACTION_ALLOW
} PruneAction;

void 
prune_failed_packages_helper (TrilobiteInstall *service, 
			      PackageData *root, 
			      PackageData *pack, 
			      GList *packages,
			      GList **path,
			      GList **result)
{
	GList *iterator;
	PruneAction action = PRUNE_ACTION_NORMAL;
	char *pack_name = packagedata_get_readable_name (pack);

#if EI2_DEBUG & 0x4
	trilobite_debug ("entering subpruner %p %s %s", 
			 pack, pack_name, 
			 packagedata_status_enum_to_str (pack->status));
#endif
	/* If package is already installed, check if the service
	   settings requires us to fail it */
	if (pack->status == PACKAGE_ALREADY_INSTALLED) {
		gboolean cancel_package;
		cancel_package = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (pack), "cancelled"));
#if EI2_DEBUG & 0x4
		trilobite_debug ("Package cancel status == %d", cancel_package);
#endif
		if (cancel_package == 0) {
			action = PRUNE_ACTION_ALLOW;
		}
	}

	/* If it's a suite and no dependencies, cancel it */
	if (packagedata_is_suite (pack) && 
	    g_list_length (pack->depends)==0 && 
	    pack->status == PACKAGE_PARTLY_RESOLVED) {
		pack->status = PACKAGE_ALREADY_INSTALLED;
	}

	/* Recursion check */
	if (g_list_find (*path, pack)) {
#if EI2_DEBUG & 0x4
		trilobite_debug ("... %p %s recurses ...", pack, pack_name);
#endif
		g_free (pack_name);
		return;
	}
	
	if (action == PRUNE_ACTION_NORMAL && pack->status == PACKAGE_PARTLY_RESOLVED) {
		action = PRUNE_ACTION_ALLOW;
	}

	if (action != PRUNE_ACTION_ALLOW) {
		char *root_name = packagedata_get_readable_name (root);
#if EI2_DEBUG & 0x4
		trilobite_debug ("subpruner kill root %p %s because of %p %s", 
				 root, root_name, pack, pack_name);
#endif
		if (g_list_find (*result, root)==NULL) {
			(*result) = g_list_prepend (*result, root);
		}
		g_free (pack_name);
		g_free (root_name);
	} else {
		g_free (pack_name);
		for (iterator = pack->depends; iterator; iterator = g_list_next (iterator)) {
			PackageDependency *dep = PACKAGEDEPENDENCY (iterator->data);
			(*path) = g_list_prepend (*path, pack);
			prune_failed_packages_helper (service, root, dep->package, packages, path, result);
			(*path) = g_list_remove (*path, pack);
		}
	}
}

/* If the calls to prune_failed_packages are removed, no
  install_failed signals are emitted, and the client gets the entire
  tree list.  This is for bug 5267 */

void 
prune_failed_packages (TrilobiteInstall *service, 
		       GList **packages)
{
	GList *iterator;
	GList *result = NULL;

	for (iterator = *packages; iterator; iterator = g_list_next (iterator)) {
		PackageData *pack = PACKAGEDATA (iterator->data);
		GList *path = NULL;
		prune_failed_packages_helper (service, pack, pack, *packages, &path, &result);
	}

	for (iterator = result; iterator; iterator = g_list_next (iterator)) {
		PackageData *pack = PACKAGEDATA (iterator->data);

#if EI2_DEBUG & 0x4
		trilobite_debug ("removing %p %s", pack, pack->name);
#endif
		(*packages) = g_list_remove (*packages, pack);
		trilobite_install_emit_install_failed (service, pack);
		gtk_object_unref (GTK_OBJECT (pack));
		/* FIXME: bugzilla.eazel.com 7654
		   If object ref reaches zero, it should be removed from the
		   dedupe hash */
	}
	if (g_list_length (*packages) == 0) {
#if EI2_DEBUG & 0x4
		trilobite_debug ("packages is empty after prune");		
#endif		
		g_list_free (*packages);
		(*packages) = NULL;
	}
	g_list_free (result);

#if EI2_DEBUG & 0x2
	dump_tree (*packages);
#endif
}


/***********************************************************************************/

/* Code to check already installed packages */

TrilobiteInstallStatus
trilobite_install_check_existing_packages (TrilobiteInstall *service, 
					   PackageData *pack)
{
	GList *existing_packages;
	GList *borked_packages = NULL;
	TrilobiteInstallStatus result;

#if EI2_DEBUG & 0x4
	trilobite_debug ("check_existing %p %s", pack, pack->name);
#endif
	result = TRILOBITE_INSTALL_STATUS_NEW_PACKAGE;
	/* query for existing package of same name */
	existing_packages = trilobite_package_system_query (service->private->package_system,
							    service->private->cur_root,
							    pack->name,
							    TRILOBITE_PACKAGE_SYSTEM_QUERY_MATCHES,
							    MODIFY_MUST_HAVE);

	/* If error, handle it */
	if (service->private->package_system->err) {
#if EI2_DEBUG & 0x4
		switch (service->private->package_system->err->e) {
		case TrilobitePackageSystemError_DB_ACCESS:
			if (strcmp (service->private->package_system->err->u.db_access.path, 
				    service->private->cur_root)==0) {
				trilobite_debug ("some package dbs is locked by another process", 
						 service->private->package_system->err->u.db_access.pid);
				pack->status = PACKAGE_PACKSYS_FAILURE;
				g_free (service->private->package_system->err);
				return TRILOBITE_INSTALL_STATUS_QUO;
				break;
			}
		}
#endif
	}
	if (existing_packages) {
		/* Get the existing package, set it's modify flag and add it */
		GList *iterator;
		PackageData *survivor = NULL;
		gboolean abort = FALSE;
		int res;

		if (g_list_length (existing_packages)>1) {
#if EI2_DEBUG & 0x4
			trilobite_debug ("there are %d existing packages called %s",
					 g_list_length (existing_packages),
					 pack->name);
#endif
			/* Verify them all, to find one that is not damaged. Mark the rest
			   as invalid so the client can suggest the user should delete them */
			for (iterator = existing_packages; iterator; iterator = g_list_next (iterator)) {
				PackageData *existing_package = PACKAGEDATA (iterator->data);
				char *name = packagedata_get_readable_name (existing_package);
				GList *foo = NULL;
				
				foo = g_list_prepend (foo, existing_package);
				if (trilobite_package_system_verify (service->private->package_system,
								     service->private->cur_root,
								     foo)) {
					/* I18N note: "%is is ok" in the sense that %s=package name and the
					   package is intact */
					g_message (_("%s is ok"), name);
					if (survivor == NULL) {
						survivor = existing_package;
					} else {						
						abort = TRUE;
					}
				} else {
					/* I18N note: "%is is not ok" in the sense that %s=package name and the
					   package is not intact */
					g_message ("%s is not ok", name);
					existing_package->status = PACKAGE_INVALID;
					/* I add the borked packages later, so they'll show up
					   earlier in the tree */
					borked_packages = g_list_prepend (borked_packages, existing_package);
				}
				g_list_free (foo);
				g_free (name);
			}
		} else {
			survivor = PACKAGEDATA (g_list_first (existing_packages)->data);
		}
		
		if (abort) {
			trilobite_debug ("*********************************************************");
			trilobite_debug ("This is a bad bad case, see bug 3511");
			trilobite_debug ("To circumvent this problem, as root, execute this command");
			trilobite_debug ("(which is dangerous by the way....)");
			trilobite_debug ("rpm -e --nodeps `rpm -q %s`", pack->name);
			
			g_list_free (borked_packages);

			/* Cancel the package, mark all the existing as invalid */			   
			pack->status = PACKAGE_CANCELLED;
			for (iterator = existing_packages; iterator; iterator = g_list_next (iterator)) {
				PackageData *existing_package = PACKAGEDATA (iterator->data);
				existing_package->status = PACKAGE_INVALID;
				packagedata_add_pack_to_modifies (pack, existing_package);
				gtk_object_unref (GTK_OBJECT (existing_package));
			}
		}

		if (abort==FALSE && survivor) {
			g_assert (pack->version);
			g_assert (survivor->version);

			res = trilobite_package_system_compare_version (service->private->package_system,
									pack->version, 
									survivor->version);

			if (survivor->epoch > 0) {
				g_warning ("modified package has epoch %d, new package has %d",
					   survivor->epoch,
					   pack->epoch);
				gtk_object_set_data (GTK_OBJECT (service->private->package_system),
						     "ignore-epochs", GINT_TO_POINTER (1));
			}
			
			/* check against minor version */
			if (res==0) {
#if EI2_DEBUG & 0x4
				trilobite_debug ("versions are equal (%s), comparing minors", pack->version);
#endif
				if (pack->minor && survivor->minor) {
#if EI2_DEBUG & 0x4
					trilobite_debug ("minors are %s for new and %s for installed)", 
							 pack->minor, survivor->minor);
#endif
					res = trilobite_package_system_compare_version (service->private->package_system,
											pack->minor, survivor->minor);
				} else if (!pack->minor && survivor->minor) {
					/* If the given packages does not have a minor,
					   but the installed has, assume we're fine */
					/* FIXME: bugzilla.eazel.com
					   This is a patch, it should be res=1, revert when
					   softcat is updated to have revisions for all packages 
					   (post PR3) */
					res = 1;
				} else {
					/* Eh, do nothing just to be safe */
					res = 0;
				}
			}

			/* Set the modify_status flag */
			if (res == 0) {
				survivor->modify_status = PACKAGE_MOD_UNTOUCHED;
			} else if (res > 0) {
				survivor->modify_status = PACKAGE_MOD_UPGRADED;
			} else {
				survivor->modify_status = PACKAGE_MOD_DOWNGRADED;
			}

			/* Calc the result */
			if (res == 0) {
				result = TRILOBITE_INSTALL_STATUS_QUO;
			} else if (res > 0) {
				result = TRILOBITE_INSTALL_STATUS_UPGRADES;
			} else {
				result = TRILOBITE_INSTALL_STATUS_DOWNGRADES;
			}
		
#if EI2_DEBUG & 0x4
			/* Debug output */ 
			switch (result) {
			case TRILOBITE_INSTALL_STATUS_QUO: {
				if (pack->minor && survivor->minor) {
					trilobite_debug (_("%s-%s version %s-%s already installed"), 
							 pack->name, pack->minor, 
							 survivor->version, survivor->minor);
				} else {
					trilobite_debug (_("%s version %s already installed"), 
							 pack->name, 
							 survivor->version);
				}
			} 
			break;
			case TRILOBITE_INSTALL_STATUS_UPGRADES: {
				/* This is certainly ugly as helll */
				if (pack->minor && survivor->minor) {
					trilobite_debug (_("%s upgrades from version %s-%s to %s-%s"),
							 pack->name, 
							 survivor->version, survivor->minor, 
							 pack->version, pack->minor);
				} else {
					trilobite_debug (_("%s upgrades from version %s-%s to %s-%s"),
							 pack->name, 
							 survivor->version, survivor->minor, 
							 pack->version, pack->minor);
				}
			}
			break;
			case TRILOBITE_INSTALL_STATUS_DOWNGRADES: {
				if (pack->minor && survivor->minor) {
					trilobite_debug (_("%s downgrades from version %s-%s to %s-%s"),
							 pack->name, 
							 survivor->version, survivor->minor, 
							 pack->version, pack->minor);
				} else {
					trilobite_debug (_("%s downgrades from version %s to %s"),
							 pack->name, 
							 survivor->version, 
							 pack->version);
				}
			}
			break;
			default:
				break;
			}
#endif		
/*
			if (g_list_find_custom (pack->modifies, 
						survivor->name,
						(GCompareFunc)trilobite_install_package_name_compare)) {
#if EI2_DEBUG & 0x4
				trilobite_debug ("%s already marked as modified", survivor->name);
#endif
				gtk_object_unref (GTK_OBJECT (survivor));
				survivor = NULL;
				continue;
			}
*/
		
			/* Set modifies list */
			if (result != TRILOBITE_INSTALL_STATUS_QUO) {
#if EI2_DEBUG & 0x4
				trilobite_debug ("%p %s modifies %p %s",
						 pack, pack->name,
						 survivor, survivor->name);
#endif

				packagedata_add_pack_to_modifies (pack, survivor);
				survivor->status = PACKAGE_RESOLVED;
			} 
			pack->status = PACKAGE_ALREADY_INSTALLED;
			gtk_object_unref (GTK_OBJECT (survivor));
		}

		/* Now add the borked packages */
		for (iterator = borked_packages; iterator; iterator = g_list_next (iterator)) {
			PackageData *existing_package = PACKAGEDATA (iterator->data);
			packagedata_add_pack_to_modifies (pack, existing_package);
			gtk_object_unref (GTK_OBJECT (existing_package));

		}
		g_list_free (borked_packages);

		/* Free the list structure from _simple_query */
		g_list_free (existing_packages);
	} else {
#if EI2_DEBUG & 0x4
		if (pack->minor) {
			trilobite_debug (_("%s installs version %s-%s"), 
					 pack->name, 
					 pack->version,
					 pack->minor);
		} else {
			trilobite_debug (_("%s installs version %s"), 
					 pack->name, 
					 pack->version);
		}

#endif
	}

	return result;
}

/***********************************************************************************/

/* Info collector */

typedef enum {
	NO_SOFTCAT_HIT = 0,
	SOFTCAT_HIT_OK  = 1,
	PACKAGE_SKIPPED = 2
} GetSoftCatResult;

void get_package_info (TrilobiteInstall *service, GList *packages);
GetSoftCatResult get_softcat_info (TrilobiteInstall *service, gpointer *ptr);

static void
add_to_dedupe_hash (TrilobiteInstall *service,
		    PackageData *package)
{
	if (g_hash_table_lookup (service->private->dedupe_hash, 
				 package->md5) != NULL) {
		return;
	}
#if EI2_DEBUG & 0x4
	trilobite_debug ("(adding %p %s %s to dedupe)", 
			 package, package->name, package->md5);
#endif
	gtk_object_ref (GTK_OBJECT (package));
	/* FIXME: bugzilla.eazel.com 7654 */
	g_hash_table_insert (service->private->dedupe_hash, 
			     g_strdup (package->md5), 
			     package);
}

static void
fail_modified_packages (TrilobiteInstall *service, 
			PackageData *package)
{
	GList *iterator;

	for (iterator = package->modifies; iterator; iterator = g_list_next (iterator)) {
		PackageData *pack = PACKAGEDATA (iterator->data);
		if (pack->status == PACKAGE_RESOLVED) {
			pack->status = PACKAGE_CANCELLED;
		}
	}
}

#if 0
static GetSoftCatResult
post_get_softcat_info (TrilobiteInstall *service, 
		       PackageData **package,
		       GetSoftCatResult result)
{
	PackageData *p1 = NULL;
	
#if EI2_DEBUG & 0x4
	trilobite_debug ("-> post_get_softcat_info %p %s", 
			 (*package), (*package)->name);
#endif

	g_assert ((*package)->md5);
	p1 = g_hash_table_lookup (service->private->dedupe_hash, (*package)->md5);
	
	if (p1 != NULL) {
		/* Don't dedupe yourself */
		if (p1 != (*package)) {
#if EI2_DEBUG & 0x4
			trilobite_debug ("\tdeduping(a) %p %s is already at %p %s", *package, (*package)->name, p1, p1->name);
#endif		
			
			gtk_object_ref (GTK_OBJECT (p1));
			gtk_object_unref (GTK_OBJECT (*package)); 
			(*package) = p1;
		} else {
#if EI2_DEBUG & 0x4
			trilobite_debug ("\tnot deduping(a) myself %p %s", *package, (*package)->name, p1);
#endif		
		}
	} 

	if (GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT ((*package)), "have-checked-existing")) == 0) {
		TrilobiteInstallStatus status;
		
		status = trilobite_install_check_existing_packages (service, *package);
		gtk_object_set_data (GTK_OBJECT ((*package)), "have-checked-existing", GINT_TO_POINTER (1));
		
#if 0
		/* HACK to always fail gnome-libs for fun... */
		if (strcmp ((*package)->name, "gnome-libs")==0) {
			trilobite_debug ("\t%s:%d MATCHED gnome-libs", __FILE__, __LINE__);
			status = TRILOBITE_INSTALL_STATUS_DOWNGRADES;
		}
#endif
		
		switch (status) {
		case TRILOBITE_INSTALL_STATUS_NEW_PACKAGE:
			add_to_dedupe_hash (service, *package);
			break;
		case TRILOBITE_INSTALL_STATUS_UPGRADES:
			if (trilobite_install_get_upgrade (service)) {
				add_to_dedupe_hash (service, *package);
			} else {
				fail_modified_packages (service, *package);
				gtk_object_set_data (GTK_OBJECT (*package), 
						     "cancelled", 
						     GINT_TO_POINTER (1));
			}
			break;
		case TRILOBITE_INSTALL_STATUS_DOWNGRADES:
			if (trilobite_install_get_downgrade (service)) {
				add_to_dedupe_hash (service, *package);
			} else {
				fail_modified_packages (service, *package);
				gtk_object_set_data (GTK_OBJECT (*package), 
						     "cancelled", 
						     GINT_TO_POINTER (1));
			}
			break;
		case TRILOBITE_INSTALL_STATUS_QUO:
			fail_modified_packages (service, *package);
			gtk_object_set_data (GTK_OBJECT (*package), 
					     "cancelled", 
					     GINT_TO_POINTER (1));
			result = PACKAGE_SKIPPED;
			break;
		}
	}

#if EI2_DEBUG & 0x4
	trilobite_debug ("<- post_get_softcat_info %p %s", 
			 (*package), (*package)->name);
#endif

	return result;
}

GetSoftCatResult
get_softcat_info (TrilobiteInstall *service, 
		  gpointer *ptr)
{
	GetSoftCatResult result = NO_SOFTCAT_HIT;
	PackageDependency *dep = NULL;
	PackageData **package = NULL;

	g_assert (ptr);
	g_assert (*ptr);
	g_assert (service);
	g_assert (TRILOBITE_IS_INSTALL (service));

	if (IS_PACKAGEDATA (*ptr)) {
		package = (PackageData**) (ptr);
	} else if (IS_PACKAGEDEPENDENCY (*ptr)) {
		dep = PACKAGEDEPENDENCY (*ptr);
		package = &(dep->package);
	} else {
		g_assert_not_reached ();
	}

	if ((*package)->fillflag == MUST_HAVE) {
		/* Package is already filled, set result to OK and toggle packages status */
		result = SOFTCAT_HIT_OK;
		(*package)->status = PACKAGE_PARTLY_RESOLVED;
	} else {
		if ((*package)->filename) {
			/* Package already has a filename, load info from disk */
			if (access ((*package)->filename, F_OK) == 0&&
			    access ((*package)->filename, R_OK) == 0) {
				PackageData *loaded_package;
#if EI2_DEBUG & 0x4
				trilobite_debug ("%p %s load from disk", (*package), (*package)->name);
#else
				g_message (_("Loading package info from file %s"), (*package)->filename);
#endif		
				loaded_package = trilobite_package_system_load_package (service->private->package_system,
											(*package),
											(*package)->filename,
											MUST_HAVE);
				if (loaded_package==NULL) {
					(*package)->status = PACKAGE_CANNOT_OPEN;
				} else {
					(*package)->status = PACKAGE_PARTLY_RESOLVED;
				}
				result = SOFTCAT_HIT_OK;
			}
		} else {
			/* Otherwise get info from SoftCat */
			TrilobiteSoftCatError err;
			
			/* if package->version is set, we want get_info to get the exact 
			   version */
			if (dep) {
				g_free ((*package)->version);
				(*package)->version = g_strdup (dep->version);
				err = trilobite_softcat_get_info (service->private->softcat,
								  (*package),
								  dep->sense,
								  MUST_HAVE);
			} else {
				err = trilobite_softcat_get_info (service->private->softcat,
								  (*package),
								  TRILOBITE_SOFTCAT_SENSE_EQ,
								  MUST_HAVE);
			}

			/* if ((err==TRILOBITE_SOFTCAT_SUCCESS) && ((*package)->fillflag & MUST_HAVE)) { */
			if (err==TRILOBITE_SOFTCAT_SUCCESS) {
				result = SOFTCAT_HIT_OK;
				(*package)->status = PACKAGE_PARTLY_RESOLVED;
			} else {
				result = NO_SOFTCAT_HIT;
				(*package)->status = PACKAGE_CANNOT_OPEN;
			}
		}
	}
	if ((result != NO_SOFTCAT_HIT) && !packagedata_is_suite ((*package))) {
		post_get_softcat_info (service, package, result);
	}
	return result;
}

static GetSoftCatResult
get_package_info_foreach (TrilobiteInstall *service,
			  gpointer *ptr)
{
	GetSoftCatResult result;

	g_assert (service);
	g_assert (TRILOBITE_IS_INSTALL (service));
	g_assert (ptr);

	result = get_softcat_info (service, ptr);

        switch (result) {
	case NO_SOFTCAT_HIT:
		g_message ("Could not get info from SoftwareCatalog ");
		break;
	case PACKAGE_SKIPPED:
#if EI2_DEBUG & 0x4
		trilobite_debug ("Package skipped");
#endif
		break;
	default:
#if EI2_DEBUG & 0x4
		trilobite_debug ("Softcat check ok");
#endif 		
		break;
	}
	return result;
}
#endif

static GetSoftCatResult
post_check (TrilobiteInstall *service, 
	    PackageData **package,
	    GetSoftCatResult result)
{
	PackageData *p1 = NULL;
	
	(*package)->status = PACKAGE_PARTLY_RESOLVED;
	
	if ((*package)->md5==NULL && packagedata_is_suite ((*package))) {
		return result;
	}

	p1 = g_hash_table_lookup (service->private->dedupe_hash, (*package)->md5);
	
	if (p1) {
#if EI2_DEBUG & 0x4
		trilobite_debug ("deduping %p %s to %p", (*package), (*package)->name, p1);
#endif		
		
		gtk_object_ref (GTK_OBJECT (p1));
		gtk_object_unref (GTK_OBJECT ((*package))); 
		(*package) = p1;
	} else {
		TrilobiteInstallStatus status;
		
		status = trilobite_install_check_existing_packages (service, (*package));
#if 0
		/* HACK to always fail gnome-libs for fun... */
		if (strcmp ((*package)->name, "gnome-libs")==0) {
			trilobite_debug ("%s:%d MATCHED gnome-libs", __FILE__, __LINE__);
			status = TRILOBITE_INSTALL_STATUS_DOWNGRADES;
		}
#endif
		
		switch (status) {
		case TRILOBITE_INSTALL_STATUS_NEW_PACKAGE:
			add_to_dedupe_hash (service, (*package));
			break;
		case TRILOBITE_INSTALL_STATUS_UPGRADES:
			if (trilobite_install_get_upgrade (service)) {
				add_to_dedupe_hash (service, (*package));
			} else {
				fail_modified_packages (service, (*package));
				gtk_object_set_data (GTK_OBJECT ((*package)), 
						     "cancelled", 
						     GINT_TO_POINTER (1));
			}
			break;
		case TRILOBITE_INSTALL_STATUS_DOWNGRADES:
			if (trilobite_install_get_downgrade (service)) {
				add_to_dedupe_hash (service, (*package));
			} else {
				fail_modified_packages (service, (*package));
				gtk_object_set_data (GTK_OBJECT ((*package)), 
							     "cancelled", 
						     GINT_TO_POINTER (1));
			}
			break;
		case TRILOBITE_INSTALL_STATUS_QUO:
			fail_modified_packages (service, (*package));
			gtk_object_set_data (GTK_OBJECT ((*package)), 
					     "cancelled", 
					     GINT_TO_POINTER (1));
			result = PACKAGE_SKIPPED;
			break;
		}
	}
	return result;
}

void
get_package_info (TrilobiteInstall *service, 
		  GList *packages)
{
	GList *iterator;
	GList *packages_by_softcat = NULL;
	GList *out = NULL, *error = NULL;

	g_assert (service);
	g_assert (TRILOBITE_IS_INSTALL (service));

	for (iterator = packages; iterator; iterator = g_list_next (iterator)) {
		PackageData **pack = NULL;
		gpointer ptr = iterator->data;

		if (IS_PACKAGEDATA (ptr)) {
			pack = (PackageData**)&ptr;
		} else if (IS_PACKAGEDEPENDENCY (ptr)) {
			pack = &(PACKAGEDEPENDENCY (ptr)->package);
		} else {
			g_assert_not_reached ();
		}		

		if ((*pack)->filename) {
			/* Package already has a filename, load info from disk */
			if (access ((*pack)->filename, F_OK)==0 &&
			    access ((*pack)->filename, R_OK)==0) {
				PackageData *loaded_package;
#if EI2_DEBUG & 0x4
				trilobite_debug ("%p %s load from disk", (*pack), (*pack)->name);
#else
				g_message (_("Loading package info from file %s"), (*pack)->filename);
#endif		
				loaded_package = trilobite_package_system_load_package (service->private->package_system,
											(*pack),
											(*pack)->filename,
											MUST_HAVE);
				if (loaded_package==NULL) {
					(*pack)->status = PACKAGE_CANNOT_OPEN;
				} else {
					(*pack)->status = PACKAGE_PARTLY_RESOLVED;
					post_check (service, pack, SOFTCAT_HIT_OK);
				}
			}
		} else {
			packages_by_softcat = g_list_prepend (packages_by_softcat, (*pack));
		}
	}

	if (packages_by_softcat) {
		trilobite_softcat_get_info_plural (service->private->softcat,
						   packages_by_softcat,
						   &out, &error,
						   TRILOBITE_SOFTCAT_SENSE_EQ,
						   MUST_HAVE);
		
		g_list_free (packages_by_softcat);
	}
		
	for (iterator = out; iterator; iterator = g_list_next (iterator)) {
		PackageData *p = PACKAGEDATA (iterator->data);
		post_check (service, &p, SOFTCAT_HIT_OK);
	}

	for (iterator = error; iterator; iterator = g_list_next (iterator)) {
		PackageData *p = PACKAGEDATA (iterator->data);
		p->status = PACKAGE_CANNOT_OPEN;
	}

	g_list_free (out);
	g_list_free (error);
}

/***********************************************************************************/

/* This is the dedupe magic, which given a tree, links dupes (same eazel-id) to
   point to the same PackageData* object and unrefs the dupes */

void dedupe_foreach (gpointer ptr, TrilobiteInstall *service);
void dedupe_foreach_depends (PackageDependency *d, TrilobiteInstall *service);
void dedupe (TrilobiteInstall *service, GList *packages);

void
dedupe_foreach_depends (PackageDependency *d,
			 TrilobiteInstall *service) 
{
	PackageData *p1;
	PackageData *p11;

	g_assert (d);
	g_assert (IS_PACKAGEDEPENDENCY (d));
	g_assert (service);
	g_assert (TRILOBITE_IS_INSTALL (service));
	
	p1 = d->package;

	if (p1->md5 == NULL) {
		/* Package info not received from SoftCat */
		return;
	}

	p11 = g_hash_table_lookup (service->private->dedupe_hash, p1->md5);
		
	if (p11) {
		if (p11 != p1) {
#if EI2_DEBUG & 0x4
			trilobite_debug ("\tdeduping(b) %p %s is already read at %p %s", p1, p1->name, p11, p11->name);
#endif         
			gtk_object_ref (GTK_OBJECT (p11));
			gtk_object_unref (GTK_OBJECT (p1)); 
			d->package = p11;
		} else {
#if EI2_DEBUG & 0x4
			trilobite_debug ("\tnot deduping(b) myself %p %s", p11, p11->name, p1);
#endif
		}
	} else {
		add_to_dedupe_hash (service, p1);
		dedupe_foreach ((gpointer)p1, service);
	}
}

void
dedupe_foreach (gpointer ptr,
		TrilobiteInstall *service)
{
	PackageData *package = NULL;

	g_assert (ptr);
	g_assert (service);
	g_assert (TRILOBITE_IS_INSTALL (service));

	if (IS_PACKAGEDATA (ptr)) {
		package = PACKAGEDATA (ptr);
	} else if (IS_PACKAGEDEPENDENCY (ptr)) {
		package = PACKAGEDEPENDENCY (ptr)->package;
	} else {
		g_assert_not_reached ();
	}

	g_list_foreach (package->depends, (GFunc)dedupe_foreach_depends, service);
} 

void
dedupe (TrilobiteInstall *service,
	GList *packages)
{
	g_assert (service);
	g_assert (TRILOBITE_IS_INSTALL (service));

	g_list_foreach (packages, (GFunc)dedupe_foreach, service);
#if EI2_DEBUG & 0x2
	dump_tree (packages);
#endif
}

/***********************************************************************************/

/* This is the code to remove already satisfied dependencies */

void check_dependencies (TrilobiteInstall *service, GList *packages, GList *mainlist);
void check_dependencies_foreach (TrilobiteInstall *service, GList *mainlist, PackageData *package);
gboolean is_satisfied (TrilobiteInstall *service, GList *mainlist, PackageDependency *dep);
gboolean is_satisfied_features (TrilobiteInstall *service, GList *mainlist, PackageData *package);
gboolean is_satisfied_from_main_package_list (TrilobiteInstall *service, GList *mainlist, PackageDependency *dep);

/*
  FIXME: bugzilla.eazel.com 6809
  is_satisfied_from_package_list is flawed since it 1) looks also at unresolved deps,
  should only look at partly_resolved 2) is only looks at the current workset, and not the complete
  (ie. everything at the current level, but now upwards */

gboolean
is_satisfied (TrilobiteInstall *service, 
	      GList *mainlist, 
	      PackageDependency *dep)
{
	char *key;
	int previous_check_state = 0;
	char *sense_str;
	gboolean result = FALSE;

	g_assert (dep);
	g_assert (IS_PACKAGEDEPENDENCY (dep));
	g_assert (service);
	g_assert (TRILOBITE_IS_INSTALL (service));

#if 0 
	/* FIXME:
	   this checks that the package isn't already filled, but what if
	   it is, but later fails, will we loose a dependency ? */
	/* First check if we've already downloaded the package */
	if (dep->package->fillflag == MUST_HAVE) {
#if EI2_DEBUG & 0x4
		trilobite_debug ("is_satisfied? %p %s", 
				 dep->package, dep->package->name);
		trilobite_debug ("\t -> already filled, must be ok");
#else
		g_message ("\tcached : %s", 
			   dep->package->name);
#endif
		
		return FALSE;
	}
#endif

	/* If the dependency has a version, but no sense, something is terribly
	   wrong with the xml */
#if EI2_DEBUG & 0x4
	if (dep->version && dep->sense == 0) {
		trilobite_debug ("I'm going to die now, because softcat is making no sense");
		trilobite_debug ("Or rather, the xml returned a invalid dependency sense");
	}
#endif
	if (dep->version) { g_assert (dep->sense!=0); }

	sense_str = trilobite_softcat_sense_flags_to_string (dep->sense);
#if EI2_DEBUG & 0x4
	trilobite_debug ("is_satisfied? %p %s %s %s", 
			 dep->package, dep->package->name, sense_str,
			 (dep->version != NULL ? dep->version : ""));
#endif
	key = g_strdup_printf ("%s/%s/%s", dep->package->name, sense_str,
			       (dep->version != NULL ? dep->version : ""));

	if (key != NULL && strcmp (key, "(null)//")!=0) {
		previous_check_state = GPOINTER_TO_INT (g_hash_table_lookup (service->private->dep_ok_hash, key));
	}
#if EI2_DEBUG & 0x4
	trilobite_debug ("\t--> key is %s", key);
#endif
	switch (previous_check_state) {
	case DEPENDENCY_OK: {
#if EI2_DEBUG & 0x4
		trilobite_debug ("\t--> dep hash ok");
#endif
		result = TRUE;
		break;
	}
	case DEPENDENCY_NOT_OK: {
#if EI2_DEBUG & 0x4
		trilobite_debug ("\t--> dep hash failed");
#endif
		result = FALSE;
		break;
	}
	default: {				
		/* If dependency has name and version, first check workset, then packages on system,
		   and use the dependency's version/sense */
		if (dep->package->name && dep->version) {
			if (is_satisfied_from_main_package_list (service, mainlist, dep)) {
#if EI2_DEBUG & 0x4
				trilobite_debug ("\t--> present with version in workset");
#endif
				result = TRUE;
			} else if (trilobite_package_system_is_installed (service->private->package_system,
									  service->private->cur_root,
									  dep->package->name,
									  dep->version,
									  NULL,
									  dep->sense)) {
#if EI2_DEBUG & 0x4
				trilobite_debug ("\t--> installed with version");
#endif
				result = TRUE;
			}
		} else if (dep->package->features) {
                        if (is_satisfied_features (service, mainlist, dep->package)) {
                                /* If package dependency didn't have a name/sense/version, but 
                                   has features, check if features are installed */
#if EI2_DEBUG & 0x4
                                trilobite_debug ("\t--> features of package are satisfied");
#endif
                                result = TRUE;
                        } else {
                                result = FALSE;
                        }
		} else if (dep->package->name) {
			/* Else check for name in workset and then amongst installed packages,
			   using sense ANY */
			if (is_satisfied_from_main_package_list (service, mainlist, dep)) {
#if EI2_DEBUG & 0x4
				trilobite_debug ("\t--> present in workset");
#endif
				result = TRUE;
			} else if (trilobite_package_system_is_installed (service->private->package_system,
									  service->private->cur_root,
									  dep->package->name,
									  NULL, 
									  NULL,
									  TRILOBITE_SOFTCAT_SENSE_ANY)) {
#if EI2_DEBUG & 0x4
				trilobite_debug ("\t--> installed");
#endif
				result = TRUE;
			}
		}

		if (result) {
#if EI2_DEBUG & 0x4
			trilobite_debug ("\t--> feature is satisfied");
#endif
			g_hash_table_insert (service->private->dep_ok_hash, 
					     key,
					     GINT_TO_POINTER (DEPENDENCY_OK));
		} else {
#if EI2_DEBUG & 0x4
			trilobite_debug ("\t--> feature not satisfied");
#endif
			g_hash_table_insert (service->private->dep_ok_hash, 
					     key,
					     GINT_TO_POINTER (DEPENDENCY_NOT_OK));
		}
	}
	}

#if ~EI2_DEBUG & 0x4	
	g_message ("\t%8.8s : %s %s %s", 
		   /* I18N note: ok is for "package dependencies are ok", "not ok".. well guess... */
		   result ? _("ok") : _("not ok"),
		   dep->package->name, sense_str,
		   (dep->version != NULL ? dep->version : ""));
#endif
	g_free (sense_str);
	return result;
}

gboolean 
is_satisfied_from_main_package_list (TrilobiteInstall *service, 
				     GList *mainlist, 
				     PackageDependency *dep)
{
	GList *iterator;
	GList *flat_packages;
	gboolean result = FALSE;

#if EI2_DEBUG & 0x4
	trilobite_debug ("\t--> is_satisfied_from_packages %s from %d packages",
			 dep->package->name, g_list_length (mainlist));
#endif
	flat_packages = flatten_packagedata_dependency_tree (mainlist);
	
	for (iterator = flat_packages; iterator; iterator = g_list_next (iterator)) {
		PackageData *p = PACKAGEDATA (iterator->data);

		trilobite_debug ("\t\t --> checking %s %s %s", 
				 p->name, 
				 packagedata_status_enum_to_str (p->status),
				 p->fillflag == MUST_HAVE ? "filled" : "not filled");

		if (p->status == PACKAGE_PARTLY_RESOLVED && 
		    (p->fillflag == MUST_HAVE) && 
		    p->name && 
		    (strcmp (p->name, dep->package->name)==0)) {
			trilobite_debug ("\t\t --> * lvl 1/3");
			if (dep->version) {
				int res;
				trilobite_debug ("\t\t --> * lvl 2/3");
				res = trilobite_package_system_compare_version (service->private->package_system,
										dep->version,p->version);
				if (res==0 && dep->sense & TRILOBITE_SOFTCAT_SENSE_EQ) {
					trilobite_debug ("\t\t --> * lvl 3.1/3");
					result = TRUE;
					break;
				} else if (res > 0 && dep->sense & TRILOBITE_SOFTCAT_SENSE_LT) {
					trilobite_debug ("\t\t --> * lvl 3.2/3");
					result = TRUE;
					break;
				} else if (res < 0 && dep->sense & TRILOBITE_SOFTCAT_SENSE_GT) {
					trilobite_debug ("\t\t --> * lvl 3.3/3");
					result = TRUE;
					break;
				} else {
					char *sense_str;

					sense_str = trilobite_softcat_sense_flags_to_string (dep->sense);
					trilobite_debug ("\t\t --> * bad karma %s %s %s %d", 
							 dep->version,
							 sense_str,
							 p->version,
							 res);
					g_free (sense_str);
				}
			} else {
				trilobite_debug ("\t\t --> * lvl 3.0/3");
				result = TRUE;
				break;
			}
		}
	}
	
	g_list_free (flat_packages);
	return result;
}

gboolean
is_satisfied_features (TrilobiteInstall *service, 
		       GList *mainlist, 
		       PackageData *package)
{
	gboolean result = TRUE;
	GList *iterator;
	GList *features;

	g_assert (service);
	g_assert (TRILOBITE_IS_INSTALL (service));
	g_assert (package);
	g_assert (IS_PACKAGEDATA (package));

	features = package->features;

#if EI2_DEBUG & 0x4
	trilobite_debug ("\t -> is_satisfied_features %d features", g_list_length (features));
#endif

	for (iterator = features; iterator && result; iterator = g_list_next (iterator)) {
		GList *query_result;
		char *f = (char*)iterator->data;

#if EI2_DEBUG & 0x4
		trilobite_debug ("\t -> %s", f);
#endif
		query_result = trilobite_package_system_query (service->private->package_system,
							       service->private->cur_root,
							       f,
							       TRILOBITE_PACKAGE_SYSTEM_QUERY_PROVIDES,
							       PACKAGE_FILL_MINIMAL);
		if (g_list_length (query_result) == 0) {
			query_result = trilobite_package_system_query (service->private->package_system,
								       service->private->cur_root,
								       f,
								       TRILOBITE_PACKAGE_SYSTEM_QUERY_OWNS,
								       PACKAGE_FILL_MINIMAL);
			if (g_list_length (query_result) == 0) {
#if EI2_DEBUG & 0x4
				trilobite_debug ("\t -> noone owns or provides %s", f);
#endif
				result = FALSE;

/*    This is a workaround for a softcat bug "speciality, where packagedependencies
      with no sense are reported as a <FEATURE>foo</FEATURE>. When bug 1129
      is closed, remove this evilness */
#if PATCH_FOR_SOFTCAT_BUG
				if (trilobite_package_system_is_installed (service->private->package_system,
									   service->private->cur_root,
									   f, NULL, NULL,
									   TRILOBITE_SOFTCAT_SENSE_ANY)) {
#if EI2_DEBUG & 0x4
					trilobite_debug ("\t -> but a package called %s exists", f);
#endif
					result = TRUE;
				}
#endif
			}
		}
		g_list_foreach (query_result, (GFunc)gtk_object_unref, NULL); 
		g_list_free (query_result);
	}

	return result;
}

void 
check_dependencies_foreach (TrilobiteInstall *service, 
			    GList *mainlist,
			    PackageData *package)
{
	GList *remove = NULL;
	GList *iterator;
	
	g_assert (service);
	g_assert (TRILOBITE_IS_INSTALL (service));
	g_assert (package);
	g_assert (IS_PACKAGEDATA (package));

	/* If the package was cancelled, don't process it's dependencies */
	if (package->status == PACKAGE_CANCELLED || 
	    (package->status == PACKAGE_ALREADY_INSTALLED && package->modifies==NULL)) {
		g_list_foreach (package->depends, (GFunc)gtk_object_unref, NULL);
		g_list_free (package->depends);
		package->depends = NULL;
		return;
	}

	if (packagedata_is_suite (package)) {
		for (iterator = package->depends; iterator; iterator = g_list_next (iterator)) {
			PackageDependency *dep = PACKAGEDEPENDENCY (iterator->data);
			dep->package->fillflag = PACKAGE_FILL_INVALID;
			g_list_foreach (dep->package->depends, (GFunc)gtk_object_unref, NULL);
			dep->package->depends = NULL;
		}
#if EI2_DEBUG & 0x4
		trilobite_debug ("skipping suite %p %s", package, package->suite_id);
#endif
	}


#if EI2_DEBUG & 0x4
	trilobite_debug ("check deps for %p %s", package, package->name);
#else
	g_message (_("Checking dependencies for %s"), package->name);
#endif

	for (iterator = package->depends; iterator; iterator = g_list_next (iterator)) {
		PackageDependency *dep = PACKAGEDEPENDENCY (iterator->data);		

		if (dep->package->name && package->name && strcmp (dep->package->name, package->name)==0) {
			char *name_a, *name_b;

			name_a = packagedata_get_readable_name (package);
			name_b = packagedata_get_readable_name (dep->package);

			g_warning ("Inconsistency!");
			g_warning ("%s depends on %s", name_a, name_b);

			g_free (name_a);
			g_free (name_b);

			package->status = PACKAGE_CIRCULAR_DEPENDENCY;
		} else {
			if (is_satisfied (service, mainlist, dep)) {
				remove = g_list_prepend (remove, dep);
			} else {
				trilobite_install_emit_dependency_check (service, package, dep);
			}
		}
	}

	for (iterator = remove; iterator; iterator = g_list_next (iterator)) {
		PackageDependency *dep = PACKAGEDEPENDENCY (iterator->data);
		package->depends = g_list_remove (package->depends, dep);
#if EI2_DEBUG & 0x4
		trilobite_debug ("removing %p %s from %p %s", 
				 dep->package, dep->package->name, 
				 package, package->name);
#endif
		gtk_object_unref (GTK_OBJECT (dep));
	}
	g_list_free (remove);
}

void
check_dependencies (TrilobiteInstall *service,
		    GList *packages,
		    GList *mainlist)
{
	GList *iterator;

	g_assert (service);
	g_assert (TRILOBITE_INSTALL (service));
	
	for (iterator = packages; iterator; iterator = g_list_next (iterator)) {
		PackageData *package = NULL;
		gpointer ptr = iterator->data;

		if (IS_PACKAGEDATA (ptr)) {
			package = PACKAGEDATA (ptr);
		} else if (IS_PACKAGEDEPENDENCY (ptr)) {
			package = PACKAGEDEPENDENCY (ptr)->package;
		} else {
			g_assert_not_reached ();
		}

		check_dependencies_foreach (service, mainlist, package);
	}

#if EI2_DEBUG & 0x1
	trilobite_debug ("post dep_check tree");
	dump_tree (packages);
#endif
}

/***********************************************************************************/

/* This is the main dependency check function */

void do_dep_check_internal (TrilobiteInstall *service, GList *packages, GList *mainlist);
void do_dep_check (TrilobiteInstall *service, GList **packages, GList *mainlist);
void do_requirement_consistency_check (TrilobiteInstall *service, GList *packages);
void do_requirement_consistency_check_internal (TrilobiteInstall *service, GList *packages);

void
do_dep_check_internal (TrilobiteInstall *service,
		       GList *packages,
		       GList *mainlist)
{
	GList *iterator;
	GList *K = NULL;

	g_assert (service);
	g_assert (TRILOBITE_IS_INSTALL (service));

	if (packages==NULL) {
		return;
	}

#if EI2_DEBUG & 0x1
	trilobite_debug ("do_dep_check tree");
	dump_tree (packages);
#endif

	get_package_info (service, packages);
	dedupe (service, packages);
	check_dependencies (service, packages, mainlist);

	/* we might have received a "stop" callback recently */
	if (service->private->cancel_download) {
		return;
	}

	/* Build the K list, consisting of packages without must_have set... */
	for (iterator = packages; iterator; iterator = g_list_next (iterator)) {
	        GList *diterator;
		PackageData *pack = NULL;

		if (IS_PACKAGEDATA (iterator->data)) {
			pack = PACKAGEDATA (iterator->data);
		} else if (IS_PACKAGEDEPENDENCY (iterator->data)) {
			pack = PACKAGEDEPENDENCY (iterator->data)->package;
		}

		for (diterator = pack->depends; diterator; diterator = g_list_next (diterator)) {
			PackageDependency *dep = PACKAGEDEPENDENCY (diterator->data);
			if (((~dep->package->fillflag & MUST_HAVE) ||
			    packagedata_is_suite (pack)) && 
			    (g_list_find (K, dep->package)==NULL)) {
				K = g_list_prepend (K, dep);
			}
		}
	}

	/* ... and process them */
	if (K) {
		GList *foo;
		foo = g_list_copy (packages);
		foo = g_list_concat (foo, mainlist);

		do_dep_check_internal (service, K, foo);

		g_list_free (foo);
	}
}

void
do_dep_check (TrilobiteInstall *service,
	      GList **packages,
	      GList *mainlist)
{
	/* Shift to the internal. We want to do a prune_failed_packages
	   after completion, but since the do_dep_check algorithm is 
	   recursive, we call an internal function here. */
	do_dep_check_internal (service, *packages, mainlist);
	prune_failed_packages (service, packages);
}

/* This function is used by do_requirement_consistency_check_package.
   "pack" is a package which modified "updated", and "dep" is a 
   dependency that someone else has on updated.

   To accept :
   calculate version compare value x for cmp (dep->version, pack->version)
   if upgrading the package and sense is any, accept
   if upgrading the package and sense has >, accept
   if upgrading the package and sense has <, accept if x > 0
   if upgrading the package and sense has =, accept if x = 0
   vice versa for downgrade (flipping the ><'s)
   
   This will not be called with dep->sense = ANY, since these are always ok.
*/
static gboolean
check_if_modification_is_ok (TrilobiteInstall *service, 
			     PackageData *pack, 
			     PackageData *updated,
			     PackageDependency *dep)
{
	gboolean accept = FALSE;
	int x;

	/* But just in case.... */
	if (dep->sense == TRILOBITE_SOFTCAT_SENSE_ANY) {
		return TRUE;
	}

	x = trilobite_package_system_compare_version (service->private->package_system, 
						      dep->version,
						      pack->version);

	switch (updated->modify_status) {
	case PACKAGE_MOD_UPGRADED:
#if EI2_DEBUG & 0x4
		trilobite_debug ("\t\t\t\t -> checking if upgrade is ok");
#endif
		if (dep->sense & TRILOBITE_SOFTCAT_SENSE_GT) {
			/* is sense it has > in it, upgrading is safe */
			accept = TRUE;
		} else if (dep->sense & TRILOBITE_SOFTCAT_SENSE_LT) {
			/* if sense it has < in it, upgrade is
			   safe if dep->version is > pack->version*/
			if (x > 0) {
				accept = TRUE;
			}
		} else if (dep->sense & TRILOBITE_SOFTCAT_SENSE_EQ) {
			/* if sense is =, upgrade is safe if
			   x == 0 */
			if (x == 0) {
				accept = TRUE;
			}
		} 
		break;
	case PACKAGE_MOD_DOWNGRADED:
#if EI2_DEBUG & 0x4
		trilobite_debug ("\t\t\t\t -> checking if downgrade is ok");
#endif
		if (dep->sense & TRILOBITE_SOFTCAT_SENSE_LT) {
			/* is sense it has < in it, 
			   downgrading is safe */
			accept = TRUE;
		} else if (dep->sense & TRILOBITE_SOFTCAT_SENSE_GT) {
			/* if sense it has > in it, upgrade is
			   safe if pack->version is > dep->version */
			if (x < 0) {
				accept = TRUE;
			}
		} else if (dep->sense & TRILOBITE_SOFTCAT_SENSE_EQ) {
			/* if sense is =, upgrade is safe if
			   x == 0 */
			if (x == 0) {
				accept = TRUE;
			}
		}
		break;
	default:
		break;
	}

#if EI2_DEBUG & 0x4
	trilobite_debug ("\t\t\t\t -> %s", accept ? "yarh" : "argh");
#endif

	return accept;
}

static int
find_break_by_package_name (PackageBreaks *breaks, PackageData *pack)
{
	return trilobite_install_package_compare (packagebreaks_get_package (breaks), 
						  pack);
}

/*
  Traverse the packages modifies and query for packages R that require them
  foreach r in R 
      foreach d in R->depends
          if d->version && d->package is set (and sense isn'any) 
             if modification not ok, add breaks structure
 */
static void 
do_requirement_consistency_check_package (TrilobiteInstall *service, 
					  GList *packages,
					  PackageData *pack)
{
	GList *iterator;

#if EI2_DEBUG & 0x4
	trilobite_debug ("\t -> do_requirement_consistency_check_package (%p %s)", pack, pack->name);
#endif

	if (GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (pack), "do_requirement_consistency_check"))!=0) {
		return;
	}
	gtk_object_set_data (GTK_OBJECT (pack), "do_requirement_consistency_check", GINT_TO_POINTER (1));
	
	/* Check all the modifies of the package */
	for (iterator = pack->modifies; iterator; iterator = g_list_next (iterator)) {
		PackageData *updated = PACKAGEDATA (iterator->data);
		GList *requiredby_list = NULL;
		GList *requiredby_it;

		GList *feature_cache;
		GList *provide_cache;

#if EI2_DEBUG & 0x4
		trilobite_debug ("\t -> querying for who requires %s", updated->name);
#endif
		/* I temporarily omit the provides, since we later do a provide consistency check */
		feature_cache = updated->features;
		updated->features = NULL;
		provide_cache = updated->provides;
		updated->provides = NULL;

		/* Get the packages that require the modified package */
		requiredby_list = trilobite_package_system_query (service->private->package_system,
								  service->private->cur_root,
								  updated,
								  TRILOBITE_PACKAGE_SYSTEM_QUERY_REQUIRES,
								  PACKAGE_FILL_NO_PROVIDES);
		updated->features = feature_cache;
		updated->provides = provide_cache;

		/* Now check all the dependencies of the packages that requires the modified package */
		for (requiredby_it = requiredby_list; requiredby_it; requiredby_it = g_list_next (requiredby_it)) {
			PackageData *requiredby = PACKAGEDATA (requiredby_it->data);
			GList *dep_it;

#if EI2_DEBUG & 0x4
			trilobite_debug ("\t\t -> scanning deps for %s", requiredby->name);
#endif
			/* If the package that requries something from the modified package is
			   in the current workset, assume the other parts of the dependency voodoo
			   works and we can there just continue... */

			if (g_list_find_custom (packages, 
						requiredby->name, 
						(GCompareFunc)trilobite_install_package_name_compare)) {
#if EI2_DEBUG & 0x4
				trilobite_debug ("\t\t -> since it's in packages, it's fine...");
#endif
			} else for (dep_it = requiredby->depends; dep_it; dep_it = g_list_next (dep_it)) {
				PackageDependency *dep = PACKAGEDEPENDENCY (dep_it->data);
				if (dep->package && dep->version) {
					/* if this is a dep for the updated package, and sense
					   isn't ANY, check if modification is safe.
					   Note, sense will never be ANY (since you can't have ANY sense
					   and a version, but just for purity, I check for it */
					if (dep->package->name &&
					    strcmp (dep->package->name, updated->name)==0 &&
					    dep->sense != TRILOBITE_SOFTCAT_SENSE_ANY) {
#if EI2_DEBUG & 0x4
						trilobite_debug ("\t\t\t -> %p %s requires %s %s %s",
								 requiredby, requiredby->name,
								 dep->package->name,
								 trilobite_softcat_sense_flags_to_string (dep->sense),
								 dep->version);
#endif
						if (check_if_modification_is_ok (service, 
										 pack, 
										 updated, 
										 dep)
						    == FALSE) {
							/* If we fail, mark pack as breaking requiredby */
							if (g_list_find_custom (pack->breaks, 
										updated,
										(GCompareFunc)find_break_by_package_name)==NULL) {
								PackageFeatureMissing *breakage = 
									packagefeaturemissing_new ();
								PackageDependency *req_dep = packagedependency_new();

								/* Ref mojo */
								gtk_object_ref (GTK_OBJECT (updated));
								gtk_object_ref (GTK_OBJECT (requiredby));
								req_dep->package = updated;
								req_dep->sense = dep->sense;
								req_dep->version = g_strdup (dep->version);
								packagedata_add_pack_to_depends (requiredby, req_dep);
								pack->status = PACKAGE_BREAKS_DEPENDENCY;
								packagebreaks_set_package (PACKAGEBREAKS (breakage), 
											   requiredby);
								packagedata_add_to_breaks (pack, PACKAGEBREAKS (breakage));
							}
						}
					}
				}
			}
		}
		g_list_foreach (requiredby_list, (GFunc)gtk_object_unref, NULL);
		g_list_free (requiredby_list);
	}
}

void 
do_requirement_consistency_check_internal (TrilobiteInstall *service, 
					   GList *packages)
{
	GList *iterator;
	
#if EI2_DEBUG & 0x4
	trilobite_debug ("--> do_requirement_consistency_check_internal");
#endif

	for (iterator = packages; iterator; iterator = g_list_next (iterator)) {
		PackageData *pack = PACKAGEDATA (iterator->data);
		if (pack->modifies) {		      
#if EI2_DEBUG & 0x4
			trilobite_debug ("\t -> checking %p %s", pack, pack->name);
#endif
			do_requirement_consistency_check_package (service, packages, pack);
			
		}
	}

#if EI2_DEBUG & 0x4
	trilobite_debug ("<-- do_requirement_consistency_check_internal");
#endif
}

void 
do_requirement_consistency_check (TrilobiteInstall *service, 
				  GList *packages)
{
	GList *flat_packages;

	flat_packages = flatten_packagedata_dependency_tree (packages);
	do_requirement_consistency_check_internal (service, flat_packages);
	g_list_free (flat_packages);
}

/***********************************************************************************/

/* This is the main file conflicts check hell */

void do_file_conflict_check (TrilobiteInstall *service, GList **packages, GList **extra_packages);
void check_no_two_packages_has_same_file (TrilobiteInstall *service, GList *packages);
void check_feature_consistency (TrilobiteInstall *service, GList *packages);
void check_conflicts_against_already_installed_packages (TrilobiteInstall *service, GList *packages);
void check_tree_for_conflicts (TrilobiteInstall *service, GList **packages, GList **extra_packages);
gboolean check_if_modified_related_package (TrilobiteInstall *service, PackageData *package, PackageData *dep);
gboolean check_if_related_package (TrilobiteInstall *service, PackageData *package, PackageData *dep);

/*
  This function tests whether "package" and "dep"
  seems to be related in some way.
  This is done by checking the package->modifies list for
  elements that have same version as dep->version.
  I then compare these elements against dep->name,
  and if one matches the x-y-z vs dep->name=x-y scheme,
  I declare that "package" and "dep" are related
*/
gboolean
check_if_related_package (TrilobiteInstall *service,
			  PackageData *package,
			  PackageData *dep)
{
	/* Pessimisn mode = HIGH */
	gboolean result = FALSE;
	char **dep_name_elements;
	char **mod_name_elements;
	char *dep_name_iterator;
	char *mod_name_iterator;
	int cnt = 0;

#if EI2_DEBUG & 0x4
	trilobite_debug ("check_if_related_package (%p %s-%s, %p %s-%s)",
			 package, package->name, package->version,
			 dep, dep->name, dep->version);
#endif 	

	/* If the two packages don't have the same version, they're not related.
	   While this may seem to be too restrictive, it's how it has to
	   be done in the context where this call is used */
	if (trilobite_package_system_compare_version (service->private->package_system,
						      package->version,
						      dep->version) != 0) {
#if EI2_DEBUG & 0x4
		trilobite_debug ("versions do not match, not related");		
#endif		
		return FALSE;
	}

	dep_name_elements = g_strsplit (dep->name, "-", 80);		
	mod_name_elements = g_strsplit (package->name, "-", 80);
		
	for (cnt=0; TRUE;cnt++) {
		dep_name_iterator = dep_name_elements[cnt];
		mod_name_iterator = mod_name_elements[cnt];
#if 0
		trilobite_debug ("dep name iterator = \"%s\"", dep_name_iterator);
		trilobite_debug ("mod name iterator = \"%s\"", mod_name_iterator);
#endif
		if ((dep_name_iterator==NULL) ||
		    (mod_name_iterator==NULL)) {
			break;
		}
		if ((strlen (dep_name_iterator) == strlen (mod_name_iterator)) &&
		    (strcmp (dep_name_iterator, mod_name_iterator)==0)) {
			continue;
		}
		break;
	}
	if (cnt >= 1) {
		if (!result) {
			result = TRUE;
		} else {
			trilobite_debug ("but what blows is, the previous also did!!");
			package->status = PACKAGE_CANCELLED;
			result = FALSE;
		}				
	} else {
#if EI2_DEBUG & 0x4
		trilobite_debug ("%s-%s is not related to %s-%s", 
				 dep->name, dep->version, 
				 package->name, package->version);
#endif
	}

	g_strfreev (mod_name_elements);			
	g_strfreev (dep_name_elements);
	return result;
}

/*
  This function tests whether "package" has modifieds that are related to "dep" in any way.
*/
gboolean
check_if_modified_related_package (TrilobiteInstall *service,
				   PackageData *package,
				   PackageData *dep)
{
	/* Pessimisn mode = HIGH */
	gboolean result = FALSE;
	GList *potiental_mates;

	potiental_mates = package->modifies;

	while ((result == FALSE) && 
	       (potiental_mates = 
		g_list_find_custom (potiental_mates, 
				    dep->version, 
				    (GCompareFunc)trilobite_install_package_version_compare))!=NULL) {
		PackageData *modpack = PACKAGEDATA (potiental_mates->data);
#if EI2_DEBUG & 0x4
		trilobite_debug ("checking against %p %s-%s %p %s-%s %p %s-%s", 
				 package, 
				 package->name, package->version,
				 modpack,
				 modpack->name,
				 modpack->version,
				 dep, dep->name, dep->version);
#endif
		if (check_if_related_package (service, 
					      modpack, 
					      dep) == TRUE) {
#if EI2_DEBUG & 0x4
			trilobite_debug ("%s-%s seems to be a related package of %s-%s, which %s-%s modifies",
					 dep->name, dep->version, 
					 modpack->name, modpack->version,
					 package->name, package->version);
#endif
			result = TRUE;			
		}
		/* Skip one ahead */
		potiental_mates = g_list_next (potiental_mates);
	}

	return result;
}

static gboolean
check_update_for_no_more_file_conflicts (PackageFileConflict *conflict, 
					 PackageData *pack_update)
{
	GList *iterator;

	g_assert (IS_PACKAGEFILECONFLICT (conflict));
	g_assert (conflict->files);
	g_assert (g_list_length (conflict->files));

	trilobite_debug ("-> check_update_for_no_more_file_conflicts");

	for (iterator = conflict->files; iterator; iterator = g_list_next (iterator)) {
		char *filename = (char*)iterator->data;
		trilobite_debug ("\t checking %s", filename);
		if (g_list_find_custom (pack_update->provides, 
					filename,
					(GCompareFunc)strcmp)) {

			trilobite_debug ("<- check_update_for_no_more_file_conflicts FALSE");
			return FALSE;
		}
	}
	trilobite_debug ("<- check_update_for_no_more_file_conflicts TRUE");
	return TRUE;
}

static gboolean
check_for_no_more_missing_features (PackageFeatureMissing *missing, 
				    PackageData *pack_update)
{
					/* FIXME: bugzilla.eazel.com 6811 */
					/* 
					   tjek that the update no longer requires 
					   missing->features;
					   for each F in missing->features {
					       for each D in pack_update->depends {
					           if F occurs in D->features pack_update still needs feature F
					       }
					   }
					   
					 */
	GList *iterator;

	for (iterator = missing->features; iterator; iterator = g_list_next (iterator)) {
		char *feature = (char*)iterator->data;
		GList *dep_iterator;

		for (dep_iterator = pack_update->depends; dep_iterator; dep_iterator = g_list_next (dep_iterator)) {
			PackageDependency *dep = PACKAGEDEPENDENCY (dep_iterator->data);
			PackageData *pack = dep->package;

			if (pack==NULL) { continue; }
			if (g_list_find (pack->features, feature)) {
				return FALSE;
			}
		}		
	}

	return TRUE;
}


static void
check_tree_helper (TrilobiteInstall *service, 
		   PackageData *pack,
		   GList **extra_packages,
		   GList **path)
{
	GList *iterator;
	GList *remove = NULL;

	if (g_list_find (*path, pack)) {
#if EI2_DEBUG & 0x4
		trilobite_debug ("... %p %s recurses ..", pack, pack->name);
#endif
		return;
	}

	if (pack->status == PACKAGE_FILE_CONFLICT ||
	    pack->status == PACKAGE_BREAKS_DEPENDENCY) {
		if (trilobite_install_get_upgrade (service)==FALSE) {
#if EI2_DEBUG & 0x4
			trilobite_debug ("cannot revive %s, update not allowed", pack->name);
#endif			
		}
#if EI2_DEBUG & 0x4
		trilobite_debug ("trying to revive %p %s", pack, pack->name);
#else
		g_message ("trying to revive %s", pack->name);
#endif
		/* Iterate over all the breakages registered so far */
		for (iterator = pack->breaks; iterator; iterator = g_list_next (iterator)) {
			PackageBreaks *breakage = PACKAGEBREAKS (iterator->data);

			PackageData *pack_broken = packagebreaks_get_package (breakage);
			PackageData *pack_update = NULL;
			gboolean update_available = FALSE;
			gboolean related = FALSE;
			
			related = check_if_modified_related_package (service,
								     pack,
								     pack_broken);
			/* If the package we're operating on modifies somehthing that is related,
			   get an exact (matching) version instead of just checking for an 
			   update. This is typicall for recovering from breaking a -devel package */
			
			if (related) {
				char *a, *b;
				a = packagedata_get_readable_name (pack);
				b = packagedata_get_readable_name (pack_broken);
				/* I18N note: both %s' are package names.
				   related is in the sense that the two packages apparently
				   are connected somehow, eg. foo and foo-devel */
				g_message (_("%s is related to %s"),
					   a, b);
				
				/* Create the pack_update */
				pack_update = packagedata_new ();
				pack_update->name = g_strdup (pack_broken->name);
				pack_update->version = g_strdup (pack->version);
				pack_update->distribution = pack_broken->distribution;
				pack_update->archtype = g_strdup (pack_broken->archtype);
				
				/* Try and get the info */
				if (trilobite_softcat_get_info (service->private->softcat,
								pack_update,
								TRILOBITE_SOFTCAT_SENSE_EQ,
								MUST_HAVE) 
				    != TRILOBITE_SOFTCAT_SUCCESS) {
					update_available = FALSE;
					/* if we failed, delete the package object */
					gtk_object_unref (GTK_OBJECT (pack_update));
				} else {
					update_available = TRUE;
				}
				
				g_free (a);
				g_free (b);
			} else {
				update_available = 
					trilobite_softcat_available_update (service->private->softcat,
									    pack_broken,
									    &pack_update,
									    MUST_HAVE);
			}
			
			/* If we got the update, do the post_check check,
			   which sets the modifies list and checks for dupes in the
			   dedupe hash, also checks to see if a possible upgrade/downgrade
			   is allowed */
			if (update_available) {
				if (post_check (service, &pack_update, SOFTCAT_HIT_OK) !=
				    SOFTCAT_HIT_OK) {
					update_available = FALSE;
				}								
			} 

			/* So far so good... */
			if (update_available) {
				gboolean proceed = TRUE;

				/* Now we need to see if the proposed package 
				   resolves the current problem */
				if (IS_PACKAGEFILECONFLICT (breakage)) {
					PackageFileConflict *conflict = PACKAGEFILECONFLICT (breakage); 
					
					/* If they're not related, check that the proposed update no
					   longer has the offending file(s).
					   If they're related, it will typically be foo and foo-devel with
					   same version number, and the double ownership of the file
					   will be ok */
					if (related==FALSE && check_update_for_no_more_file_conflicts (conflict, pack_update)==FALSE) {
						proceed = FALSE;
					}
				} else if (IS_PACKAGEFEATUREMISSING (breakage)) {
					PackageFeatureMissing *missing = PACKAGEFEATUREMISSING (breakage);
					
					/* Check that the package we're proposing to update to does not
					   require the feature that would be lost */
					if (check_for_no_more_missing_features (missing, pack_update)==FALSE) {
						proceed = FALSE;
					}					
				} 
				if (proceed) {
					/* The proposed package did indeed resolve this problem... */
#if EI2_DEBUG & 0x4
					trilobite_debug ("adding %s to packages to be installed, conflicts resolved", 
							 pack_update->name);
#else
					g_message (_("updating %s to version %s-%s solves conflict"),
						pack_update->name, pack_update->version,
						pack_update->minor);
#endif
					/* If not already in extra_packages, add it.
					   Since post_softcat_get_info dedupes, even multiple 
					   resolves to the same package will at this point be
					   the same package */
					if (g_list_find ((*extra_packages), pack_update)==NULL) {
						/* ref the package and add it to the extra_packages list */
						gtk_object_ref (GTK_OBJECT (pack_update));
						(*extra_packages) = g_list_prepend (*extra_packages, 
										    pack_update);
						pack_update->status = PACKAGE_PARTLY_RESOLVED;
					}

					remove = g_list_prepend (remove, breakage);
					pack_update->toplevel = TRUE;
				} else {
					/* Nope, proposed package did not help... */
#if EI2_DEBUG & 0x4
					trilobite_debug ("%s still has conflict", pack_update->name);
#else
					g_message (_("available update to %s (%s-%s) does not solves conflict"),
						   pack_update->name,
						   pack_update->version, pack_update->minor);
#endif
					/* If a previous resolve added it to the list, remove it again */
					if (g_list_find ((*extra_packages), pack_update)!=NULL) {
						(*extra_packages) = g_list_remove ((*extra_packages), pack_update);
					}
					/* And remove it from the dedupe hash */
					g_hash_table_remove (service->private->dedupe_hash,
							     pack_update->md5);
					/* and blast it away */
					gtk_object_unref (GTK_OBJECT (pack_update));
				}
			} else {
				g_message (_("could not revive %s"), pack->name);
			} 
		}

		/* Now nuke the successfully revived PackageBreaks */
		for (iterator = remove; iterator; iterator = g_list_next (iterator)) {
			PackageBreaks *breakage = PACKAGEBREAKS (iterator->data);
			gtk_object_unref (GTK_OBJECT (breakage));
			pack->breaks = g_list_remove (pack->breaks, breakage);
		}
		g_list_free (remove);

		/* if no breaks were unrevived, null out the list */
		if (g_list_length (pack->breaks)==0) {
			/* reset the broken  to some sane values if it has no more issues */
			pack->status = PACKAGE_PARTLY_RESOLVED;
			g_list_free (pack->breaks);
			pack->breaks = NULL;
		}
	}
	
	for (iterator = pack->depends; iterator; iterator = g_list_next (iterator)) {
		PackageDependency *dep = PACKAGEDEPENDENCY (iterator->data);
		(*path) = g_list_prepend (*path, pack);
		check_tree_helper (service, dep->package, extra_packages, path);
		(*path) = g_list_remove (*path, pack);
	}
}

/* 
   Scan the tree for file conflicts. Add the broken packages to extra_packages
   and reset the status flag on the offending package. Since the prune_failed_packages
   is called at every step, we won't risk resetting a borked package to ok.
 */
void 
check_tree_for_conflicts (TrilobiteInstall *service, 
			  GList **packages, 
			  GList **extra_packages)
{
	GList *iterator;
#if EI2_DEBUG & 0x4
	trilobite_debug ("-> check_tree_for_conflicts");
#endif
	for (iterator = g_list_first (*packages); iterator != NULL; iterator = g_list_next (iterator)) {
		PackageData *pack = PACKAGEDATA (iterator->data);
		GList *path = NULL;
		check_tree_helper (service, pack, extra_packages, &path);
	}
#if EI2_DEBUG & 0x4
	trilobite_debug ("<- check_tree_for_conflicts");
#endif
}

static gboolean
add_file_conflict (TrilobiteInstall *service, 
		   PackageData *pack, 
		   PackageData *broken,
		   char *filename)
{
	GList *prev;

	if (check_if_related_package (service, pack, broken) == TRUE) {
#if EI2_DEBUG & 0x4				
		trilobite_debug ("\%p %s %p %s are related, ignoring conflict",
				 pack, pack->name,
				 broken, broken->name);
#endif	
		return FALSE;
	}

#if EI2_DEBUG & 0x4				
	trilobite_debug ("\tadding file conflict between %p %s %p %s",
			 pack, pack->name,
			 broken, broken->name);
#endif	

	prev = g_list_find_custom (pack->breaks, 
				   broken,
				   (GCompareFunc)find_break_by_package_name);
	pack->status = PACKAGE_FILE_CONFLICT;

	if (prev) {
		PackageFileConflict *conflict = PACKAGEFILECONFLICT (prev->data);
		conflict->files = g_list_prepend (conflict->files, g_strdup (filename));
	} else {
		PackageFileConflict *conflict = packagefileconflict_new ();
		
		pack->status = PACKAGE_FILE_CONFLICT;
		broken->status = PACKAGE_FILE_CONFLICT;
	
		packagebreaks_set_package (PACKAGEBREAKS (conflict), broken);
		conflict->files = g_list_prepend (conflict->files, g_strdup (filename));
		packagedata_add_to_breaks (pack, PACKAGEBREAKS (conflict));
		gtk_object_unref (GTK_OBJECT (conflict));
	}
	
	return TRUE;
}

/* make sure none of the packages we're installing will share files

   add code to check that no two packages provides the same file 

	hash<char*, PackageData> Hfile;

	for each p in packages {
		for each f in p->provides {
			if p'=Hfile[f] {	
				# f is provided by p'
				whine (about p' conflicting with p over file f)
				set: p breaks p'
			} else {
				Hfile[f] = p;
			}
		}
	}

   (rough draft by robey:)
*/
void 
check_no_two_packages_has_same_file (TrilobiteInstall *service, 
				     GList *packages)
{
	GHashTable *file_table;		/* filename(char *) -> package(PackageData *) */
	GList *iter, *iter_file;
	PackageData *pack, *pack_other;
	char *filename;
	GList *flat_packages;

	flat_packages = flatten_packagedata_dependency_tree (packages);

	if (trilobite_install_get_ignore_file_conflicts (service) ||
	    (g_list_length (flat_packages) == 1)) {
#if EI2_DEBUG & 0x4
		trilobite_debug ("(not performing duplicate file check)");
#endif
		g_list_free (flat_packages);
		return;
	}

	file_table = g_hash_table_new (g_str_hash, g_str_equal);

#if EI2_DEBUG & 0x4
	trilobite_debug ("-> no-two-packages conflict check begins (%d unique packages)", g_list_length (flat_packages));
#endif
	
	for (iter = g_list_first (flat_packages); iter != NULL; iter = g_list_next (iter)) {
		gboolean reported_yet = FALSE;
		int other_conflicts = 0;

		pack = PACKAGEDATA (iter->data);

		if (packagedata_is_suite (pack)) {
			continue;
		}

		g_message (_("file uniqueness checking %s"), pack->name);
		trilobite_install_emit_file_uniqueness_check (service, pack);

		for (iter_file = g_list_first (pack->provides); iter_file != NULL; iter_file = g_list_next (iter_file)) {
			filename = (char *)(iter_file->data);

			pack_other = g_hash_table_lookup (file_table, filename);
			if (pack_other != NULL) {
				/* Dang, this file is provided by both 'pack' and 'pack_other' */
				/* Only report it once in the debug log or we'll spam to eternity on some
				 * large broken packages... */
				if (! reported_yet) {
#if EI2_DEBUG & 0x4
					trilobite_debug ("Duplicate file : %s occurs in %p %s and %p %s",
							 filename, pack, pack->name, 
							 pack_other, pack_other->name);
#else
					{
						char *a, *b;
						a = packagedata_get_readable_name (pack);
						b = packagedata_get_readable_name (pack_other);
						g_message (_("Duplicate file : %s occurs in %s and %s"),
							   filename, a, b);
						g_free (a);
						g_free (b);
					}
#endif
					reported_yet = TRUE;
				} else {
					other_conflicts++;
				}
				if (add_file_conflict (service, pack, pack_other, filename) == FALSE) {
					g_message (_("... but it's ok, the packages are related"));
				}
			} else {
				/* file is okay */
				g_hash_table_insert (file_table, filename, pack);
			}
		}
		if (other_conflicts) {
			g_message (_("(there were %d other conflicts)"), other_conflicts);
		}
	}

	/* let's free all this crap, unlike last time (cough cough) :) */
	/* elements in flat_packages are not to be unreffed */
	g_list_free (flat_packages);
	/* the hashentries point to strings inside the packagedata objects */
	g_hash_table_destroy (file_table);	

#if EI2_DEBUG & 0x4
	trilobite_debug ("<- no_two_packages conflict");
#endif
}

/* determine if package 'pack' is being upgraded in the 'packages' list,
 * and that upgrade no longer needs 'filename'
 */
static gboolean
package_is_upgrading_and_doesnt_need_file (PackageData *pack, GList *packages, const char *filename)
{
	PackageData *pack_upgrade;
	GList *item, *item2;

	/* Hmmm, would trilobite_install_package_compare be better ? */
	item = g_list_find_custom (packages, pack->name,
				   (GCompareFunc)trilobite_install_package_name_compare);
	if (item != NULL) {
		pack_upgrade = PACKAGEDATA (item->data);
		item2 = g_list_find_custom (pack_upgrade->provides, (char *)filename, (GCompareFunc)strcmp);
		if (item2 == NULL) {
			/* package IS in the package list, and does NOT provide this file anymore */
			return TRUE;
		}
	}
	return FALSE;
}

/* check if any of our packages contain files that would conflict with packages already installed.

   Add code to do conflict checking against already installed packages.

	for each p in packages {
		for each f in p->provides {
			L = query (PROVIDES, f)
			L -= p->modifies	# dont care about packages we're modifying
			if L.size > 0 {
				# someone else might own f
				foreach p' in L {
					next if p'->name == p->name
					if p' in packages (name compare && p->provides[f]==null) {
						# we're upgrading p' to p'' which doesn't have f, 
						# so all is well 
					} else {
						whine (p might break p' since they share f);
					}
				}
			} else {
				# f is fine, noone else owns it 
			}
		}
	}

    (rough draft by robey:)
*/
void 
check_conflicts_against_already_installed_packages (TrilobiteInstall *service, 
						    GList *packages)
{
	GList *owners;
	GList *iter, *iter_file, *iter_pack;
	PackageData *pack, *pack_owner;
	char *filename;
	GList *flat_packages;

	if (trilobite_install_get_ignore_file_conflicts (service)) {
#if EI2_DEBUG & 0x4
		trilobite_debug ("(not performing file conflict check)");
#endif
		return;
	}

	flat_packages = flatten_packagedata_dependency_tree (packages);

#if EI2_DEBUG & 0x4
	trilobite_debug ("-> file conflict check begins (%d unique packages)", g_list_length (flat_packages));
#endif

	for (iter = g_list_first (flat_packages); iter != NULL; iter = g_list_next (iter)) {
		pack = PACKAGEDATA (iter->data);

		if (pack->conflicts_checked || packagedata_is_suite (pack)) {
			continue;
		}

		g_message (_("file conflict checking %s"), pack->name);
		trilobite_install_emit_file_conflict_check (service, pack);

		pack->conflicts_checked = TRUE;
		for (iter_file = g_list_first (pack->provides); iter_file != NULL; iter_file = g_list_next (iter_file)) {
			filename = (char *)(iter_file->data);

			/* If the file isn't on the system, no need to check for conflicts */
			if (access (filename, F_OK) != 0) {
				continue;
			}

			owners = trilobite_package_system_query (service->private->package_system,
								 service->private->cur_root,
								 filename,
								 TRILOBITE_PACKAGE_SYSTEM_QUERY_OWNS,
								 OWNS_MUST_HAVE);
			/* No need to check packages that we modify */	
			packagedata_list_prune (&owners, pack->modifies, TRUE, TRUE);

			for (iter_pack = g_list_first (owners); iter_pack != NULL; iter_pack = g_list_next (iter_pack)) {
				pack_owner = (PackageData *)(iter_pack->data);

				if (strcmp (pack_owner->name, pack->name) == 0) {
					/* can't conflict with self */
					gtk_object_unref (GTK_OBJECT (pack_owner));
					continue;
				}

#if EI2_DEBUG & 0x4
				trilobite_debug ("file conflict : package %p %s already provides %s also provided by %p %s",
						 pack_owner, pack_owner->name, filename, pack, pack->name);
#else
				{
					char *a, *b;
					a = packagedata_get_readable_name (pack_owner);
					b = packagedata_get_readable_name (pack);
					g_message (_("file conflict : package %s already provides file %s also provided by %s"),
						   a, filename, b);
					g_free (a);
					g_free (b);
				}
#endif
				if (package_is_upgrading_and_doesnt_need_file (pack_owner, 
									       flat_packages, filename)) {
					/* the owner of this file is a package that we're upgrading, and the
					 * new version no longer has this file, so everything's okay. */
					g_message (_("...but it is okay, we're upgrading %s and it ditched that file"),
						   pack_owner->name);
				} else {
					if (add_file_conflict (service, pack, pack_owner, filename) == FALSE) {
						g_message (_("... but it's ok, the packages are related"));
					}
				}
				gtk_object_unref (GTK_OBJECT (pack_owner));
			}
			g_list_free (owners);
		}
	}

#if EI2_DEBUG & 0x4
	trilobite_debug ("<- file conflict check ends");
#endif
	g_list_free (flat_packages);
}

/* 
   hash<char*, PackageData> Hfeat;
   
	for each p in packages {
		for each f in p->features {
			Hfeat[f] = p;
		}
	}

	for each p in packages {
		for each p' in p->modifies {
			for each f in p'->features {
				if Hfeat[f] {
					# all is well, feature is still on system 
				} else {
					# feature is lost 
					L = query (REQUIRES, f);
					if l.size > 0 {
						# and someone requires this feature 
						whine (p updates p' which breaks packages in L);
					}
				}
			}
		}
	}
*/
void 
check_feature_consistency (TrilobiteInstall *service, 
			   GList *packages)
{
	GHashTable *feature_hash;
	GList *iterator;
	GList *flat_packages;

	flat_packages = flatten_packagedata_dependency_tree (packages);
#if EI2_DEBUG & 0x4
	trilobite_debug ("-> check_feature_consistency begins, %d packages", g_list_length (flat_packages));
#endif
	feature_hash = g_hash_table_new ((GHashFunc)g_str_hash,
					 (GCompareFunc)g_str_equal);

	for (iterator = flat_packages; iterator; iterator = g_list_next (iterator)) {
		PackageData *pack = PACKAGEDATA (iterator->data);
		GList *feature_it;

		if (GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (pack), "do_feature_consistency_check"))!=0) {
			continue;
		}

#if EI2_DEBUG & 0x4
		trilobite_debug ("%s provides %d features", pack->name, g_list_length (pack->features));
#endif
		for (feature_it = pack->features; feature_it; feature_it = g_list_next (feature_it)) {
			char *feature = (char*)feature_it->data;
			g_hash_table_insert (feature_hash, feature, pack);
#if EI2_DEBUG & 0x4
			trilobite_debug ("%s provides feature %s", pack->name, feature);
#endif
		}
	}

	for (iterator = flat_packages; iterator; iterator = g_list_next (iterator)) {
		PackageData *pack = PACKAGEDATA (iterator->data);
		GList *modify_it;

		if (GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (pack), "do_feature_consistency_check"))!=0) {
			continue;
		}
		gtk_object_set_data (GTK_OBJECT (pack), "do_feature_consistency_check", GINT_TO_POINTER (1));

#if EI2_DEBUG & 0x4
		trilobite_debug ("checking %s", pack->name);
#else
		g_message (_("checking feature consistency of %s"), pack->name);
#endif
		trilobite_install_emit_feature_consistency_check (service, pack);

		for (modify_it = pack->modifies; modify_it; modify_it = g_list_next (modify_it)) {
			PackageData *pack_modified = PACKAGEDATA (modify_it->data);
			GList *feature_it;
#if EI2_DEBUG & 0x4
			trilobite_debug ("%s modifies %s", pack->name, pack_modified->name);
#endif

			for (feature_it = pack_modified->features; feature_it; feature_it = g_list_next (feature_it)) {
				char *feature = (char*)feature_it->data;

				if (g_hash_table_lookup (feature_hash, feature)) {
					/* Since feature was in hash, it is still provided by someone */
#if EI2_DEBUG & 0x4
					trilobite_debug ("feature %s is ok", feature);
#endif
				} else {
					GList *required_by;
					GList *break_it;
					/* Feature is lost */
#if EI2_DEBUG & 0x4
					trilobite_debug ("feature %s is lost", feature);
#endif
					required_by = trilobite_package_system_query (service->private->package_system,
										      service->private->cur_root,
										      feature,
										      TRILOBITE_PACKAGE_SYSTEM_QUERY_REQUIRES_FEATURE,
										      PACKAGE_FILL_NO_DIRS_IN_PROVIDES);

#if EI2_DEBUG & 0x4
					if (g_list_length (required_by)==0) {
						trilobite_debug ("but noone uses it...");
					}
#endif
					for (break_it = required_by; break_it; break_it = g_list_next (break_it)) {
						PackageFeatureMissing *feature_missing = packagefeaturemissing_new ();
						PackageData *pack_broken = PACKAGEDATA (break_it->data);
						
						if (trilobite_install_package_compare (pack_broken, pack_modified)==0) {
#if EI2_DEBUG & 0x4
							trilobite_debug ("%p %s, it's my child",
									 pack_broken, pack_broken->name);
#endif
							continue;
							
						}

						if (g_list_find_custom (pack->breaks, pack_broken,
									(GCompareFunc)find_break_by_package_name)) {
#if EI2_DEBUG & 0x4
							trilobite_debug ("%p %s, already marked as borked",
									 pack_broken, pack_broken->name);
#endif
							continue;

						}

						feature_missing = packagefeaturemissing_new ();

#if EI2_DEBUG & 0x4
						trilobite_debug ("%p %s is broken by %p %s modifying %p %s", 
								 pack_broken, pack_broken->name,
								 pack, pack->name,
								 pack_modified, pack_modified->name);
#else
						g_message (_("feature missing : %s breaks if %s is installed (feature %s would be lost"),
							   pack_broken->name,
							   pack->name,
							   feature);
#endif
						pack_broken->status = PACKAGE_DEPENDENCY_FAIL;
						pack->status = PACKAGE_BREAKS_DEPENDENCY;
						packagebreaks_set_package (PACKAGEBREAKS (feature_missing), pack_broken);
						feature_missing->features = g_list_prepend (feature_missing->features, g_strdup (feature));
						packagedata_add_to_breaks (pack, PACKAGEBREAKS (feature_missing));
						gtk_object_unref (GTK_OBJECT (pack_broken));
					}
					g_list_free (required_by);
				}
			}
		}
	}

	g_list_free (flat_packages);
	g_hash_table_destroy (feature_hash);

#if EI2_DEBUG & 0x4
	trilobite_debug ("<- check_feature_consistency");
	trilobite_debug ("POST-FEATURE-CHECK PACKAGE TREE");
	dump_tree (packages);
#endif
}


/* Main conflict entry point */
void
do_file_conflict_check (TrilobiteInstall *service, 
			GList **packages,
			GList **extra_packages)
{
	g_assert (extra_packages);

	/* Check that no two packages in the work set provides the 
	   same files */
	check_no_two_packages_has_same_file (service, *packages);
	/* If so, prune them no, no point in trying to update */
	prune_failed_packages (service, packages); 

	/* Check for file conflicts against already installed packages,
	   if conflicts, try and revive */
	check_conflicts_against_already_installed_packages (service, *packages);
	do_requirement_consistency_check (service, *packages);
	check_tree_for_conflicts (service, packages, extra_packages);
	prune_failed_packages (service, packages); 

	/* If packages were revived (added to extra_packages,
	   exit and we'll be recursed, otherwise check
	   feature consistency */
	if (*extra_packages==NULL) {
		check_feature_consistency (service, *packages);
		check_tree_for_conflicts (service, packages, extra_packages);
		prune_failed_packages (service, packages);
	} else {
#if EI2_DEBUG & 0x4
		trilobite_debug ("extra_packages set, no doing feature consistency check");
#endif		
	}
}

/***********************************************************************************/

/* This is the download stuff */

gboolean download_packages (TrilobiteInstall *service, GList *packages);

gboolean
download_packages (TrilobiteInstall *service, 
		   GList *packages)
{
	GList *flat_packages;
	GList *iterator;
	gboolean result = TRUE;
	
	flat_packages = flatten_packagedata_dependency_tree (packages);
	g_message (_("downloading %d packages"), g_list_length (flat_packages));

	service->private->cancel_download = FALSE;
	for (iterator = flat_packages; iterator; iterator = g_list_next (iterator)) {
		PackageData *pack = PACKAGEDATA (iterator->data);
		if (pack->filename == NULL) {
			if (trilobite_install_fetch_package (service, pack) == FALSE) {
				result = FALSE;
				break;
			}
			if (service->private->cancel_download) {
				result = FALSE;
				break;
			}
		}
	}

	g_list_free (flat_packages);

	return result;
}

/***********************************************************************************/

static gboolean
clean_up_dedupe_hash (char *md5, PackageData *pack)
{
	g_free (md5);
	if (IS_PACKAGEDATA (pack)) {
		gtk_object_unref (GTK_OBJECT (pack));
	} else if (IS_PACKAGEDEPENDENCY (pack)) {
		gtk_object_unref (GTK_OBJECT (pack));
	} else {
		g_assert_not_reached ();
	}
	return TRUE;
}

static gboolean
clean_up_dep_ok_hash (char *key, gpointer unused)
{
	g_free (key);
	return TRUE;
}

unsigned long
trilobite_install_get_total_size_of_packages (TrilobiteInstall *service,
					  const GList *packages)
{
	const GList *iterator;
	unsigned long result = 0;
	for (iterator = packages; iterator; glist_step (iterator)) {
		PackageData *pack;

		pack = (PackageData*)iterator->data;
		result += pack->bytesize;
	}
	return result;
}

static gboolean
execute (TrilobiteInstall *service,
	 GList *packages,
	 TrilobitePackageSystemOperation op,
	 int flags) 
{
	TrilobiteRootHelper *root_helper;
	GList *flat_packages;
	gboolean result = FALSE;

	flat_packages = flatten_packagedata_dependency_tree (packages);

	root_helper = gtk_object_get_data (GTK_OBJECT (service), "trilobite-root-helper");
	gtk_object_set_data (GTK_OBJECT (service->private->package_system), 
			     "trilobite-root-helper", root_helper);	

	if (trilobite_install_get_test (service)) {
		flags |= TRILOBITE_PACKAGE_SYSTEM_OPERATION_TEST;
	}

	if (trilobite_install_get_force (service)) {
		flags |= TRILOBITE_PACKAGE_SYSTEM_OPERATION_FORCE;
	}

	if (trilobite_install_get_downgrade (service)) {
		flags |= TRILOBITE_PACKAGE_SYSTEM_OPERATION_DOWNGRADE;
	}

	if (trilobite_install_get_upgrade (service)) {
		flags |= TRILOBITE_PACKAGE_SYSTEM_OPERATION_UPGRADE;
	}

	trilobite_install_init_transaction (service);
	
	/* Init the hack var to emit the old style progress signals */
	service->private->infoblock [0] = 0; /* bytes of package completed */
	service->private->infoblock [1] = 0; /* total size of package */
	service->private->infoblock [2] = 0; /* number of packages */ 
	service->private->infoblock [3] = g_list_length (flat_packages);
	service->private->infoblock [4] = 0; /* total size completed */
	service->private->infoblock [5] = trilobite_install_get_total_size_of_packages (service, flat_packages);

	switch (op) {
	case TRILOBITE_PACKAGE_SYSTEM_OPERATION_INSTALL:
		trilobite_package_system_install (service->private->package_system,
						  service->private->cur_root,
						  flat_packages,
						  flags);
		break;
	case TRILOBITE_PACKAGE_SYSTEM_OPERATION_UNINSTALL:
		trilobite_package_system_uninstall (service->private->package_system,
						    service->private->cur_root,
						    flat_packages,
						    flags);
		break;
	case TRILOBITE_PACKAGE_SYSTEM_OPERATION_VERIFY:
		trilobite_package_system_verify (service->private->package_system,
						 service->private->cur_root,
						 flat_packages);
		g_assert (0);
		break;
	}

	if (service->private->failed_packages == NULL) {
		result = TRUE;
		if (trilobite_install_emit_save_transaction (service, packages) == TRUE) {
			trilobite_install_save_transaction_report (service);
		}
	} 

	trilobite_install_init_transaction (service);

	g_list_free (flat_packages);
	
	return result;
}

static void 
install_packages_helper (TrilobiteInstall *service, 
			 GList **packages,
			 GList **extra_packages)
{
	g_assert (extra_packages);

#if EI2_DEBUG & 0x4
	trilobite_debug ("-> install_packages_helper");
#endif	
	{
		GList *foo = g_list_copy (*packages);
		do_dep_check (service, packages, foo);
		g_list_free (foo);
	}
	if (service->private->cancel_download) {
		return;
	}
	do_file_conflict_check (service, packages, extra_packages);

#if EI2_DEBUG & 0x8
	trilobite_debug ("FINAL TREE BEGIN");
	dump_tree (*packages);
	if (*extra_packages) {
		trilobite_debug ("EXTRA PACKAGES BEGIN");
		dump_tree (*extra_packages);
	}
	trilobite_debug ("FINAL TREE END");
#endif
#if EI2_DEBUG & 0x4
	trilobite_debug ("<- install_packages_helper");
#endif	
	return;
}

static void
set_toplevel (PackageData *package,
	      TrilobiteInstall *service)
{
	package->toplevel = TRUE;
}

/***********************************************************************************/
/* This is the revert majick */

static GList *
get_packages_with_mod_flag (GList *packages,
			    PackageModification mod) 
{
	GList *it;
	GList *res;
	
	res = NULL;
	for (it = packages; it; it = g_list_next (it)) {
		PackageData *pack = NULL;
		if (IS_PACKAGEDATA (it->data)) {
			pack = PACKAGEDATA (it->data);
		} else if (IS_PACKAGEDEPENDENCY (it->data)) {
			pack = PACKAGEDEPENDENCY (it->data)->package;
		} else {
			g_assert_not_reached ();
		}

		if (pack->modify_status == mod) {
			res = g_list_prepend (res, pack);
		}
		if (pack->depends) {
			res = g_list_concat (res, 
					     get_packages_with_mod_flag (pack->depends, mod));
		}
		if (pack->modifies) {
			res = g_list_concat (res, 
					     get_packages_with_mod_flag (pack->modifies, mod));
		}

		pack->modify_status = PACKAGE_MOD_UNTOUCHED;
	}
	return res;
}

/* Function to prune the uninstall list for elements marked as downgrade */
static void
check_uninst_vs_downgrade (GList **inst, 
			   GList **down) 
{
	GList *it;
	GList *remove;
	
	remove = NULL;
	for (it = *inst; it; it = g_list_next (it)) {
		GList *entry;
		PackageData *pack;

		pack = (PackageData*)it->data;
		entry = g_list_find_custom (*down, pack->name, (GCompareFunc)trilobite_install_package_name_compare);
		if (entry != NULL) {
			remove = g_list_prepend (remove, it->data);
		}
	}

	for (it = remove; it; it = g_list_next (it)) {
		(*inst) = g_list_remove (*inst, it->data);
	}
}

static void
debug_revert (PackageData *pack, gpointer modr)
{
	PackageModification mod = GPOINTER_TO_INT (modr);
	char *name = packagedata_get_readable_name (pack);

	switch (mod) {
	case PACKAGE_MOD_UPGRADED:
		g_message (_("will upgrade %s"), name);
		break;
	case PACKAGE_MOD_DOWNGRADED:
		g_message (_("will downgrade %s"), name);
		break;
	case PACKAGE_MOD_INSTALLED:
		g_message (_("will install %s"), name);
		break;
	case PACKAGE_MOD_UNINSTALLED:
		g_message (_("will uninstall %s"), name);
		break;
	default: break;
			
	};

	g_free (name);
}

/***********************************************************************************/
/* This is the uninstall dep check majick */

/*
static int
compare_break_to_package_by_name (PackageBreaks *breakage, PackageData *pack)
{
	PackageData *broken_package = packagebreaks_get_package (breakage);

	return trilobite_install_package_compare (broken_package, pack);
}
*/

/* This traverses upwards in the deptree from the initial list, and adds
   all packages that will break to "breaks" */
static void
trilobite_uninstall_upward_traverse (TrilobiteInstall *service,
				     GList **packages,
				     GList **failed,
				     GList **breaks)
{
	GList *iterator;
	/*
	  Create set
	  add all packs from packages to set
	  dep check
	  for all break, add to packages and recurse
	 */
#if EI2_DEBUG & 0x4
	trilobite_debug ("--> trilobite_uninstall_upward_traverse %d packages", g_list_length (*packages));
#endif

	g_assert (packages!=NULL);
	g_assert (*packages!=NULL);
	g_assert (breaks!=NULL);
	g_assert (*breaks==NULL);
	g_assert (failed!=NULL);

	/* Open the package system */

	/* Add all packages to the set */

	for (iterator = *packages; iterator; iterator = g_list_next (iterator)) {
		PackageData *pack = (PackageData*)iterator->data;
		GList *matches = NULL;
		GList *match_iterator;
		GList *tmp_breaks = NULL;
		GList *b_iterator = NULL;

		/* Get the packages required by pack */
#if EI2_DEBUG & 0x4
		trilobite_debug ("\t checking requirements by %p %s", pack, rpmname_from_packagedata (pack));
#endif
		matches = trilobite_package_system_query (service->private->package_system,
							  service->private->cur_root,
							  pack,
							  TRILOBITE_PACKAGE_SYSTEM_QUERY_REQUIRES,
							  PACKAGE_FILL_NO_TEXT |
							  PACKAGE_FILL_NO_DIRS_IN_PROVIDES | 
							  PACKAGE_FILL_NO_DEPENDENCIES);
		
		/* For all of them, mark as a break conflict */
		for (match_iterator = matches; match_iterator; match_iterator = g_list_next (match_iterator)) {
			PackageData *requiredby = (PackageData*)match_iterator->data;;
			
			requiredby->status = PACKAGE_DEPENDENCY_FAIL;
			pack->status = PACKAGE_BREAKS_DEPENDENCY;
#if EI2_DEBUG & 0x4
			trilobite_debug ("\t %p %s requires %p %s", 
					 requiredby, requiredby->name, 
					 pack, pack->name);
#else
			{
				char *a, *b;
				a = packagedata_get_readable_name (requiredby);
				b = packagedata_get_readable_name (pack);
				g_message (_("%s requires %s"), a, b);
				g_free (a);
				g_free (b);
			}
#endif

			/* If the broken package is in packages, just continue */
			if (g_list_find_custom (*packages, requiredby->name,
						(GCompareFunc)trilobite_install_package_name_compare)) {
#if EI2_DEBUG & 0x4
				trilobite_debug ("\t skip %p %s", requiredby, requiredby->name);
#endif
				continue;
			}

			/* only add to breaks if it's a new breakage */
			if (g_list_find_custom (*breaks, 
						(gpointer)requiredby, 
						(GCompareFunc)trilobite_install_package_compare) == NULL) {
				PackageFeatureMissing *breakage = packagefeaturemissing_new ();
#if EI2_DEBUG & 0x4
				trilobite_debug ("\t Adding %p %s to breaks", requiredby, requiredby->name);
#endif
				(*breaks) = g_list_prepend ((*breaks), requiredby);
				packagebreaks_set_package (PACKAGEBREAKS (breakage), requiredby);
				packagedata_add_to_breaks (pack, PACKAGEBREAKS (breakage));
				gtk_object_unref (GTK_OBJECT (breakage));
			}

			/* If the pac has not been failed yet (and is a toplevel),
			   fail it */
			if (!g_list_find_custom (*failed, (gpointer)pack->name, 
						 (GCompareFunc)trilobite_install_package_name_compare) &&
			    pack->toplevel) {
				(*failed) = g_list_prepend (*failed, pack);
			}
		}
		g_list_foreach (matches, (GFunc)gtk_object_unref, NULL);
		g_list_free (matches);

		/* Now check the packages that broke, this is where eg. uninstalling
		   glib begins to take forever */
		if (*breaks) {
			trilobite_uninstall_upward_traverse (service, breaks, failed, &tmp_breaks);
		}
		
		/* Add the result from the recursion */
		for (b_iterator = tmp_breaks; b_iterator; b_iterator = g_list_next (b_iterator)) {
			(*breaks) = g_list_prepend ((*breaks), b_iterator->data);
		}
	}
	
	/* Remove the failed packages */
	for (iterator = *failed; iterator; iterator = g_list_next (iterator)) {
		(*packages) = g_list_remove (*packages, iterator->data);
	}

#if EI2_DEBUG & 0x1
	trilobite_debug ("post uninstall upward tree");
	dump_tree (*packages);
#endif
#if EI2_DEBUG & 0x4
	trilobite_debug ("<-- trilobite_uninstall_upward_traverse");
#endif
}

static void
trilobite_uninstall_check_for_install (TrilobiteInstall *service,
				       GList **packages,
				       GList **failed)
{
	GList *iterator;
	GList *remove  = NULL;
	GList *result = NULL;

#if EI2_DEBUG & 0x4
	trilobite_debug ("--> trilobite_uninstall_check_for_install");
#endif
	g_assert (packages);

	for (iterator = *packages; iterator; iterator = g_list_next (iterator)) {		
		PackageData *pack = (PackageData*)iterator->data;		

		if (trilobite_package_system_is_installed (service->private->package_system,
							   service->private->cur_root,
							   pack->name,
							   pack->version,
							   pack->minor,
							   TRILOBITE_SOFTCAT_SENSE_EQ)) {			
			GList *qresult;
			GList *qresult_it;
			
			qresult = trilobite_package_system_query (service->private->package_system,
								  service->private->cur_root,
								  pack->name,
								  TRILOBITE_PACKAGE_SYSTEM_QUERY_MATCHES,
								  PACKAGE_FILL_NO_DEPENDENCIES | 
								  PACKAGE_FILL_NO_TEXT);
			for (qresult_it = qresult; qresult_it; qresult_it = g_list_next (qresult_it)) {
				PackageData *match = PACKAGEDATA (qresult_it->data);
				gboolean proceed = FALSE;
				if (pack->version == NULL) {
					proceed = TRUE;
				}
				if (proceed == FALSE &&
				    trilobite_package_system_compare_version (service->private->package_system,
									      pack->version,
									      match->version) == 0) {
					proceed = TRUE;
				}
				if (proceed) {					
					pack->toplevel = TRUE;
					packagedata_fill_in_missing (pack, match,
								     PACKAGE_FILL_NO_DEPENDENCIES | 
								     PACKAGE_FILL_NO_TEXT);
					result = g_list_prepend (result, pack);
					break;
				}
			}
			g_list_foreach (qresult, (GFunc)gtk_object_unref, NULL);
			g_list_free (qresult);
		} else {
#if EI2_DEBUG & 0x4
			trilobite_debug ("\t %p %s is not installed", pack, pack->name);
#endif
			pack->status = PACKAGE_CANNOT_OPEN;
			remove = g_list_prepend (remove, pack);
		}		
	}
	
	for (iterator = remove; iterator; iterator=g_list_next (iterator)) {
		(*packages) = g_list_remove (*packages, iterator->data);
		(*failed) = g_list_prepend (*failed, iterator->data);
	}
	g_list_free (remove);
	remove = NULL;
	
	g_list_free (*packages);
	(*packages) = result;
	
#if EI2_DEBUG & 0x4
	trilobite_debug ("<-- trilobite_uninstall_check_for_install");
#endif
}

/* Calls the upward and downward traversal */
static void
trilobite_uninstall_globber (TrilobiteInstall *service,
			     GList **packages,
			     GList **failed)
{
	GList *iterator;
	GList *tmp;

	/*
	  call upward with packages
	  call downward with packages and &tmp
	  add all from &tmp to packages
	*/

#if EI2_DEBUG & 0x4
	trilobite_debug ("--> trilobite_uninstall_globber %d packages", g_list_length (*packages));
#endif

	tmp = NULL;

	trilobite_uninstall_check_for_install (service, packages, failed);
	for (iterator = *failed; iterator; iterator = g_list_next (iterator)) {
		trilobite_install_emit_uninstall_failed (service, (PackageData*)iterator->data);
	}
	g_list_foreach (*failed, (GFunc)gtk_object_unref, NULL);
	g_list_free (*failed);
	(*failed)=NULL;

	/* If there are still packages and we're not forcing,
	   do upwards traversel */
	if (*packages && !trilobite_install_get_force (service)) {
		trilobite_uninstall_upward_traverse (service, packages, failed, &tmp);

#if EI2_DEBUG & 0x4
		if (g_list_length (*failed)) {
			trilobite_debug ("FAILED");
			dump_tree (*failed);
		}
#endif
		for (iterator = *failed; iterator; iterator = g_list_next (iterator)) {
			PackageData *pack = (PackageData*)iterator->data;
#if EI2_DEBUG & 0x4
			trilobite_debug ("failed %p %s", pack, pack->name);
#else
			{
				char *a;
				a = packagedata_get_readable_name (pack);
				g_message (_("failed %s"), a);
				g_free (a);
			}
#endif
			trilobite_install_emit_uninstall_failed (service, pack);
		}
		g_list_foreach (*failed, (GFunc)gtk_object_unref, NULL);
		g_list_free (*failed);
		g_list_free (tmp);
	}

#if EI2_DEBUG & 0x4
	trilobite_debug ("<-- trilobite_uninstall_glob");
#endif
}


/***********************************************************************************/

/* These are the methods exposed to the the rest of the service object */

TrilobiteInstallOperationStatus 
install_packages (TrilobiteInstall *service, GList *categories)
{
	TrilobiteInstallOperationStatus result = TRILOBITE_INSTALL_NOTHING;       	
	GList *packages;
	GList *extra_packages = NULL;

	trilobite_softcat_reset_server_update_flag (service->private->softcat);

	packages = packagedata_list_copy (categorylist_flatten_to_packagelist (categories), TRUE);

	g_list_foreach (packages, (GFunc)set_toplevel, service);
	do {
		extra_packages = NULL;
		install_packages_helper (service, &packages, &extra_packages);
		if (extra_packages) {
			GList *iterator;
			/* add the contents of extra_packages, but avoid inserting
			   dupes */
			for (iterator = extra_packages; iterator; iterator = g_list_next (iterator)) {
				PackageData *p = PACKAGEDATA (iterator->data);
				if (g_list_find_custom (packages, p,
							(GCompareFunc)trilobite_install_package_compare)==NULL) {
					packages = g_list_prepend (packages, p);
				}
			}
			
		}
	} while (extra_packages != NULL);

#if EI2_DEBUG & 0x4
	trilobite_debug ("%d packages survived", g_list_length (packages));
#endif	

	if (packages) {
		if (trilobite_install_emit_preflight_check (service, packages)) {
			int flags = 0;
			gboolean go_ahead = TRUE;
			
#if EI2_DEBUG & 0x4
			trilobite_debug ("emit_preflight returned true");
#endif	
			if (download_packages (service, packages)) {
				if (trilobite_install_get_force (service) == FALSE) {
					if (!check_md5_on_files (service, packages)) {
						go_ahead = FALSE;
					}
				}
				if (go_ahead) {
					/* Execute the operation */
					if (execute (service, packages, TRILOBITE_PACKAGE_SYSTEM_OPERATION_INSTALL, flags)) {
						result = TRILOBITE_INSTALL_INSTALL_OK;
					} 
				}
			} else {
				/* FIXME: bugzilla.eazel.com 5722
				   download could fail, do the download func needs to
				   be able to fail the operation... */
			}
		}
	}

	g_hash_table_foreach_remove (service->private->dedupe_hash,
				     (GHRFunc)clean_up_dedupe_hash,
				     service);
	g_hash_table_foreach_remove (service->private->dep_ok_hash,
				     (GHRFunc)clean_up_dep_ok_hash,
				     service);
	g_list_foreach (packages, (GFunc)gtk_object_unref, NULL);

	return result;
}

TrilobiteInstallOperationStatus 
uninstall_packages (TrilobiteInstall *service, GList *categories)
{
	TrilobiteInstallStatus result = TRILOBITE_INSTALL_NOTHING;
	GList *packages = NULL;
	GList *failed = NULL;	

	trilobite_softcat_reset_server_update_flag (service->private->softcat);

#if EI2_DEBUG & 0x4
	trilobite_debug ("--> uninstall_packages");
#endif
	packages = packagedata_list_copy (categorylist_flatten_to_packagelist (categories), TRUE);	
	trilobite_uninstall_globber (service, &packages, &failed);
	
	if (packages) {
		if (trilobite_install_emit_preflight_check (service, packages)) {
			int flags = 0;
			if (execute (service, packages, TRILOBITE_PACKAGE_SYSTEM_OPERATION_UNINSTALL, flags)) {
				result = TRILOBITE_INSTALL_UNINSTALL_OK;
			}
		}
	}

	g_list_foreach (packages, (GFunc)gtk_object_unref, NULL);
	g_list_free (packages);

#if EI2_DEBUG & 0x4
	trilobite_debug ("\tuninstall returns returning %s", 
			 result == TRILOBITE_INSTALL_UNINSTALL_OK ? "OK" : "FAILED");
	trilobite_debug ("<-- uninstall_all_packages");
#endif
	return result;
}

TrilobiteInstallStatus
revert_transaction (TrilobiteInstall *service, 
		    GList *packages)
{
	GList *uninst, *inst, *upgrade, *downgrade;
	CategoryData *cat;
	GList *categories;
	TrilobiteInstallStatus result = TRILOBITE_INSTALL_NOTHING;

	uninst = get_packages_with_mod_flag (packages, PACKAGE_MOD_INSTALLED);
	inst = get_packages_with_mod_flag (packages, PACKAGE_MOD_UNINSTALLED);
	upgrade = get_packages_with_mod_flag (packages, PACKAGE_MOD_DOWNGRADED);
	downgrade = get_packages_with_mod_flag (packages, PACKAGE_MOD_UPGRADED);

	check_uninst_vs_downgrade (&uninst, &downgrade);

	g_list_foreach (uninst, (GFunc)debug_revert, GINT_TO_POINTER (PACKAGE_MOD_UNINSTALLED));
	g_list_foreach (inst, (GFunc)debug_revert, GINT_TO_POINTER (PACKAGE_MOD_INSTALLED));
	g_list_foreach (downgrade, (GFunc)debug_revert, GINT_TO_POINTER (PACKAGE_MOD_DOWNGRADED));
	g_list_foreach (upgrade, (GFunc)debug_revert, GINT_TO_POINTER (PACKAGE_MOD_UPGRADED));

	cat = categorydata_new ();
	categories = g_list_prepend (NULL, cat);

	if (uninst) {
		trilobite_install_set_uninstall (service, TRUE);
		trilobite_install_set_downgrade (service, FALSE);
		trilobite_install_set_upgrade (service, FALSE);
		cat->packages = uninst;
		if (uninstall_packages (service, categories) == TRILOBITE_INSTALL_UNINSTALL_OK) {
			result = TRILOBITE_INSTALL_REVERSION_OK;
		}
	}
	if (inst) {
		trilobite_install_set_uninstall (service, FALSE);
		trilobite_install_set_downgrade (service, FALSE);
		trilobite_install_set_upgrade (service, FALSE);
		cat->packages = inst;
		if (install_packages (service, categories) == TRILOBITE_INSTALL_UNINSTALL_OK) {
			result = TRILOBITE_INSTALL_REVERSION_OK;
		}
	}
	if (downgrade) {
		trilobite_install_set_uninstall (service, FALSE);
		trilobite_install_set_downgrade (service, TRUE);
		trilobite_install_set_upgrade (service, FALSE);
		cat->packages = downgrade;
		if (install_packages (service, categories) == TRILOBITE_INSTALL_UNINSTALL_OK) {
			result = TRILOBITE_INSTALL_REVERSION_OK;
		}
	}
	if (upgrade) {
		trilobite_install_set_uninstall (service, FALSE);
		trilobite_install_set_downgrade (service, TRUE);
		trilobite_install_set_upgrade (service, TRUE);
		cat->packages = upgrade;
		if (install_packages (service, categories) == TRILOBITE_INSTALL_UNINSTALL_OK) {
			result = TRILOBITE_INSTALL_REVERSION_OK;
		}
	}


	categorydata_destroy (cat);
	g_list_free (categories);

	return result;
}


