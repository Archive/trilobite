/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* 
 * Copyright (C) 2000, 2001 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Eskil Heyn Olsen <eskil@eazel.com>
 */

/*
  If compiled with TRILOBITE_INSTALL_NO_CORBA, 
  the object should _NOT_ use Bonobo, OAF, ORBIT
  and whatnot. This is to facilite the statically linked nautilus bootstrap thingy
 */

#ifndef TRILOBITE_INSTALL_PUBLIC_H
#define TRILOBITE_INSTALL_PUBLIC_H

#include <libgnome/gnome-defs.h>
#ifndef TRILOBITE_INSTALL_NO_CORBA
#include "bonobo.h"
#include "trilobite-install.h"
#endif /* TRILOBITE_INSTALL_NO_CORBA */

#include <libtrilobite-ups/trilobite-ups-types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define TYPE_TRILOBITE_INSTALL \
	(trilobite_install_get_type ())
#define TRILOBITE_INSTALL(obj) \
	(GTK_CHECK_CAST ((obj), TYPE_TRILOBITE_INSTALL, TrilobiteInstall))
#define TRILOBITE_INSTALL_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST ((klass), TYPE_TRILOBITE_INSTALL, TrilobiteInstallClass))
#define TRILOBITE_IS_INSTALL(obj) \
	(GTK_CHECK_TYPE ((obj), TYPE_TRILOBITE_INSTALL))
#define TRILOBITE_IS_INSTALL_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE ((klass), TYPE_TRILOBITE_INSTALL))

typedef enum {
	TRILOBITE_INSTALL_NOTHING = 0x0,
	TRILOBITE_INSTALL_INSTALL_OK = 0x1,
	TRILOBITE_INSTALL_UNINSTALL_OK = 0x2,
	TRILOBITE_INSTALL_REVERSION_OK = 0x4
} TrilobiteInstallOperationStatus;

typedef enum {
	TRILOBITE_INSTALL_STATUS_NEW_PACKAGE,
	TRILOBITE_INSTALL_STATUS_UPGRADES,
	TRILOBITE_INSTALL_STATUS_DOWNGRADES,
	TRILOBITE_INSTALL_STATUS_QUO
} TrilobiteInstallStatus;
	
typedef struct _TrilobiteInstall TrilobiteInstall;
typedef struct _TrilobiteInstallClass TrilobiteInstallClass;

struct _TrilobiteInstallClass 
{
#ifdef TRILOBITE_INSTALL_NO_CORBA	
	GtkObjectClass parent_class;
#else 
	BonoboObjectClass parent_class;
#endif /* TRILOBITE_INSTALL_NO_CORBA */
	/* signal prototypes */
	void (*download_progress) (TrilobiteInstall *service, const PackageData *package, int amount, int total);
	void (*file_conflict_check) (TrilobiteInstall *service, const PackageData *package);
	void (*file_uniqueness_check) (TrilobiteInstall *service, const PackageData *package);
	void (*feature_consistency_check) (TrilobiteInstall *service, const PackageData *package);

	gboolean (*preflight_check) (TrilobiteInstall *service, 
				     GList *packages,
				     int total_bytes, 
				     int total_packages);
	gboolean (*save_transaction) (TrilobiteInstall *service, 
				      GList *packages);

	void (*install_progress)  (TrilobiteInstall *service, 
				   const PackageData *pack, 
				   int package_num, int num_packages, 
				   int package_size_completed, int package_size_total,
				   int total_size_completed, int total_size);
	void (*uninstall_progress)  (TrilobiteInstall *service, 
				     const PackageData *pack, 
				     int package_num, int num_packages, 
				     int package_size_completed, int package_size_total,
				     int total_size_completed, int total_size);
	void (*dependency_check) (TrilobiteInstall *service, const PackageData *pack, const PackageData *needed);
	/* 
	   if the set URLType is PROTOCOL_HTTP, info is a HTTPError struc 
	*/
	void (*download_failed) (TrilobiteInstall *service, const PackageData *package);
	/*
	  if RPM_FAIL is RPM_SRC_NOT_SUPPORTED, info is NULL
	                 RPM_DEP_FAIL, info is a GSList of required packages (PackageData objects)
			 RPM_NOT_AN_RPM, info is NULL
	*/
	void (*md5_check_failed) (TrilobiteInstall *service, const PackageData *pd, const char *actual_md5);
	void (*install_failed) (TrilobiteInstall *service, const PackageData *pd);
	void (*uninstall_failed) (TrilobiteInstall *service, const PackageData *pd);

	gboolean (*delete_files) (TrilobiteInstall *service);

	void (*done) (TrilobiteInstall *service, gboolean result);
#ifndef TRILOBITE_INSTALL_NO_CORBA
	gpointer servant_vepv;
#endif /* TRILOBITE_INSTALL_NO_CORBA */
};

typedef struct _TrilobiteInstallPrivate TrilobiteInstallPrivate;

struct _TrilobiteInstall
{
#ifdef TRILOBITE_INSTALL_NO_CORBA	
	GtkObject parent;
#else 
	BonoboObject parent;
	GNOME_Trilobite_InstallCallback callback;
#endif /* TRILOBITE_INSTALL_NO_CORBA */
	TrilobiteInstallPrivate *private;
};

TrilobiteInstall               *trilobite_install_new (void);
TrilobiteInstall               *trilobite_install_new_with_config (void);
GtkType                      	trilobite_install_get_type   (void);
void                        	trilobite_install_unref       (GtkObject *object);

#ifndef TRILOBITE_INSTALL_NO_CORBA
POA_GNOME_Trilobite_Install__epv *trilobite_install_get_epv (void);
GNOME_Trilobite_Install trilobite_install_create_corba_object (BonoboObject *service);
#endif /* TRILOBITE_INSTALL_NO_CORBA */

void trilobite_install_set_log			  (TrilobiteInstall *service,
						   FILE *logfp);
void trilobite_install_open_log                   (TrilobiteInstall *service,
						   const char *fname);
void trilobite_install_log_to_stderr		  (TrilobiteInstall *service,
						   gboolean log_to_stderr);
gboolean trilobite_install_failed_because_of_disk_full (TrilobiteInstall *service);
void trilobite_install_add_repository		  (TrilobiteInstall *service, const char *dir);

/* This sets mode 500 on tmpdir and all files in
   private->downloaded_files */
gboolean trilobite_install_lock_tmp_dir               (TrilobiteInstall *service);
/* This sets mode 700 on tmpdir and downloaded files */
gboolean trilobite_install_unlock_tmp_dir               (TrilobiteInstall *service);

void trilobite_install_emit_file_conflict_check       (TrilobiteInstall *service,
						       const PackageData *pack);
void trilobite_install_emit_file_uniqueness_check     (TrilobiteInstall *service,
						       const PackageData *pack);
void trilobite_install_emit_feature_consistency_check (TrilobiteInstall *service,
						       const PackageData *pack);
void trilobite_install_emit_install_progress          (TrilobiteInstall *service, 
						       const PackageData *pack,
						       int package_num, int num_packages, 
						       int package_size_completed, int package_size_total,
						       int total_size_completed, int total_size);
void trilobite_install_emit_uninstall_progress      (TrilobiteInstall *service, 
						     const PackageData *pack,
						     int package_num, int num_packages, 
						     int package_size_completed, int package_size_total,
						     int total_size_completed, int total_size);
void trilobite_install_emit_download_progress         (TrilobiteInstall *service,
						       const PackageData *package,
						       int amount, 
						       int total);
gboolean trilobite_install_emit_preflight_check     (TrilobiteInstall *service, 
						     GList *packages);
gboolean trilobite_install_emit_save_transaction     (TrilobiteInstall *service, 
						      GList *packages);
void trilobite_install_emit_download_failed           (TrilobiteInstall *service, 
						       const PackageData *package);
void trilobite_install_emit_md5_check_failed          (TrilobiteInstall *service, 
						       const PackageData *pd,
						       const char *actual_md5);
void trilobite_install_emit_install_failed            (TrilobiteInstall *service, 
						       const PackageData *pd);
void trilobite_install_emit_uninstall_failed          (TrilobiteInstall *service, 
						       const PackageData *pd);
void trilobite_install_emit_dependency_check          (TrilobiteInstall *service, 
						       const PackageData *package, 
						       const PackageDependency *needed);
void trilobite_install_emit_dependency_check_pre_ei2  (TrilobiteInstall *service, 
						       const PackageData *package, 
						       const PackageData *needed);
void trilobite_install_emit_done                      (TrilobiteInstall *service, 
						       gboolean result);

/* This is in flux */

gboolean trilobite_install_fetch_remote_package_list (TrilobiteInstall *service);

void trilobite_install_install_packages (TrilobiteInstall *service, 
					 GList *categories, 
					 const char *root);

void trilobite_install_uninstall_packages (TrilobiteInstall *service, 
					   GList *categories, 
					   const char *root);

GList* trilobite_install_query_package_system (TrilobiteInstall *service,
					       const char *query, 
					       int flags,
					       const char *root);

void trilobite_install_revert_transaction_from_xmlstring (TrilobiteInstall *service, 
							  const char *xml, 
							  int size,
							  const char *root);

void trilobite_install_revert_transaction_from_file (TrilobiteInstall *service, 
						     const char *filename,
						     const char *root);

void trilobite_install_delete_downloads (TrilobiteInstall *service);

void trilobite_install_init_transaction (TrilobiteInstall *service);
void trilobite_install_save_transaction_report (TrilobiteInstall *service);

/******************************************************************************/
/* Beware, from hereonafter, it's #def madness, to make the get/set functions */

#define TRILOBITE_INSTALL_SANITY_VAL(name, ret)\
	g_return_val_if_fail (name != NULL, ret); \
	g_return_val_if_fail (TRILOBITE_IS_INSTALL (name), ret); \
	g_return_val_if_fail (name->private->softcat != NULL, ret); \
	g_return_val_if_fail (TRILOBITE_IS_SOFTCAT (name->private->softcat), ret); \
	g_assert (name->private != NULL); \
	g_assert (name->private->iopts != NULL); \
	g_assert (name->private->topts != NULL) 

#define TRILOBITE_INSTALL_SANITY(name)\
	g_return_if_fail (name != NULL); \
	g_return_if_fail (TRILOBITE_IS_INSTALL (name)); \
	g_return_if_fail (name->private->softcat != NULL); \
	g_return_if_fail (TRILOBITE_IS_SOFTCAT (name->private->softcat)); \
	g_assert (name->private != NULL); \
	g_assert (name->private->iopts != NULL); \
	g_assert (name->private->topts != NULL) 

#define ei_access_decl(name, type) \
type trilobite_install_get_##name (TrilobiteInstall *service)

#define ei_access_impl(name, type, var, defl) \
type trilobite_install_get_##name (TrilobiteInstall *service) { \
        TRILOBITE_INSTALL_SANITY_VAL (service, defl); \
	return service->private->var; \
}

#define ei_mutator_decl(name, type) \
void trilobite_install_set_##name (TrilobiteInstall *service, \
                                         const type name)

#define ei_mutator_impl(name, type, var) \
void trilobite_install_set_##name (TrilobiteInstall *service, \
                                         type name) { \
        TRILOBITE_INSTALL_SANITY (service); \
	service->private->var = name; \
}

#define ei_mutator_impl_copy(name, type, var, copyfunc) \
void trilobite_install_set_##name (TrilobiteInstall *service, \
                                         const type name) { \
        TRILOBITE_INSTALL_SANITY (service); \
        g_free (service->private->var); \
	service->private->var = copyfunc ( name ); \
}

/* When adding fields to TrilobiteInstall object, add
   _mutator_decl here
   _access_decl here
   ARG_ in -object.c
   code in trilobite_install_set_arg
   code in trilobite_install_class_initialize
   _mutator_impl in -object.c
   _access_impl in -object.c
*/

ei_mutator_decl (verbose, gboolean);
ei_mutator_decl (silent, gboolean);
ei_mutator_decl (debug, gboolean);
ei_mutator_decl (test, gboolean);
ei_mutator_decl (force, gboolean);
ei_mutator_decl (depend, gboolean);
ei_mutator_decl (upgrade, gboolean);
ei_mutator_decl (uninstall, gboolean);
ei_mutator_decl (downgrade, gboolean);
ei_mutator_decl (protocol, URLType);
ei_mutator_decl (tmp_dir, char*);
ei_mutator_decl (rpmrc_file, char*);
ei_mutator_decl (server, char*);
ei_mutator_decl (username, char*);
ei_mutator_decl (package_list_storage_path, char*);
ei_mutator_decl (package_list, char*);
ei_mutator_decl (root_dirs, GList*);
ei_mutator_decl (transaction_dir, char*);
ei_mutator_decl (server_port, guint);
ei_mutator_decl (cgi_path, char*);
ei_mutator_decl (eazel_auth, gboolean);
ei_mutator_decl (ssl_rename, gboolean);
ei_mutator_decl (ignore_file_conflicts, gboolean);
ei_mutator_decl (ei2, gboolean);

ei_access_decl (verbose, gboolean);
ei_access_decl (silent, gboolean);
ei_access_decl (debug, gboolean);
ei_access_decl (test, gboolean);
ei_access_decl (force, gboolean);
ei_access_decl (depend, gboolean);
ei_access_decl (upgrade, gboolean);
ei_access_decl (uninstall, gboolean);
ei_access_decl (downgrade, gboolean);
ei_access_decl (protocol, URLType );
ei_access_decl (tmp_dir, char*);
ei_access_decl (rpmrc_file, char*);
ei_access_decl (server, char*);
ei_access_decl (username, char*);
ei_access_decl (package_list_storage_path, char*);
ei_access_decl (package_list, char*);
ei_access_decl (root_dirs, GList*);
ei_access_decl (transaction_dir, char*);
ei_access_decl (server_port, guint);
ei_access_decl (cgi_path, char*);
ei_access_decl (eazel_auth, gboolean);
ei_access_decl (ssl_rename, gboolean);
ei_access_decl (ignore_file_conflicts, gboolean);
ei_access_decl (ei2, gboolean);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* TRILOBITE_INSTALL_PUBLIC_H */

