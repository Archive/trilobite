/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* 
 * Copyright (C) 2000 Eazel, Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Andy Hertzfeld
 */

/* main.c - main function and object activation function for the rpm view component. */

#include <config.h>

#include "nautilus-package-view.h"

#include <gnome.h>
#include <libgnomevfs/gnome-vfs.h>
#include <liboaf/liboaf.h>
#include <bonobo.h>

static int object_count = 0;

static void
package_view_object_destroyed(GtkObject *obj)
{
	object_count--;
	if (object_count <= 0) {
		gtk_main_quit ();
	}
}

static BonoboObject *
package_view_make_object (BonoboGenericFactory *factory, 
		    const char *goad_id, 
		    void *closure)
{
	NautilusPackageView *rpm_view;
	NautilusView *nautilus_view;

	if (strcmp (goad_id, "OAFIID:nautilus_package_view:22ea002c-11e6-44fd-b13c-9445175a5e70")) {
		return NULL;
	}
	
	rpm_view = NAUTILUS_PACKAGE_VIEW (gtk_object_new (NAUTILUS_TYPE_PACKAGE_VIEW, NULL));

	object_count++;

	nautilus_view = nautilus_package_view_get_nautilus_view (rpm_view);

	gtk_signal_connect (GTK_OBJECT (nautilus_view), "destroy", package_view_object_destroyed, NULL);

	return BONOBO_OBJECT (nautilus_view);
}

int main(int argc, char *argv[])
{
	BonoboGenericFactory *factory;
	CORBA_ORB orb;
	char *registration_id;
	
	/* Initialize gettext support */
#ifdef ENABLE_NLS /* sadly we need this ifdef because otherwise the following get empty statement warnings */
	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);
#endif
	
	gnomelib_register_popt_table (oaf_popt_options, oaf_get_popt_table_name ());
	orb = oaf_init (argc, argv);
        gnome_init ("nautilus-package-view", VERSION, 
		    argc, argv); 
	
	bonobo_init (orb, CORBA_OBJECT_NIL, CORBA_OBJECT_NIL);

	/* initialize gnome-vfs, etc */
	g_thread_init (NULL);
	gnome_vfs_init ();
	
        registration_id = oaf_make_registration_id ("OAFIID:nautilus_package_view_factory:5986d6a5-8840-44ea-84a1-e7f052bd85cf", getenv ("DISPLAY"));
	factory = bonobo_generic_factory_new_multi (registration_id, 
						    package_view_make_object,
						    NULL);
	g_free (registration_id);

	
	do {
		bonobo_main ();
	} while (object_count > 0);
	
        gnome_vfs_shutdown ();

	return 0;
}
