/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8; tab-width: 8 -*- */

/* 
 * Copyright (C) 2000, 2001  Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: J Shane Culpepper <pepper@eazel.com>
 *          Robey Pointer <robey@eazel.com>
 */

#ifndef _CALLBACKS_H_
#define _CALLBACKS_H_

void nautilus_install_conflict_check (TrilobiteInstallCallback *cb, const PackageData *pack,
				      NautilusInstallView *view);
void nautilus_install_dependency_check (TrilobiteInstallCallback *cb, const PackageData *package,
					const PackageData *needs, NautilusInstallView *view);
gboolean nautilus_install_preflight_check (TrilobiteInstallCallback *cb, 
					   TrilobiteInstallCallbackOperation op,
					   const GList *packages,
					   int total_bytes, 
					   int total_packages,
					   NautilusInstallView *view);
gboolean nautilus_install_save_transaction (TrilobiteInstallCallback *cb, 
					    TrilobiteInstallCallbackOperation op,
					    const GList *packages,
					    NautilusInstallView *view);
void nautilus_install_download_progress (TrilobiteInstallCallback *cb, const PackageData *pack,
					 int amount, int total,
					 NautilusInstallView *view);
void nautilus_install_download_failed (TrilobiteInstallCallback *cb, const PackageData *pack,
				       NautilusInstallView *view);
void nautilus_install_progress (TrilobiteInstallCallback *cb, const PackageData *pack,
				int current_package, int total_packages,
				int package_progress, int package_total,
				int total_progress, int total_total,
				NautilusInstallView *view);
void nautilus_install_failed (TrilobiteInstallCallback *cb, 
			      PackageData *package,
			      NautilusInstallView *view);
void nautilus_install_done (TrilobiteInstallCallback *cb, 
			    gboolean success, 
			    NautilusInstallView *view);


#endif	/* _CALLBACKS_H_ */
